	.section .mdebug.abi32
	.previous
	.file	"output/array.sw.ll"
	.text
	.align	2
	.type	fct,@function
	.ent	fct                     # @fct
fct:
$tmp0:
	.cfi_startproc
	.frame	$sp,0,$ra
	.mask 	0x00000000,0
	.fmask	0x00000000,0
# BB#0:
	.set	noreorder
	.set	nomacro
	addiu	$2, $zero, 78
	jr	$ra
	nop
	.set	macro
	.set	reorder
	.end	fct
$tmp1:
	.size	fct, ($tmp1)-fct
$tmp2:
	.cfi_endproc
$eh_func_end0:

	.section	_main_section,"ax",@progbits
	.globl	main
	.align	2
	.type	main,@function
	.ent	main                    # @main
main:
$tmp5:
	.cfi_startproc
	.frame	$sp,32,$ra
	.mask 	0x80030000,-4
	.fmask	0x00000000,0
# BB#0:
	.set	noreorder
	.set	nomacro
	addiu	$sp, $sp, -32
$tmp6:
	.cfi_def_cfa_offset 32
	sw	$ra, 28($sp)
	sw	$17, 24($sp)
	sw	$16, 20($sp)
$tmp7:
	.cfi_offset 31, -4
$tmp8:
	.cfi_offset 17, -8
$tmp9:
	.cfi_offset 16, -12
	addiu	$2, $zero, 0
	addu	$16, $zero, $2
$BB1_1:                                 # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_3 Depth 2
                                        #       Child Loop BB1_5 Depth 3
	addiu	$3, $zero, 48
	beq	$2, $3, $BB1_9
	nop
# BB#2:                                 #   in Loop: Header=BB1_1 Depth=1
	lui	$3, %hi(array)
	addiu	$3, $3, %lo(array)
	addu	$3, $3, $2
	addiu	$4, $zero, 0
$BB1_3:                                 #   Parent Loop BB1_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_5 Depth 3
	addiu	$5, $zero, 24
	beq	$4, $5, $BB1_8
	nop
# BB#4:                                 #   in Loop: Header=BB1_3 Depth=2
	addu	$5, $3, $4
	addiu	$6, $zero, 0
$BB1_5:                                 #   Parent Loop BB1_1 Depth=1
                                        #     Parent Loop BB1_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	addiu	$7, $zero, -3
	beq	$6, $7, $BB1_7
	nop
# BB#6:                                 #   in Loop: Header=BB1_5 Depth=3
	sll	$7, $6, 2
	subu	$7, $5, $7
	lw	$7, 0($7)
	nop
	addu	$16, $16, $7
	addiu	$6, $6, -1
	j	$BB1_5
	nop
$BB1_7:                                 #   in Loop: Header=BB1_3 Depth=2
	addiu	$4, $4, 12
	j	$BB1_3
	nop
$BB1_8:                                 #   in Loop: Header=BB1_1 Depth=1
	addiu	$2, $2, 24
	j	$BB1_1
	nop
$BB1_9:
	jal	fct
	nop
	lui	$3, %hi($.str)
	addu	$16, $16, $2
	addiu	$4, $3, %lo($.str)
	addiu	$17, $zero, 156
	addu	$5, $zero, $16
	jal	printf
	nop
	bne	$16, $17, $BB1_11
	nop
# BB#10:
	lui	$2, %hi($.str1)
	addiu	$4, $2, %lo($.str1)
	j	$BB1_12
	nop
$BB1_11:
	lui	$2, %hi($.str2)
	addiu	$4, $2, %lo($.str2)
$BB1_12:
	jal	printf
	nop
	addu	$2, $zero, $16
	lw	$16, 20($sp)
	nop
	lw	$17, 24($sp)
	nop
	lw	$ra, 28($sp)
	nop
	addiu	$sp, $sp, 32
	jr	$ra
	nop
	.set	macro
	.set	reorder
	.end	main
$tmp10:
	.size	main, ($tmp10)-main
$tmp11:
	.cfi_endproc
$eh_func_end1:

	.type	array,@object           # @array
	.section	.rodata,"a",@progbits
	.align	2
array:
	.4byte	1                       # 0x1
	.4byte	2                       # 0x2
	.4byte	3                       # 0x3
	.4byte	4                       # 0x4
	.4byte	5                       # 0x5
	.4byte	6                       # 0x6
	.4byte	7                       # 0x7
	.4byte	8                       # 0x8
	.4byte	9                       # 0x9
	.4byte	10                      # 0xa
	.4byte	11                      # 0xb
	.4byte	12                      # 0xc
	.size	array, 48

	.type	$.str,@object           # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
$.str:
	.asciz	 "Result: %d\n"
	.size	$.str, 12

	.type	$.str1,@object          # @.str1
$.str1:
	.asciz	 "RESULT: PASS\n"
	.size	$.str1, 14

	.type	$.str2,@object          # @.str2
$.str2:
	.asciz	 "RESULT: FAIL\n"
	.size	$.str2, 14


