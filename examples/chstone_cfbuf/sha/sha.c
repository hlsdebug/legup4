/*
+--------------------------------------------------------------------------+
| CHStone : a suite of benchmark programs for C-based High-Level Synthesis |
| ======================================================================== |
|                                                                          |
| * Collected and Modified : Y. Hara, H. Tomiyama, S. Honda,               |
|                            H. Takada and K. Ishii                        |
|                            Nagoya University, Japan                      |
|                                                                          |
| * Remark :                                                               |
|    1. This source code is modified to unify the formats of the benchmark |
|       programs in CHStone.                                               |
|    2. Test vectors are added for CHStone.                                |
|    3. If "main_result" is 0 at the end of the program, the program is    |
|       correctly executed.                                                |
|    4. Please follow the copyright of each benchmark program.             |
+--------------------------------------------------------------------------+
*/
/* NIST Secure Hash Algorithm */
/* heavily modified by Uwe Hollerbach uh@alumni.caltech edu */
/* from Peter C. Gutmann's implementation as found in */
/* Applied Cryptography by Bruce Schneier */
/* NIST's proposed modification to SHA of 7/11/94 may be */
/* activated by defining USE_MODIFIED_SHA */
/* SHA f()-functions */
#define f1(x,y,z)	((x & y) | (~x & z))
#define f2(x,y,z)	(x ^ y ^ z)
#define f3(x,y,z)	((x & y) | (x & z) | (y & z))
#define f4(x,y,z)	(x ^ y ^ z)
/* SHA constants */
#define CONST1		0x5a827999L
#define CONST2		0x6ed9eba1L
#define CONST3		0x8f1bbcdcL
#define CONST4		0xca62c1d6L
/* 32-bit rotate */
#define ROT32(x,n)	((x << n) | (x >> (32 - n)))
#define FUNC(n,i)						\
    temp = ROT32(A,5) + f##n(B,C,D) + E + W[i] + CONST##n;	\
    E = D; D = C; C = ROT32(B,30); B = A; A = temp
#include "sha.h"

void memset(LONG *s,int c,int n,int e)
{
  pushDbgCF(54);
  LONG uc;
  LONG *p;
  int m;
  m = n / 4;
  uc = c;
  p = ((LONG *)s);
  while(e-- > 0){
    pushDbgCF(63);
    p++;
  }
  while(m-- > 0){
    pushDbgCF(67);
     *(p++) = uc;
  }
}

void memcpy(LONG *s1,const BYTE *s2,int n)
{
  pushDbgCF(74);
  LONG *p1;
  BYTE *p2;
  LONG tmp;
  int m;
  m = n / 4;
  p1 = ((LONG *)s1);
  p2 = ((BYTE *)s2);
  while(m-- > 0){
    pushDbgCF(84);
    tmp = 0;
    tmp |= (0xFF & ( *(p2++)));
    tmp |= ((0xFF & ( *(p2++))) << 8);
    tmp |= ((0xFF & ( *(p2++))) << 16);
    tmp |= ((0xFF & ( *(p2++))) << 24);
     *p1 = tmp;
    p1++;
  }
}
/* do SHA transformation */

static void sha_transform()
{
  pushDbgCF(99);
  int i;
  LONG temp;
  LONG A;
  LONG B;
  LONG C;
  LONG D;
  LONG E;
  LONG W[80];
  for (i = 0; i < 16; ++i) {
    pushDbgCF(104);
    W[i] = sha_info_data[i];
  }
  for (i = 16; i < 80; ++i) {
    pushDbgCF(108);
    W[i] = W[i - 3] ^ W[i - 8] ^ W[i - 14] ^ W[i - 16];
  }
  A = sha_info_digest[0];
  B = sha_info_digest[1];
  C = sha_info_digest[2];
  D = sha_info_digest[3];
  E = sha_info_digest[4];
  for (i = 0; i < 20; ++i) {
    pushDbgCF(118);
    temp = (((A << 5 | A >> 32 - 5) + (B & C | ~B & D) + E + W[i]) + 0x5a827999L);
    E = D;
    D = C;
    C = B << 30 | B >> 32 - 30;
    B = A;
    A = temp;
  }
  for (i = 20; i < 40; ++i) {
    pushDbgCF(122);
    temp = (((A << 5 | A >> 32 - 5) + (B ^ C ^ D) + E + W[i]) + 0x6ed9eba1L);
    E = D;
    D = C;
    C = B << 30 | B >> 32 - 30;
    B = A;
    A = temp;
  }
  for (i = 40; i < 60; ++i) {
    pushDbgCF(126);
    temp = (((A << 5 | A >> 32 - 5) + (B & C | B & D | C & D) + E + W[i]) + 0x8f1bbcdcL);
    E = D;
    D = C;
    C = B << 30 | B >> 32 - 30;
    B = A;
    A = temp;
  }
  for (i = 60; i < 80; ++i) {
    pushDbgCF(130);
    temp = (((A << 5 | A >> 32 - 5) + (B ^ C ^ D) + E + W[i]) + 0xca62c1d6L);
    E = D;
    D = C;
    C = B << 30 | B >> 32 - 30;
    B = A;
    A = temp;
  }
  sha_info_digest[0] += A;
  sha_info_digest[1] += B;
  sha_info_digest[2] += C;
  sha_info_digest[3] += D;
  sha_info_digest[4] += E;
}
/* initialize the SHA digest */

void sha_init()
{
  pushDbgCF(145);
  sha_info_digest[0] = 0x67452301L;
  sha_info_digest[1] = 0xefcdab89L;
  sha_info_digest[2] = 0x98badcfeL;
  sha_info_digest[3] = 0x10325476L;
  sha_info_digest[4] = 0xc3d2e1f0L;
  sha_info_count_lo = 0L;
  sha_info_count_hi = 0L;
}
/* update the SHA digest */

void sha_update(const BYTE *buffer,int count)
{
  pushDbgCF(159);
  if (sha_info_count_lo + (((LONG )count) << 3) < sha_info_count_lo) {
    pushDbgCF(161);
    ++sha_info_count_hi;
  }
  sha_info_count_lo += ((LONG )count) << 3;
  sha_info_count_hi += ((LONG )count) >> 29;
  while(count >= 64){
    pushDbgCF(167);
    memcpy(sha_info_data,buffer,64);
    sha_transform();
    buffer += 64;
    count -= 64;
  }
  memcpy(sha_info_data,buffer,count);
}
/* finish computing the SHA digest */

void sha_final()
{
  pushDbgCF(180);
  int count;
  LONG lo_bit_count;
  LONG hi_bit_count;
  lo_bit_count = sha_info_count_lo;
  hi_bit_count = sha_info_count_hi;
  count = ((int )(lo_bit_count >> 3 & 0x3f));
  sha_info_data[count++] = 0x80;
  if (count > 56) {
    pushDbgCF(190);
    memset(sha_info_data,0,64 - count,count);
    sha_transform();
    memset(sha_info_data,0,56,0);
  }
   else {
    pushDbgCF(196);
    memset(sha_info_data,0,56 - count,count);
  }
  sha_info_data[14] = hi_bit_count;
  sha_info_data[15] = lo_bit_count;
  sha_transform();
}
/* compute the SHA digest of a FILE stream */

void sha_stream()
{
  pushDbgCF(207);
  int i;
  int j;
  const BYTE *p;
  sha_init();
  for (j = 0; j < 2; j++) {
    pushDbgCF(213);
    i = in_i[j];
    p = &indata[j][0];
    sha_update(p,i);
  }
  sha_final();
}
