volatile long TRACEBUFFER[256];
unsigned char bufIndex;
volatile long traceOut;

void pushDbg(int data,int ID)
{
  TRACEBUFFER[bufIndex++] = ((long )ID) << 32 | ((long )data);
}

void pushDbgCF(int ID)
{
  TRACEBUFFER[bufIndex++] = ID;
}

void pushDbgLong(long dataL,int ID)
{
  TRACEBUFFER[bufIndex++] = ((long )ID) << 32 | dataL >> 32;
  TRACEBUFFER[bufIndex++] = ((long )ID) << 32 | (dataL & 0xffffffffUL);
}
void traceUnload();
/*
+--------------------------------------------------------------------------+
| CHStone : a suite of benchmark programs for C-based High-Level Synthesis |
| ======================================================================== |
|                                                                          |
| * Collected and Modified : Y. Hara, H. Tomiyama, S. Honda,               |
|                            H. Takada and K. Ishii                        |
|                            Nagoya University, Japan                      |
|                                                                          |
| * Remark :                                                               |
|    1. This source code is modified to unify the formats of the benchmark |
|       programs in CHStone.                                               |
|    2. Test vectors are added for CHStone.                                |
|    3. If "main_result" is 0 at the end of the program, the program is    |
|       correctly executed.                                                |
|    4. Please follow the copyright of each benchmark program.             |
+--------------------------------------------------------------------------+
*/
/*
* Copyright (C) 2008
* Y. Hara, H. Tomiyama, S. Honda, H. Takada and K. Ishii
* Nagoya University, Japan
* All rights reserved.
*
* Disclaimer of Warranty
*
* These software programs are available to the user without any license fee or
* royalty on an "as is" basis. The authors disclaims any and all warranties, 
* whether express, implied, or statuary, including any implied warranties or 
* merchantability or of fitness for a particular purpose. In no event shall the
* copyright-holder be liable for any incidental, punitive, or consequential damages
* of any kind whatsoever arising from the use of these programs. This disclaimer
* of warranty extends to the user of these programs and user's customers, employees,
* agents, transferees, successors, and assigns.
*
*/
#include <stdio.h>
int main_result;
#define R 0
#define ADDU 33
#define SUBU 35
#define MULT 24
#define MULTU 25
#define MFHI 16
#define MFLO 18
#define AND 36
#define OR 37
#define XOR 38
#define SLL 0
#define SRL 2
#define SLLV 4
#define SRLV 6
#define SLT 42
#define SLTU 43
#define JR 8
#define J 2
#define JAL 3
#define ADDIU 9
#define ANDI 12
#define ORI 13
#define XORI 14
#define LW 35
#define SW 43
#define LUI 15
#define BEQ 4
#define BNE 5
#define BGEZ 1
#define SLTI 10
#define SLTIU 11
#include "imem.h"
/*
+--------------------------------------------------------------------------+
| * Test Vectors (added for CHStone)                                       |
|     A : input data                                                       |
|     outData : expected output data                                       |
+--------------------------------------------------------------------------+
*/
const int A[8] = {(22), (5), (- 9), (3), (- 17), (38), (0), (11)};
const int outData[8] = {(- 17), (- 9), (0), (3), (5), (11), (22), (38)};
#define IADDR(x)	(((x)&0x000000ff)>>2)
#define DADDR(x)	(((x)&0x000000ff)>>2)

int main()
{
  pushDbgCF(99);
  long long hilo;
  int reg[32];
  int Hi = 0;
  int Lo = 0;
  int pc = 0;
  int dmem[64] = {(0)};
  int j;
  unsigned int ins;
  int op;
  int rs;
  int rt;
  int rd;
  int shamt;
  int funct;
  short address;
  int tgtadr;
  while(1){
    pushDbgCF(119);
    int i;
    int n_inst;
    n_inst = 0;
    main_result = 0;
    for (i = 0; i < 32; i++) {
      pushDbgCF(127);
      reg[i] = 0;
    }
    reg[29] = 0x7fffeffc;
    for (i = 0; i < 8; i++) {
      pushDbgCF(133);
      dmem[i] = A[i];
    }
    pc = 0x00400000;
    do {
      pushDbgCF(140);
      ins = imem[(pc & 0x000000ff) >> 2];
      pc = pc + 4;
      op = (ins >> 26);
      switch(op){
        pushDbgCF(147);
        case 0:
{
          pushDbgCF(0);
          funct = (ins & 0x3f);
          shamt = (ins >> 6 & 0x1f);
          rd = (ins >> 11 & 0x1f);
          rt = (ins >> 16 & 0x1f);
          rs = (ins >> 21 & 0x1f);
          switch(funct){
            pushDbgCF(156);
            case 33:
{
              pushDbgCF(0);
              reg[rd] = reg[rs] + reg[rt];
              break; 
            }
            case 35:
{
              pushDbgCF(0);
              reg[rd] = reg[rs] - reg[rt];
              break; 
            }
            case 24:
{
              pushDbgCF(0);
              hilo = ((long long )reg[rs]) * ((long long )reg[rt]);
              Lo = (hilo & 0x00000000ffffffffULL);
              Hi = (((int )(hilo >> 32)) & 0xffffffffUL);
              break; 
            }
            case 25:
{
              pushDbgCF(0);
              hilo = (((unsigned long long )reg[rs]) * ((unsigned long long )reg[rt]));
              Lo = (hilo & 0x00000000ffffffffULL);
              Hi = (((int )(hilo >> 32)) & 0xffffffffUL);
              break; 
            }
            case 16:
{
              pushDbgCF(0);
              reg[rd] = Hi;
              break; 
            }
            case 18:
{
              pushDbgCF(0);
              reg[rd] = Lo;
              break; 
            }
            case 36:
{
              pushDbgCF(0);
              reg[rd] = reg[rs] & reg[rt];
              break; 
            }
            case 37:
{
              pushDbgCF(0);
              reg[rd] = reg[rs] | reg[rt];
              break; 
            }
            case 38:
{
              pushDbgCF(0);
              reg[rd] = reg[rs] ^ reg[rt];
              break; 
            }
            case 0:
{
              pushDbgCF(0);
              reg[rd] = reg[rt] << shamt;
              break; 
            }
            case 2:
{
              pushDbgCF(0);
              reg[rd] = reg[rt] >> shamt;
              break; 
            }
            case 4:
{
              pushDbgCF(0);
              reg[rd] = reg[rt] << reg[rs];
              break; 
            }
            case 6:
{
              pushDbgCF(0);
              reg[rd] = reg[rt] >> reg[rs];
              break; 
            }
            case 42:
{
              pushDbgCF(0);
              reg[rd] = reg[rs] < reg[rt];
              break; 
            }
            case 43:
{
              pushDbgCF(0);
              reg[rd] = ((unsigned int )reg[rs]) < ((unsigned int )reg[rt]);
              break; 
            }
            case 8:
{
              pushDbgCF(0);
              pc = reg[rs];
              break; 
            }
            default:
{
              pushDbgCF(0);
// error
              pc = 0;
              break; 
            }
          }
          break; 
        }
        case 2:
{
          pushDbgCF(0);
          tgtadr = (ins & 0x3ffffff);
          pc = tgtadr << 2;
          break; 
        }
        case 3:
{
          pushDbgCF(0);
          tgtadr = (ins & 0x3ffffff);
          reg[31] = pc;
          pc = tgtadr << 2;
          break; 
        }
        default:
{
          pushDbgCF(0);
          address = (ins & 0xffff);
          rt = (ins >> 16 & 0x1f);
          rs = (ins >> 21 & 0x1f);
          switch(op){
            pushDbgCF(239);
            case 9:
{
              pushDbgCF(0);
              reg[rt] = reg[rs] + address;
              break; 
            }
            case 12:
{
              pushDbgCF(0);
              reg[rt] = reg[rs] & ((unsigned short )address);
              break; 
            }
            case 13:
{
              pushDbgCF(0);
              reg[rt] = reg[rs] | ((unsigned short )address);
              break; 
            }
            case 14:
{
              pushDbgCF(0);
              reg[rt] = reg[rs] ^ ((unsigned short )address);
              break; 
            }
            case 35:
{
              pushDbgCF(0);
              reg[rt] = dmem[(reg[rs] + address & 0x000000ff) >> 2];
              break; 
            }
            case 43:
{
              pushDbgCF(0);
              dmem[(reg[rs] + address & 0x000000ff) >> 2] = reg[rt];
              break; 
            }
            case 15:
{
              pushDbgCF(0);
              reg[rt] = address << 16;
              break; 
            }
            case 4:
{
              pushDbgCF(0);
              if (reg[rs] == reg[rt]) 
                pc = pc - 4 + (address << 2);
              break; 
            }
            case 5:
{
              pushDbgCF(0);
              if (reg[rs] != reg[rt]) 
                pc = pc - 4 + (address << 2);
              break; 
            }
            case 1:
{
              pushDbgCF(0);
              if (reg[rs] >= 0) 
                pc = pc - 4 + (address << 2);
              break; 
            }
            case 10:
{
              pushDbgCF(0);
              reg[rt] = reg[rs] < address;
              break; 
            }
            case 11:
{
              pushDbgCF(0);
              reg[rt] = ((unsigned int )reg[rs]) < ((unsigned short )address);
              break; 
            }
            default:
{
              pushDbgCF(0);
/* error */
              pc = 0;
              break; 
            }
          }
          break; 
        }
      }
      reg[0] = 0;
      n_inst = n_inst + 1;
    }while (pc != 0);
    main_result += n_inst == 611;
    for (j = 0; j < 8; j++) {
      pushDbgCF(299);
      main_result += dmem[j] == outData[j];
    }
    printf("Result: %d\n",main_result);
    if (main_result == 9) {
      pushDbgCF(304);
      printf("RESULT: PASS\n");
    }
     else {
      pushDbgCF(306);
      printf("RESULT: FAIL\n");
    }
traceUnload();
    return main_result;
  }
}

void traceUnload()
{
  int i;
  for (i = 0; i < 256; i++) 
    traceOut = TRACEBUFFER[i];
}
