#!/bin/bash

cd adpcm
make
make v
cd ../aes
make
make v
cd ../blowfish
make
make v
cd ../dfadd
make
make v
cd ../dfdiv
make
make v
cd ../dfmul
make
make v
cd ../dfsin
make
make v
cd ../gsm
make
make v
cd ../jpeg
make
make v
cd ../mips
make
make v
cd ../motion
make
make v
cd ../sha
make
make v
