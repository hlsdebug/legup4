	.section .mdebug.abi32
	.previous
	.file	"output/gsm.sw.ll"
	.section	_main_section,"ax",@progbits
	.globl	main
	.align	2
	.type	main,@function
	.ent	main                    # @main
main:
$tmp2:
	.cfi_startproc
	.frame	$sp,432,$ra
	.mask 	0x80FF0000,-4
	.fmask	0x00000000,0
# BB#0:
	.set	noreorder
	.set	nomacro
	addiu	$sp, $sp, -432
$tmp3:
	.cfi_def_cfa_offset 432
	sw	$ra, 428($sp)
	sw	$23, 424($sp)
	sw	$22, 420($sp)
	sw	$21, 416($sp)
	sw	$20, 412($sp)
	sw	$19, 408($sp)
	sw	$18, 404($sp)
	sw	$17, 400($sp)
	sw	$16, 396($sp)
$tmp4:
	.cfi_offset 31, -4
$tmp5:
	.cfi_offset 23, -8
$tmp6:
	.cfi_offset 22, -12
$tmp7:
	.cfi_offset 21, -16
$tmp8:
	.cfi_offset 20, -20
$tmp9:
	.cfi_offset 19, -24
$tmp10:
	.cfi_offset 18, -28
$tmp11:
	.cfi_offset 17, -32
$tmp12:
	.cfi_offset 16, -36
	addiu	$3, $zero, 0
$BB0_1:                                 # =>This Inner Loop Header: Depth=1
	lui	$2, %hi(inData)
	addiu	$2, $2, %lo(inData)
	sll	$4, $3, 1
	subu	$5, $2, $4
	addiu	$2, $sp, 36
	addiu	$3, $3, -1
	lhu	$5, 0($5)
	nop
	subu	$6, $2, $4
	addiu	$4, $zero, -160
	sh	$5, 0($6)
	bne	$3, $4, $BB0_1
	nop
# BB#2:
	addiu	$3, $zero, 0
	addu	$5, $zero, $3
$BB0_3:                                 # =>This Inner Loop Header: Depth=1
	sll	$4, $3, 1
	subu	$4, $2, $4
	lh	$4, 0($4)
	nop
	addiu	$6, $zero, -1
	slt	$6, $6, $4
	bne	$6, $zero, $BB0_7
	nop
# BB#4:                                 #   in Loop: Header=BB0_3 Depth=1
	andi	$6, $4, 65535
	ori	$7, $zero, 32768
	xor	$6, $6, $7
	beq	$6, $zero, $BB0_6
	nop
# BB#5:                                 #   in Loop: Header=BB0_3 Depth=1
	addiu	$6, $zero, 0
	subu	$4, $6, $4
	j	$BB0_7
	nop
$BB0_6:                                 #   in Loop: Header=BB0_3 Depth=1
	addiu	$4, $zero, 32767
$BB0_7:                                 # %gsm_abs.exit.i
                                        #   in Loop: Header=BB0_3 Depth=1
	sll	$6, $5, 16
	sll	$7, $4, 16
	sra	$6, $6, 16
	sra	$7, $7, 16
	slt	$6, $6, $7
	beq	$6, $zero, $BB0_9
	nop
# BB#8:                                 # %gsm_abs.exit.i
                                        #   in Loop: Header=BB0_3 Depth=1
	addu	$5, $zero, $4
$BB0_9:                                 # %gsm_abs.exit.i
                                        #   in Loop: Header=BB0_3 Depth=1
	addiu	$3, $3, -1
	addiu	$4, $zero, -160
	bne	$3, $4, $BB0_3
	nop
# BB#10:
	addiu	$3, $zero, 0
	sw	$3, 16($sp)
	andi	$4, $5, 65535
	beq	$4, $zero, $BB0_29
	nop
# BB#11:
	sll	$3, $5, 16
	addiu	$4, $zero, -1
	slt	$4, $4, $3
	bne	$4, $zero, $BB0_15
	nop
# BB#12:
	lui	$4, 49152
	ori	$4, $4, 1
	slt	$4, $3, $4
	beq	$4, $zero, $BB0_14
	nop
# BB#13:
	addiu	$3, $zero, 0
	j	$BB0_22
	nop
$BB0_14:
	nor	$3, $3, $zero
$BB0_15:
	lui	$4, 1
	ori	$4, $4, 0
	sltu	$4, $3, $4
	bne	$4, $zero, $BB0_19
	nop
# BB#16:
	lui	$4, 256
	ori	$4, $4, 0
	sltu	$4, $3, $4
	bne	$4, $zero, $BB0_18
	nop
# BB#17:
	lui	$4, %hi(bitoff)
	addiu	$4, $4, %lo(bitoff)
	srl	$3, $3, 24
	addu	$3, $4, $3
	lbu	$3, 0($3)
	nop
	addiu	$3, $3, -1
	j	$BB0_22
	nop
$BB0_18:
	lui	$5, %hi(bitoff)
	srl	$4, $3, 16
	addiu	$3, $5, %lo(bitoff)
	andi	$4, $4, 255
	addu	$3, $3, $4
	lbu	$3, 0($3)
	nop
	addiu	$3, $3, 7
	j	$BB0_22
	nop
$BB0_19:
	andi	$4, $3, 65280
	beq	$4, $zero, $BB0_21
	nop
# BB#20:
	lui	$5, %hi(bitoff)
	srl	$4, $3, 8
	addiu	$3, $5, %lo(bitoff)
	andi	$4, $4, 255
	addu	$3, $3, $4
	lbu	$3, 0($3)
	nop
	addiu	$3, $3, 15
	j	$BB0_22
	nop
$BB0_21:
	lui	$4, %hi(bitoff)
	addiu	$4, $4, %lo(bitoff)
	andi	$3, $3, 255
	addu	$3, $4, $3
	lbu	$3, 0($3)
	nop
	addiu	$3, $3, 23
$BB0_22:
	addiu	$4, $zero, 4
	subu	$3, $4, $3
	sll	$4, $3, 16
	lui	$5, 4
	addiu	$6, $zero, 0
	sra	$3, $4, 16
	slt	$6, $6, $4
	sw	$6, 16($sp)
	ori	$5, $5, 65534
	addiu	$4, $4, -1
	sltu	$4, $5, $4
	bne	$4, $zero, $BB0_29
	nop
# BB#23:                                # %.preheader6.i
	addiu	$4, $3, -1
	addiu	$5, $zero, 16384
	srlv	$4, $5, $4
	sll	$5, $4, 16
	sra	$5, $5, 16
	addiu	$6, $zero, 0
	andi	$4, $4, 65535
	ori	$7, $zero, 32768
	bne	$4, $7, $BB0_28
	nop
$BB0_24:                                # %.preheader6.split.us.i
                                        # =>This Inner Loop Header: Depth=1
	sll	$4, $6, 1
	subu	$7, $2, $4
	lh	$4, 0($7)
	nop
	addiu	$8, $zero, -32768
	bne	$4, $8, $BB0_26
	nop
# BB#25:                                #   in Loop: Header=BB0_24 Depth=1
	addiu	$4, $zero, 32767
	j	$BB0_27
	nop
$BB0_26:                                #   in Loop: Header=BB0_24 Depth=1
	mult	$4, $5
	mflo	$4
	addiu	$4, $4, 16384
	srl	$4, $4, 15
$BB0_27:                                # %gsm_mult_r.exit.us.i
                                        #   in Loop: Header=BB0_24 Depth=1
	addiu	$6, $6, -1
	addiu	$8, $zero, -160
	sh	$4, 0($7)
	bne	$6, $8, $BB0_24
	nop
	j	$BB0_29
	nop
$BB0_28:                                # %gsm_mult_r.exit.i
                                        # =>This Inner Loop Header: Depth=1
	sll	$4, $6, 1
	subu	$4, $2, $4
	lh	$7, 0($4)
	nop
	mult	$7, $5
	mflo	$7
	addiu	$7, $7, 16384
	addiu	$6, $6, -1
	srl	$8, $7, 15
	addiu	$7, $zero, -160
	sh	$8, 0($4)
	bne	$6, $7, $BB0_28
	nop
$BB0_29:                                # %.thread.i
	lh	$5, 36($sp)
	nop
	lh	$7, 38($sp)
	nop
	lh	$8, 40($sp)
	nop
	mult	$7, $7
	mflo	$4
	mult	$5, $5
	mflo	$6
	lh	$9, 42($sp)
	nop
	addu	$11, $8, $5
	mult	$8, $8
	mflo	$12
	addu	$4, $4, $6
	lh	$10, 44($sp)
	nop
	mult	$9, $9
	mflo	$6
	addu	$13, $12, $4
	mult	$8, $9
	mflo	$4
	mult	$7, $11
	mflo	$24
	mult	$7, $9
	mflo	$11
	mult	$5, $8
	mflo	$16
	lh	$12, 46($sp)
	nop
	mult	$10, $10
	mflo	$15
	addu	$25, $6, $13
	mult	$9, $10
	mflo	$14
	addu	$24, $4, $24
	mult	$8, $10
	mflo	$13
	addu	$4, $11, $16
	mult	$7, $10
	mflo	$6
	mult	$5, $9
	mflo	$18
	lh	$11, 48($sp)
	nop
	mult	$12, $12
	mflo	$16
	addu	$17, $15, $25
	mult	$10, $12
	mflo	$25
	addu	$21, $14, $24
	mult	$9, $12
	mflo	$24
	addu	$20, $13, $4
	mult	$8, $12
	mflo	$15
	addu	$19, $6, $18
	mult	$7, $12
	mflo	$13
	mult	$5, $10
	mflo	$23
	lh	$6, 50($sp)
	nop
	mult	$11, $11
	mflo	$14
	addu	$18, $16, $17
	mult	$12, $11
	mflo	$17
	addu	$22, $25, $21
	mult	$10, $11
	mflo	$16
	addu	$21, $24, $20
	mult	$9, $11
	mflo	$25
	addu	$20, $15, $19
	mult	$8, $11
	mflo	$24
	addu	$19, $13, $23
	mult	$7, $11
	mflo	$15
	mult	$5, $12
	mflo	$23
	mult	$6, $6
	mflo	$13
	addu	$14, $14, $18
	mult	$11, $6
	mflo	$18
	addu	$17, $17, $22
	mult	$12, $6
	mflo	$22
	addu	$16, $16, $21
	mult	$10, $6
	mflo	$10
	addu	$4, $25, $20
	mult	$9, $6
	mflo	$25
	addu	$24, $24, $19
	mult	$8, $6
	mflo	$19
	addu	$15, $15, $23
	mult	$7, $6
	mflo	$20
	mult	$5, $11
	mflo	$21
	addu	$12, $13, $14
	addu	$7, $18, $17
	addu	$8, $22, $16
	addu	$9, $10, $4
	addu	$10, $25, $24
	addu	$11, $19, $15
	addu	$13, $20, $21
	mult	$5, $6
	mflo	$14
	addiu	$24, $2, 8
	addiu	$25, $zero, 0
	addu	$15, $zero, $25
$BB0_30:                                # =>This Inner Loop Header: Depth=1
	sll	$4, $25, 1
	subu	$16, $24, $4
	sll	$4, $6, 16
	lh	$6, 8($16)
	nop
	sra	$17, $4, 16
	lh	$19, 4($16)
	nop
	lh	$21, 2($16)
	nop
	lh	$23, 0($16)
	nop
	lh	$4, -2($16)
	nop
	lh	$22, -4($16)
	nop
	lh	$20, -6($16)
	nop
	lh	$18, -8($16)
	nop
	mult	$6, $6
	mflo	$16
	mult	$17, $6
	mflo	$17
	mult	$19, $6
	mflo	$19
	mult	$21, $6
	mflo	$21
	mult	$23, $6
	mflo	$23
	mult	$4, $6
	mflo	$4
	mult	$22, $6
	mflo	$22
	mult	$20, $6
	mflo	$20
	mult	$18, $6
	mflo	$18
	addiu	$25, $25, -1
	addu	$12, $16, $12
	addu	$7, $17, $7
	addu	$8, $19, $8
	addu	$9, $21, $9
	addu	$10, $23, $10
	addu	$11, $4, $11
	addu	$13, $22, $13
	addu	$14, $20, $14
	addu	$15, $18, $15
	addiu	$4, $zero, -152
	bne	$25, $4, $BB0_30
	nop
# BB#31:                                # %.preheader5.i
	lw	$4, 16($sp)
	nop
	beq	$4, $zero, $BB0_35
	nop
# BB#32:
	addiu	$4, $zero, 159
$BB0_33:                                # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	sll	$5, $5, 16
	sra	$6, $5, 16
	sll	$5, $4, 1
	subu	$5, $2, $5
	sllv	$24, $6, $3
	slti	$6, $4, 1
	sh	$24, 318($5)
	bne	$6, $zero, $BB0_35
	nop
# BB#34:                                # %._crit_edge.i
                                        #   in Loop: Header=BB0_33 Depth=1
	addiu	$4, $4, -1
	lhu	$5, 320($5)
	nop
	j	$BB0_33
	nop
$BB0_35:                                # %Autocorrelation.exit
	sll	$4, $12, 1
	bne	$4, $zero, $BB0_37
	nop
# BB#36:                                # %.preheader.i5
	addiu	$3, $zero, 0
	sw	$zero, 20($sp)
	sw	$zero, 24($sp)
	sw	$zero, 28($sp)
	sw	$zero, 32($sp)
	j	$BB0_121
	nop
$BB0_37:
	sll	$3, $15, 1
	sll	$5, $14, 1
	sll	$6, $13, 1
	sll	$11, $11, 1
	sll	$12, $10, 1
	sll	$9, $9, 1
	sll	$8, $8, 1
	sll	$7, $7, 1
	addiu	$10, $zero, -1
	slt	$13, $10, $4
	addu	$10, $zero, $4
	bne	$13, $zero, $BB0_41
	nop
# BB#38:
	lui	$10, 49152
	ori	$10, $10, 1
	slt	$10, $4, $10
	beq	$10, $zero, $BB0_40
	nop
# BB#39:
	addiu	$13, $zero, 0
	j	$BB0_48
	nop
$BB0_40:
	nor	$10, $4, $zero
$BB0_41:
	lui	$13, 1
	ori	$13, $13, 0
	sltu	$13, $10, $13
	bne	$13, $zero, $BB0_45
	nop
# BB#42:
	lui	$13, 256
	ori	$13, $13, 0
	sltu	$13, $10, $13
	bne	$13, $zero, $BB0_44
	nop
# BB#43:
	lui	$13, %hi(bitoff)
	addiu	$13, $13, %lo(bitoff)
	srl	$10, $10, 24
	addu	$10, $13, $10
	lbu	$10, 0($10)
	nop
	addiu	$13, $10, -1
	j	$BB0_48
	nop
$BB0_44:
	lui	$14, %hi(bitoff)
	srl	$13, $10, 16
	addiu	$10, $14, %lo(bitoff)
	andi	$13, $13, 255
	addu	$10, $10, $13
	lbu	$10, 0($10)
	nop
	addiu	$13, $10, 7
	j	$BB0_48
	nop
$BB0_45:
	andi	$13, $10, 65280
	beq	$13, $zero, $BB0_47
	nop
# BB#46:
	lui	$14, %hi(bitoff)
	srl	$13, $10, 8
	addiu	$10, $14, %lo(bitoff)
	andi	$13, $13, 255
	addu	$10, $10, $13
	lbu	$10, 0($10)
	nop
	addiu	$13, $10, 15
	j	$BB0_48
	nop
$BB0_47:
	lui	$13, %hi(bitoff)
	addiu	$13, $13, %lo(bitoff)
	andi	$10, $10, 255
	addu	$10, $13, $10
	lbu	$10, 0($10)
	nop
	addiu	$13, $10, 23
$BB0_48:                                # %gsm_norm.exit.i
	sllv	$7, $7, $13
	srl	$10, $7, 16
	sllv	$7, $8, $13
	sllv	$8, $9, $13
	srl	$7, $7, 16
	sh	$10, 358($sp)
	sllv	$12, $12, $13
	srl	$9, $8, 16
	sh	$7, 360($sp)
	sllv	$8, $11, $13
	srl	$11, $12, 16
	sh	$9, 362($sp)
	sllv	$12, $6, $13
	srl	$6, $8, 16
	sh	$11, 364($sp)
	sllv	$8, $5, $13
	srl	$5, $12, 16
	sh	$6, 366($sp)
	sllv	$4, $4, $13
	srl	$8, $8, 16
	sh	$5, 368($sp)
	srl	$4, $4, 16
	sh	$8, 370($sp)
	sh	$4, 376($sp)
	sh	$10, 378($sp)
	sh	$7, 380($sp)
	sh	$9, 382($sp)
	sh	$11, 384($sp)
	sh	$6, 386($sp)
	addiu	$4, $sp, 376
	addiu	$6, $sp, 356
	sllv	$9, $3, $13
	sh	$5, 388($sp)
	addiu	$4, $4, 4
	ori	$5, $6, 2
	addiu	$3, $zero, 0
	addiu	$6, $zero, 2
	addiu	$7, $zero, 7
	srl	$9, $9, 16
	sh	$8, 390($sp)
	sh	$9, 392($sp)
$BB0_49:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_102 Depth 2
	sll	$9, $10, 16
	addiu	$8, $zero, -1
	sra	$9, $9, 16
	slt	$8, $8, $9
	bne	$8, $zero, $BB0_53
	nop
# BB#50:                                #   in Loop: Header=BB0_49 Depth=1
	andi	$8, $10, 65535
	ori	$9, $zero, 32768
	xor	$8, $8, $9
	beq	$8, $zero, $BB0_52
	nop
# BB#51:                                #   in Loop: Header=BB0_49 Depth=1
	addiu	$8, $zero, 0
	subu	$10, $8, $10
	j	$BB0_53
	nop
$BB0_52:                                #   in Loop: Header=BB0_49 Depth=1
	addiu	$10, $zero, 32767
$BB0_53:                                # %gsm_abs.exit.i9
                                        #   in Loop: Header=BB0_49 Depth=1
	addiu	$8, $sp, 20
	sll	$11, $10, 16
	lh	$9, 376($sp)
	nop
	sra	$11, $11, 16
	slt	$12, $9, $11
	beq	$12, $zero, $BB0_57
	nop
# BB#54:                                # %.preheader14.i
	addiu	$5, $6, -1
	addiu	$4, $zero, 8
	slt	$4, $4, $5
	bne	$4, $zero, $BB0_120
	nop
# BB#55:                                # %.lr.ph.i.preheader
	addu	$4, $8, $3
	addiu	$3, $zero, 9
	subu	$5, $3, $5
	addiu	$3, $zero, 0
$BB0_56:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	addiu	$5, $5, -1
	addiu	$6, $4, 2
	sh	$3, 0($4)
	addu	$4, $zero, $6
	bne	$5, $zero, $BB0_56
	nop
	j	$BB0_121
	nop
$BB0_57:                                #   in Loop: Header=BB0_49 Depth=1
	sll	$12, $7, 1
	subu	$8, $8, $12
	andi	$10, $10, 65535
	bne	$10, $zero, $BB0_59
	nop
# BB#58:                                #   in Loop: Header=BB0_49 Depth=1
	addiu	$9, $zero, 0
	j	$BB0_90
	nop
$BB0_59:                                # %.preheader.i.i
                                        #   in Loop: Header=BB0_49 Depth=1
	sll	$11, $11, 1
	addiu	$10, $zero, 0
	slt	$13, $11, $9
	addu	$12, $zero, $9
	beq	$13, $zero, $BB0_61
	nop
# BB#60:                                # %.preheader.i.i
                                        #   in Loop: Header=BB0_49 Depth=1
	addu	$12, $zero, $10
$BB0_61:                                # %.preheader.i.i
                                        #   in Loop: Header=BB0_49 Depth=1
	bne	$13, $zero, $BB0_63
	nop
# BB#62:                                #   in Loop: Header=BB0_49 Depth=1
	addiu	$10, $zero, 2
$BB0_63:                                # %.preheader.i.i
                                        #   in Loop: Header=BB0_49 Depth=1
	subu	$11, $11, $12
	sll	$11, $11, 1
	slt	$12, $11, $9
	bne	$12, $zero, $BB0_65
	nop
# BB#64:                                #   in Loop: Header=BB0_49 Depth=1
	subu	$11, $11, $9
	ori	$10, $10, 1
$BB0_65:                                # %.backedge.1.i.i
                                        #   in Loop: Header=BB0_49 Depth=1
	sll	$10, $10, 1
	sll	$11, $11, 1
	slt	$12, $11, $9
	bne	$12, $zero, $BB0_67
	nop
# BB#66:                                #   in Loop: Header=BB0_49 Depth=1
	subu	$11, $11, $9
	ori	$10, $10, 1
$BB0_67:                                # %.backedge.2.i.i
                                        #   in Loop: Header=BB0_49 Depth=1
	sll	$10, $10, 1
	sll	$11, $11, 1
	slt	$12, $11, $9
	bne	$12, $zero, $BB0_69
	nop
# BB#68:                                #   in Loop: Header=BB0_49 Depth=1
	subu	$11, $11, $9
	ori	$10, $10, 1
$BB0_69:                                # %.backedge.3.i.i
                                        #   in Loop: Header=BB0_49 Depth=1
	sll	$10, $10, 1
	sll	$11, $11, 1
	slt	$12, $11, $9
	bne	$12, $zero, $BB0_71
	nop
# BB#70:                                #   in Loop: Header=BB0_49 Depth=1
	subu	$11, $11, $9
	ori	$10, $10, 1
$BB0_71:                                # %.backedge.4.i.i
                                        #   in Loop: Header=BB0_49 Depth=1
	sll	$10, $10, 1
	sll	$11, $11, 1
	slt	$12, $11, $9
	bne	$12, $zero, $BB0_73
	nop
# BB#72:                                #   in Loop: Header=BB0_49 Depth=1
	subu	$11, $11, $9
	ori	$10, $10, 1
$BB0_73:                                # %.backedge.5.i.i
                                        #   in Loop: Header=BB0_49 Depth=1
	sll	$10, $10, 1
	sll	$11, $11, 1
	slt	$12, $11, $9
	bne	$12, $zero, $BB0_75
	nop
# BB#74:                                #   in Loop: Header=BB0_49 Depth=1
	subu	$11, $11, $9
	ori	$10, $10, 1
$BB0_75:                                # %.backedge.6.i.i
                                        #   in Loop: Header=BB0_49 Depth=1
	sll	$10, $10, 1
	sll	$11, $11, 1
	slt	$12, $11, $9
	bne	$12, $zero, $BB0_77
	nop
# BB#76:                                #   in Loop: Header=BB0_49 Depth=1
	subu	$11, $11, $9
	ori	$10, $10, 1
$BB0_77:                                # %.backedge.7.i.i
                                        #   in Loop: Header=BB0_49 Depth=1
	sll	$10, $10, 1
	sll	$11, $11, 1
	slt	$12, $11, $9
	bne	$12, $zero, $BB0_79
	nop
# BB#78:                                #   in Loop: Header=BB0_49 Depth=1
	subu	$11, $11, $9
	ori	$10, $10, 1
$BB0_79:                                # %.backedge.8.i.i
                                        #   in Loop: Header=BB0_49 Depth=1
	sll	$10, $10, 1
	sll	$11, $11, 1
	slt	$12, $11, $9
	bne	$12, $zero, $BB0_81
	nop
# BB#80:                                #   in Loop: Header=BB0_49 Depth=1
	subu	$11, $11, $9
	ori	$10, $10, 1
$BB0_81:                                # %.backedge.9.i.i
                                        #   in Loop: Header=BB0_49 Depth=1
	sll	$10, $10, 1
	sll	$11, $11, 1
	slt	$12, $11, $9
	bne	$12, $zero, $BB0_83
	nop
# BB#82:                                #   in Loop: Header=BB0_49 Depth=1
	subu	$11, $11, $9
	ori	$10, $10, 1
$BB0_83:                                # %.backedge.10.i.i
                                        #   in Loop: Header=BB0_49 Depth=1
	sll	$10, $10, 1
	sll	$11, $11, 1
	slt	$12, $11, $9
	bne	$12, $zero, $BB0_85
	nop
# BB#84:                                #   in Loop: Header=BB0_49 Depth=1
	subu	$11, $11, $9
	ori	$10, $10, 1
$BB0_85:                                # %.backedge.11.i.i
                                        #   in Loop: Header=BB0_49 Depth=1
	sll	$10, $10, 1
	sll	$11, $11, 1
	slt	$12, $11, $9
	bne	$12, $zero, $BB0_87
	nop
# BB#86:                                #   in Loop: Header=BB0_49 Depth=1
	subu	$11, $11, $9
	ori	$10, $10, 1
$BB0_87:                                # %.backedge.12.i.i
                                        #   in Loop: Header=BB0_49 Depth=1
	sll	$10, $10, 1
	sll	$11, $11, 1
	slt	$12, $11, $9
	bne	$12, $zero, $BB0_89
	nop
# BB#88:                                #   in Loop: Header=BB0_49 Depth=1
	subu	$11, $11, $9
	ori	$10, $10, 1
$BB0_89:                                # %.backedge.13.i.i
                                        #   in Loop: Header=BB0_49 Depth=1
	sll	$11, $11, 1
	slt	$11, $11, $9
	sll	$9, $10, 1
	xori	$10, $11, 1
	or	$9, $9, $10
$BB0_90:                                # %gsm_div.exit.i
                                        #   in Loop: Header=BB0_49 Depth=1
	sh	$9, 14($8)
	lh	$10, 378($sp)
	nop
	slti	$10, $10, 1
	bne	$10, $zero, $BB0_92
	nop
# BB#91:                                #   in Loop: Header=BB0_49 Depth=1
	addiu	$10, $zero, 0
	subu	$9, $10, $9
	sh	$9, 14($8)
$BB0_92:                                #   in Loop: Header=BB0_49 Depth=1
	beq	$7, $zero, $BB0_120
	nop
# BB#93:                                #   in Loop: Header=BB0_49 Depth=1
	lh	$10, 378($sp)
	nop
	andi	$11, $9, 65535
	ori	$12, $zero, 32768
	bne	$11, $12, $BB0_96
	nop
# BB#94:                                #   in Loop: Header=BB0_49 Depth=1
	andi	$11, $10, 65535
	ori	$12, $zero, 32768
	bne	$11, $12, $BB0_96
	nop
# BB#95:                                #   in Loop: Header=BB0_49 Depth=1
	addiu	$9, $zero, 32767
	j	$BB0_97
	nop
$BB0_96:                                #   in Loop: Header=BB0_49 Depth=1
	sll	$9, $9, 16
	sra	$9, $9, 16
	mult	$10, $9
	mflo	$9
	sll	$9, $9, 1
	ori	$10, $zero, 32768
	addu	$9, $9, $10
	sra	$9, $9, 16
$BB0_97:                                # %gsm_mult_r.exit11.i
                                        #   in Loop: Header=BB0_49 Depth=1
	lh	$10, 376($sp)
	nop
	addu	$9, $10, $9
	slti	$10, $9, -32768
	beq	$10, $zero, $BB0_99
	nop
# BB#98:                                #   in Loop: Header=BB0_49 Depth=1
	ori	$9, $zero, 32768
	j	$BB0_101
	nop
$BB0_99:                                #   in Loop: Header=BB0_49 Depth=1
	addiu	$10, $zero, 32767
	slt	$11, $10, $9
	beq	$11, $zero, $BB0_101
	nop
# BB#100:                               #   in Loop: Header=BB0_49 Depth=1
	addu	$9, $zero, $10
$BB0_101:                               # %gsm_add.exit8.i
                                        #   in Loop: Header=BB0_49 Depth=1
	slti	$12, $7, 1
	sh	$9, 376($sp)
	addu	$10, $zero, $5
	addu	$9, $zero, $7
	addu	$11, $zero, $4
	bne	$12, $zero, $BB0_119
	nop
$BB0_102:                               # %.lr.ph27.i
                                        #   Parent Loop BB0_49 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	lh	$12, 0($10)
	nop
	lh	$13, 14($8)
	nop
	addiu	$14, $zero, -32768
	bne	$13, $14, $BB0_105
	nop
# BB#103:                               # %.lr.ph27.i
                                        #   in Loop: Header=BB0_102 Depth=2
	andi	$14, $12, 65535
	ori	$15, $zero, 32768
	bne	$14, $15, $BB0_105
	nop
# BB#104:                               #   in Loop: Header=BB0_102 Depth=2
	addiu	$14, $zero, 32767
	j	$BB0_106
	nop
$BB0_105:                               #   in Loop: Header=BB0_102 Depth=2
	mult	$12, $13
	mflo	$13
	sll	$13, $13, 1
	ori	$14, $zero, 32768
	addu	$13, $13, $14
	sra	$14, $13, 16
$BB0_106:                               # %gsm_mult_r.exit6.i
                                        #   in Loop: Header=BB0_102 Depth=2
	lh	$13, 0($11)
	nop
	addu	$14, $13, $14
	slti	$15, $14, -32768
	beq	$15, $zero, $BB0_108
	nop
# BB#107:                               #   in Loop: Header=BB0_102 Depth=2
	ori	$14, $zero, 32768
	j	$BB0_110
	nop
$BB0_108:                               #   in Loop: Header=BB0_102 Depth=2
	addiu	$15, $zero, 32767
	slt	$24, $15, $14
	beq	$24, $zero, $BB0_110
	nop
# BB#109:                               #   in Loop: Header=BB0_102 Depth=2
	addu	$14, $zero, $15
$BB0_110:                               # %gsm_add.exit3.i
                                        #   in Loop: Header=BB0_102 Depth=2
	sh	$14, -2($11)
	lh	$14, 14($8)
	nop
	addiu	$15, $zero, -32768
	bne	$14, $15, $BB0_113
	nop
# BB#111:                               # %gsm_add.exit3.i
                                        #   in Loop: Header=BB0_102 Depth=2
	andi	$15, $13, 65535
	ori	$24, $zero, 32768
	bne	$15, $24, $BB0_113
	nop
# BB#112:                               #   in Loop: Header=BB0_102 Depth=2
	addiu	$13, $zero, 32767
	j	$BB0_114
	nop
$BB0_113:                               #   in Loop: Header=BB0_102 Depth=2
	mult	$13, $14
	mflo	$13
	sll	$13, $13, 1
	ori	$14, $zero, 32768
	addu	$13, $13, $14
	sra	$13, $13, 16
$BB0_114:                               # %gsm_mult_r.exit.i13
                                        #   in Loop: Header=BB0_102 Depth=2
	addu	$12, $12, $13
	slti	$13, $12, -32768
	beq	$13, $zero, $BB0_116
	nop
# BB#115:                               #   in Loop: Header=BB0_102 Depth=2
	ori	$12, $zero, 32768
	j	$BB0_118
	nop
$BB0_116:                               #   in Loop: Header=BB0_102 Depth=2
	addiu	$13, $zero, 32767
	slt	$14, $13, $12
	beq	$14, $zero, $BB0_118
	nop
# BB#117:                               #   in Loop: Header=BB0_102 Depth=2
	addu	$12, $zero, $13
$BB0_118:                               # %gsm_add.exit.i
                                        #   in Loop: Header=BB0_102 Depth=2
	addiu	$9, $9, -1
	addiu	$11, $11, 2
	addiu	$13, $10, 2
	sh	$12, 0($10)
	addu	$10, $zero, $13
	bne	$9, $zero, $BB0_102
	nop
$BB0_119:                               # %._crit_edge.i14
                                        #   in Loop: Header=BB0_49 Depth=1
	addiu	$8, $zero, 8
	slt	$8, $8, $6
	beq	$8, $zero, $BB0_219
	nop
$BB0_120:
	addiu	$3, $zero, 0
$BB0_121:                               # %Reflection_coefficients.exit
                                        # =>This Inner Loop Header: Depth=1
	addiu	$4, $sp, 20
	sll	$5, $3, 1
	subu	$4, $4, $5
	lh	$5, 0($4)
	nop
	addiu	$6, $zero, -1
	slt	$7, $6, $5
	addu	$6, $zero, $5
	bne	$7, $zero, $BB0_124
	nop
# BB#122:                               #   in Loop: Header=BB0_121 Depth=1
	andi	$6, $5, 65535
	ori	$7, $zero, 32768
	beq	$6, $7, $BB0_128
	nop
# BB#123:                               #   in Loop: Header=BB0_121 Depth=1
	addiu	$6, $zero, 0
	subu	$6, $6, $5
$BB0_124:                               # %gsm_abs.exit.i.i
                                        #   in Loop: Header=BB0_121 Depth=1
	sll	$7, $6, 16
	addiu	$8, $zero, 22117
	sra	$7, $7, 16
	slt	$8, $8, $7
	bne	$8, $zero, $BB0_126
	nop
# BB#125:                               #   in Loop: Header=BB0_121 Depth=1
	srl	$6, $7, 1
	j	$BB0_130
	nop
$BB0_126:                               #   in Loop: Header=BB0_121 Depth=1
	addiu	$8, $zero, 31129
	slt	$7, $8, $7
	bne	$7, $zero, $BB0_129
	nop
# BB#127:                               #   in Loop: Header=BB0_121 Depth=1
	addiu	$6, $6, -11059
	j	$BB0_130
	nop
$BB0_128:                               #   in Loop: Header=BB0_121 Depth=1
	addiu	$6, $zero, 32767
$BB0_129:                               # %.thread.i.i
                                        #   in Loop: Header=BB0_121 Depth=1
	sll	$6, $6, 2
	addiu	$6, $6, 26624
$BB0_130:                               #   in Loop: Header=BB0_121 Depth=1
	slti	$5, $5, 0
	beq	$5, $zero, $BB0_132
	nop
# BB#131:                               #   in Loop: Header=BB0_121 Depth=1
	addiu	$5, $zero, 0
	subu	$6, $5, $6
$BB0_132:                               #   in Loop: Header=BB0_121 Depth=1
	addiu	$3, $3, -1
	addiu	$5, $zero, -8
	sh	$6, 0($4)
	bne	$3, $5, $BB0_121
	nop
# BB#133:                               # %Gsm_LPC_Analysis.exit
	lh	$3, 20($sp)
	nop
	ori	$4, $zero, 40960
	mult	$3, $4
	mflo	$3
	sra	$3, $3, 16
	addiu	$4, $3, 256
	slti	$3, $4, -32768
	beq	$3, $zero, $BB0_135
	nop
# BB#134:
	addiu	$3, $zero, 0
	j	$BB0_140
	nop
$BB0_135:
	addiu	$5, $zero, 32767
	addiu	$3, $zero, 63
	slt	$5, $5, $4
	bne	$5, $zero, $BB0_140
	nop
# BB#136:
	sll	$4, $4, 16
	sra	$4, $4, 25
	addiu	$5, $zero, 31
	slt	$5, $5, $4
	bne	$5, $zero, $BB0_140
	nop
# BB#137:
	slti	$3, $4, -32
	beq	$3, $zero, $BB0_139
	nop
# BB#138:
	addiu	$3, $zero, 0
	j	$BB0_140
	nop
$BB0_139:
	addiu	$3, $4, 32
$BB0_140:                               # %.thread.i4
	sh	$3, 20($sp)
	lh	$4, 22($sp)
	nop
	ori	$5, $zero, 40960
	mult	$4, $5
	mflo	$4
	sra	$4, $4, 16
	addiu	$5, $4, 256
	slti	$4, $5, -32768
	beq	$4, $zero, $BB0_142
	nop
# BB#141:
	addiu	$4, $zero, 0
	j	$BB0_147
	nop
$BB0_142:
	addiu	$6, $zero, 32767
	addiu	$4, $zero, 63
	slt	$6, $6, $5
	bne	$6, $zero, $BB0_147
	nop
# BB#143:
	sll	$5, $5, 16
	sra	$5, $5, 25
	addiu	$6, $zero, 31
	slt	$6, $6, $5
	bne	$6, $zero, $BB0_147
	nop
# BB#144:
	slti	$4, $5, -32
	beq	$4, $zero, $BB0_146
	nop
# BB#145:
	addiu	$4, $zero, 0
	j	$BB0_147
	nop
$BB0_146:
	addiu	$4, $5, 32
$BB0_147:                               # %.thread98.i
	sh	$4, 22($sp)
	lh	$5, 24($sp)
	nop
	ori	$6, $zero, 40960
	mult	$5, $6
	mflo	$5
	sra	$5, $5, 16
	addiu	$5, $5, 2048
	slti	$6, $5, -32768
	beq	$6, $zero, $BB0_149
	nop
# BB#148:
	addiu	$5, $zero, 0
	j	$BB0_158
	nop
$BB0_149:
	addiu	$6, $zero, 32767
	slt	$6, $6, $5
	beq	$6, $zero, $BB0_151
	nop
# BB#150:
	addiu	$5, $zero, 31
	j	$BB0_158
	nop
$BB0_151:                               # %gsm_add.exit29.i
	sll	$5, $5, 16
	sra	$5, $5, 16
	addiu	$6, $5, 256
	slti	$5, $6, -32768
	beq	$5, $zero, $BB0_153
	nop
# BB#152:
	addiu	$5, $zero, 0
	j	$BB0_158
	nop
$BB0_153:                               # %gsm_add.exit29.thread.i
	addiu	$7, $zero, 32767
	addiu	$5, $zero, 31
	slt	$7, $7, $6
	bne	$7, $zero, $BB0_158
	nop
# BB#154:                               # %gsm_add.exit29.thread.i
	sll	$6, $6, 16
	sra	$6, $6, 25
	addiu	$7, $zero, 15
	slt	$7, $7, $6
	bne	$7, $zero, $BB0_158
	nop
# BB#155:
	slti	$5, $6, -16
	beq	$5, $zero, $BB0_157
	nop
# BB#156:
	addiu	$5, $zero, 0
	j	$BB0_158
	nop
$BB0_157:
	addiu	$5, $6, 16
$BB0_158:                               # %.thread102.i
	sh	$5, 24($sp)
	lh	$6, 26($sp)
	nop
	ori	$7, $zero, 40960
	mult	$6, $7
	mflo	$6
	sra	$6, $6, 16
	addiu	$6, $6, -2560
	slti	$7, $6, -32768
	beq	$7, $zero, $BB0_160
	nop
# BB#159:
	addiu	$6, $zero, 0
	j	$BB0_169
	nop
$BB0_160:
	addiu	$7, $zero, 32767
	slt	$7, $7, $6
	beq	$7, $zero, $BB0_162
	nop
# BB#161:
	addiu	$6, $zero, 31
	j	$BB0_169
	nop
$BB0_162:                               # %gsm_add.exit25.i
	sll	$6, $6, 16
	sra	$6, $6, 16
	addiu	$7, $6, 256
	slti	$6, $7, -32768
	beq	$6, $zero, $BB0_164
	nop
# BB#163:
	addiu	$6, $zero, 0
	j	$BB0_169
	nop
$BB0_164:                               # %gsm_add.exit25.thread.i
	addiu	$8, $zero, 32767
	addiu	$6, $zero, 31
	slt	$8, $8, $7
	bne	$8, $zero, $BB0_169
	nop
# BB#165:                               # %gsm_add.exit25.thread.i
	sll	$7, $7, 16
	sra	$7, $7, 25
	addiu	$8, $zero, 15
	slt	$8, $8, $7
	bne	$8, $zero, $BB0_169
	nop
# BB#166:
	slti	$6, $7, -16
	beq	$6, $zero, $BB0_168
	nop
# BB#167:
	addiu	$6, $zero, 0
	j	$BB0_169
	nop
$BB0_168:
	addiu	$6, $7, 16
$BB0_169:                               # %.thread107.i
	sh	$6, 26($sp)
	lh	$7, 28($sp)
	nop
	addiu	$8, $zero, 27928
	mult	$7, $8
	mflo	$7
	sra	$7, $7, 16
	addiu	$7, $7, 94
	slti	$8, $7, -32768
	beq	$8, $zero, $BB0_171
	nop
# BB#170:
	addiu	$7, $zero, 0
	j	$BB0_180
	nop
$BB0_171:
	addiu	$8, $zero, 32767
	slt	$8, $8, $7
	beq	$8, $zero, $BB0_173
	nop
# BB#172:
	addiu	$7, $zero, 15
	j	$BB0_180
	nop
$BB0_173:                               # %gsm_add.exit21.i
	sll	$7, $7, 16
	sra	$7, $7, 16
	addiu	$8, $7, 256
	slti	$7, $8, -32768
	beq	$7, $zero, $BB0_175
	nop
# BB#174:
	addiu	$7, $zero, 0
	j	$BB0_180
	nop
$BB0_175:                               # %gsm_add.exit21.thread.i
	addiu	$9, $zero, 32767
	addiu	$7, $zero, 15
	slt	$9, $9, $8
	bne	$9, $zero, $BB0_180
	nop
# BB#176:                               # %gsm_add.exit21.thread.i
	sll	$8, $8, 16
	sra	$8, $8, 25
	addiu	$9, $zero, 7
	slt	$9, $9, $8
	bne	$9, $zero, $BB0_180
	nop
# BB#177:
	slti	$7, $8, -8
	beq	$7, $zero, $BB0_179
	nop
# BB#178:
	addiu	$7, $zero, 0
	j	$BB0_180
	nop
$BB0_179:
	addiu	$7, $8, 8
$BB0_180:                               # %.thread112.i
	sh	$7, 28($sp)
	lh	$8, 30($sp)
	nop
	addiu	$9, $zero, 30720
	mult	$8, $9
	mflo	$8
	sra	$8, $8, 16
	addiu	$8, $8, -1792
	slti	$9, $8, -32768
	beq	$9, $zero, $BB0_182
	nop
# BB#181:
	addiu	$8, $zero, 0
	j	$BB0_191
	nop
$BB0_182:
	addiu	$9, $zero, 32767
	slt	$9, $9, $8
	beq	$9, $zero, $BB0_184
	nop
# BB#183:
	addiu	$8, $zero, 15
	j	$BB0_191
	nop
$BB0_184:                               # %gsm_add.exit17.i
	sll	$8, $8, 16
	sra	$8, $8, 16
	addiu	$9, $8, 256
	slti	$8, $9, -32768
	beq	$8, $zero, $BB0_186
	nop
# BB#185:
	addiu	$8, $zero, 0
	j	$BB0_191
	nop
$BB0_186:                               # %gsm_add.exit17.thread.i
	addiu	$10, $zero, 32767
	addiu	$8, $zero, 15
	slt	$10, $10, $9
	bne	$10, $zero, $BB0_191
	nop
# BB#187:                               # %gsm_add.exit17.thread.i
	sll	$9, $9, 16
	sra	$9, $9, 25
	addiu	$10, $zero, 7
	slt	$10, $10, $9
	bne	$10, $zero, $BB0_191
	nop
# BB#188:
	slti	$8, $9, -8
	beq	$8, $zero, $BB0_190
	nop
# BB#189:
	addiu	$8, $zero, 0
	j	$BB0_191
	nop
$BB0_190:
	addiu	$8, $9, 8
$BB0_191:                               # %.thread117.i
	sh	$8, 30($sp)
	lh	$9, 32($sp)
	nop
	addiu	$10, $zero, 17068
	mult	$9, $10
	mflo	$9
	sra	$9, $9, 16
	addiu	$9, $9, -341
	slti	$10, $9, -32768
	beq	$10, $zero, $BB0_193
	nop
# BB#192:
	addiu	$9, $zero, 0
	j	$BB0_202
	nop
$BB0_193:
	addiu	$10, $zero, 32767
	slt	$10, $10, $9
	beq	$10, $zero, $BB0_195
	nop
# BB#194:
	addiu	$9, $zero, 7
	j	$BB0_202
	nop
$BB0_195:                               # %gsm_add.exit13.i
	sll	$9, $9, 16
	sra	$9, $9, 16
	addiu	$10, $9, 256
	slti	$9, $10, -32768
	beq	$9, $zero, $BB0_197
	nop
# BB#196:
	addiu	$9, $zero, 0
	j	$BB0_202
	nop
$BB0_197:                               # %gsm_add.exit13.thread.i
	addiu	$11, $zero, 32767
	addiu	$9, $zero, 7
	slt	$11, $11, $10
	bne	$11, $zero, $BB0_202
	nop
# BB#198:                               # %gsm_add.exit13.thread.i
	sll	$10, $10, 16
	sra	$10, $10, 25
	addiu	$11, $zero, 3
	slt	$11, $11, $10
	bne	$11, $zero, $BB0_202
	nop
# BB#199:
	slti	$9, $10, -4
	beq	$9, $zero, $BB0_201
	nop
# BB#200:
	addiu	$9, $zero, 0
	j	$BB0_202
	nop
$BB0_201:
	addiu	$9, $10, 4
$BB0_202:                               # %.thread122.i
	sh	$9, 32($sp)
	lh	$10, 34($sp)
	nop
	addiu	$11, $zero, 18072
	mult	$10, $11
	mflo	$10
	sra	$10, $10, 16
	addiu	$10, $10, -1144
	slti	$11, $10, -32768
	beq	$11, $zero, $BB0_204
	nop
# BB#203:
	addiu	$10, $zero, 0
	j	$BB0_213
	nop
$BB0_204:
	addiu	$11, $zero, 32767
	slt	$11, $11, $10
	beq	$11, $zero, $BB0_206
	nop
# BB#205:
	addiu	$10, $zero, 7
	j	$BB0_213
	nop
$BB0_206:                               # %gsm_add.exit9.i
	sll	$10, $10, 16
	sra	$10, $10, 16
	addiu	$11, $10, 256
	slti	$10, $11, -32768
	beq	$10, $zero, $BB0_208
	nop
# BB#207:
	addiu	$10, $zero, 0
	j	$BB0_213
	nop
$BB0_208:                               # %gsm_add.exit9.thread.i
	addiu	$12, $zero, 32767
	addiu	$10, $zero, 7
	slt	$12, $12, $11
	bne	$12, $zero, $BB0_213
	nop
# BB#209:                               # %gsm_add.exit9.thread.i
	sll	$11, $11, 16
	sra	$11, $11, 25
	addiu	$12, $zero, 3
	slt	$12, $12, $11
	bne	$12, $zero, $BB0_213
	nop
# BB#210:
	slti	$10, $11, -4
	beq	$10, $zero, $BB0_212
	nop
# BB#211:
	addiu	$10, $zero, 0
	j	$BB0_213
	nop
$BB0_212:
	addiu	$10, $11, 4
$BB0_213:                               # %Quantization_and_coding.exit
	addiu	$12, $zero, 0
	sh	$10, 34($sp)
	addu	$11, $zero, $12
$BB0_214:                               # =>This Inner Loop Header: Depth=1
	lui	$14, %hi(outData)
	sll	$13, $12, 1
	addiu	$14, $14, %lo(outData)
	subu	$15, $2, $13
	subu	$14, $14, $13
	lhu	$13, 0($15)
	nop
	lhu	$14, 0($14)
	nop
	xor	$13, $13, $14
	sltu	$13, $13, 1
	addiu	$12, $12, -1
	addu	$11, $13, $11
	addiu	$13, $zero, -160
	bne	$12, $13, $BB0_214
	nop
# BB#215:                               # %.preheader
	andi	$2, $3, 65535
	addiu	$3, $zero, 32
	xor	$3, $2, $3
	andi	$2, $4, 65535
	addiu	$4, $zero, 33
	xor	$2, $2, $4
	andi	$5, $5, 65535
	addiu	$12, $zero, 22
	sltu	$4, $3, 1
	xor	$3, $5, $12
	andi	$12, $6, 65535
	addiu	$13, $zero, 13
	sltu	$5, $2, 1
	addu	$6, $4, $11
	xor	$2, $12, $13
	andi	$7, $7, 65535
	addiu	$11, $zero, 7
	sltu	$4, $3, 1
	addu	$6, $5, $6
	xor	$3, $7, $11
	andi	$7, $8, 65535
	addiu	$8, $zero, 5
	sltu	$5, $2, 1
	addu	$6, $4, $6
	xor	$2, $7, $8
	andi	$7, $9, 65535
	addiu	$8, $zero, 3
	sltu	$4, $3, 1
	addu	$5, $5, $6
	xor	$3, $7, $8
	andi	$6, $10, 65535
	addiu	$7, $zero, 2
	sltu	$2, $2, 1
	addu	$5, $4, $5
	xor	$4, $6, $7
	sltu	$3, $3, 1
	addu	$5, $2, $5
	lui	$2, %hi($.str)
	sltu	$4, $4, 1
	addu	$3, $3, $5
	addu	$16, $4, $3
	addiu	$4, $2, %lo($.str)
	addiu	$17, $zero, 168
	addu	$5, $zero, $16
	jal	printf
	nop
	bne	$16, $17, $BB0_217
	nop
# BB#216:
	lui	$2, %hi($.str1)
	addiu	$4, $2, %lo($.str1)
	j	$BB0_218
	nop
$BB0_217:
	lui	$2, %hi($.str2)
	addiu	$4, $2, %lo($.str2)
$BB0_218:
	jal	printf
	nop
	addu	$2, $zero, $16
	lw	$16, 396($sp)
	nop
	lw	$17, 400($sp)
	nop
	lw	$18, 404($sp)
	nop
	lw	$19, 408($sp)
	nop
	lw	$20, 412($sp)
	nop
	lw	$21, 416($sp)
	nop
	lw	$22, 420($sp)
	nop
	lw	$23, 424($sp)
	nop
	lw	$ra, 428($sp)
	nop
	addiu	$sp, $sp, 432
	jr	$ra
	nop
$BB0_219:                               # %._crit_edge43.i
                                        #   in Loop: Header=BB0_49 Depth=1
	lhu	$10, 378($sp)
	nop
	addiu	$7, $7, -1
	addiu	$6, $6, 1
	addiu	$3, $3, 2
	j	$BB0_49
	nop
	.set	macro
	.set	reorder
	.end	main
$tmp13:
	.size	main, ($tmp13)-main
$tmp14:
	.cfi_endproc
$eh_func_end0:

	.type	bitoff,@object          # @bitoff
	.section	.rodata,"a",@progbits
bitoff:
	.asciz	 "\b\007\006\006\005\005\005\005\004\004\004\004\004\004\004\004\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\003\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\002\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\001\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000\000"
	.size	bitoff, 256

	.type	inData,@object          # @inData
	.align	1
inData:
	.2byte	81                      # 0x51
	.2byte	10854                   # 0x2a66
	.2byte	1893                    # 0x765
	.2byte	55245                   # 0xd7cd
	.2byte	7614                    # 0x1dbe
	.2byte	29718                   # 0x7416
	.2byte	20475                   # 0x4ffb
	.2byte	36321                   # 0x8de1
	.2byte	46587                   # 0xb5fb
	.2byte	35730                   # 0x8b92
	.2byte	33519                   # 0x82ef
	.2byte	1596                    # 0x63c
	.2byte	15744                   # 0x3d80
	.2byte	62448                   # 0xf3f0
	.2byte	48123                   # 0xbbfb
	.2byte	43413                   # 0xa995
	.2byte	6798                    # 0x1a8e
	.2byte	52260                   # 0xcc24
	.2byte	3819                    # 0xeeb
	.2byte	49263                   # 0xc06f
	.2byte	63963                   # 0xf9db
	.2byte	53013                   # 0xcf15
	.2byte	38433                   # 0x9621
	.2byte	65343                   # 0xff3f
	.2byte	39948                   # 0x9c0c
	.2byte	4698                    # 0x125a
	.2byte	35100                   # 0x891c
	.2byte	15264                   # 0x3ba0
	.2byte	64143                   # 0xfa8f
	.2byte	11418                   # 0x2c9a
	.2byte	11370                   # 0x2c6a
	.2byte	4986                    # 0x137a
	.2byte	7869                    # 0x1ebd
	.2byte	63633                   # 0xf891
	.2byte	9123                    # 0x23a3
	.2byte	33810                   # 0x8412
	.2byte	40299                   # 0x9d6b
	.2byte	51381                   # 0xc8b5
	.2byte	17982                   # 0x463e
	.2byte	32427                   # 0x7eab
	.2byte	53097                   # 0xcf69
	.2byte	49605                   # 0xc1c5
	.2byte	43914                   # 0xab8a
	.2byte	7896                    # 0x1ed8
	.2byte	1689                    # 0x699
	.2byte	28113                   # 0x6dd1
	.2byte	3615                    # 0xe1f
	.2byte	22131                   # 0x5673
	.2byte	59964                   # 0xea3c
	.2byte	45426                   # 0xb172
	.2byte	12387                   # 0x3063
	.2byte	9177                    # 0x23d9
	.2byte	40992                   # 0xa020
	.2byte	12480                   # 0x30c0
	.2byte	21546                   # 0x542a
	.2byte	47694                   # 0xba4e
	.2byte	51891                   # 0xcab3
	.2byte	20277                   # 0x4f35
	.2byte	9987                    # 0x2703
	.2byte	17652                   # 0x44f4
	.2byte	54072                   # 0xd338
	.2byte	48210                   # 0xbc52
	.2byte	54984                   # 0xd6c8
	.2byte	38436                   # 0x9624
	.2byte	207                     # 0xcf
	.2byte	27612                   # 0x6bdc
	.2byte	2517                    # 0x9d5
	.2byte	7167                    # 0x1bff
	.2byte	35802                   # 0x8bda
	.2byte	43095                   # 0xa857
	.2byte	30039                   # 0x7557
	.2byte	63168                   # 0xf6c0
	.2byte	12813                   # 0x320d
	.2byte	300                     # 0x12c
	.2byte	39981                   # 0x9c2d
	.2byte	9087                    # 0x237f
	.2byte	29022                   # 0x715e
	.2byte	58977                   # 0xe661
	.2byte	45225                   # 0xb0a9
	.2byte	51189                   # 0xc7f5
	.2byte	57981                   # 0xe27d
	.2byte	43827                   # 0xab33
	.2byte	61860                   # 0xf1a4
	.2byte	35454                   # 0x8a7e
	.2byte	62346                   # 0xf38a
	.2byte	34557                   # 0x86fd
	.2byte	8580                    # 0x2184
	.2byte	27126                   # 0x69f6
	.2byte	3414                    # 0xd56
	.2byte	60933                   # 0xee05
	.2byte	43233                   # 0xa8e1
	.2byte	48393                   # 0xbd09
	.2byte	13788                   # 0x35dc
	.2byte	64440                   # 0xfbb8
	.2byte	50919                   # 0xc6e7
	.2byte	22071                   # 0x5637
	.2byte	51984                   # 0xcb10
	.2byte	32646                   # 0x7f86
	.2byte	16689                   # 0x4131
	.2byte	57063                   # 0xdee7
	.2byte	52803                   # 0xce43
	.2byte	10503                   # 0x2907
	.2byte	20745                   # 0x5109
	.2byte	6696                    # 0x1a28
	.2byte	38694                   # 0x9726
	.2byte	34521                   # 0x86d9
	.2byte	3792                    # 0xed0
	.2byte	45672                   # 0xb268
	.2byte	45105                   # 0xb031
	.2byte	35229                   # 0x899d
	.2byte	32421                   # 0x7ea5
	.2byte	52299                   # 0xcc4b
	.2byte	9006                    # 0x232e
	.2byte	18249                   # 0x4749
	.2byte	2403                    # 0x963
	.2byte	57540                   # 0xe0c4
	.2byte	50709                   # 0xc615
	.2byte	59676                   # 0xe91c
	.2byte	7122                    # 0x1bd2
	.2byte	29817                   # 0x7479
	.2byte	33642                   # 0x836a
	.2byte	17955                   # 0x4623
	.2byte	28836                   # 0x70a4
	.2byte	34239                   # 0x85bf
	.2byte	31821                   # 0x7c4d
	.2byte	38034                   # 0x9492
	.2byte	12276                   # 0x2ff4
	.2byte	59949                   # 0xea2d
	.2byte	43431                   # 0xa9a7
	.2byte	9192                    # 0x23e8
	.2byte	42987                   # 0xa7eb
	.2byte	15675                   # 0x3d3b
	.2byte	53271                   # 0xd017
	.2byte	7212                    # 0x1c2c
	.2byte	41787                   # 0xa33b
	.2byte	52680                   # 0xcdc8
	.2byte	59679                   # 0xe91f
	.2byte	7521                    # 0x1d61
	.2byte	17349                   # 0x43c5
	.2byte	13773                   # 0x35cd
	.2byte	62445                   # 0xf3ed
	.2byte	47724                   # 0xba6c
	.2byte	55881                   # 0xda49
	.2byte	26667                   # 0x682b
	.2byte	7902                    # 0x1ede
	.2byte	2487                    # 0x9b7
	.2byte	3177                    # 0xc69
	.2byte	29412                   # 0x72e4
	.2byte	45312                   # 0xb100
	.2byte	62760                   # 0xf528
	.2byte	24084                   # 0x5e14
	.2byte	57573                   # 0xe0e5
	.2byte	55098                   # 0xd73a
	.2byte	53598                   # 0xd15e
	.2byte	50703                   # 0xc60f
	.2byte	58878                   # 0xe5fe
	.2byte	32058                   # 0x7d3a
	.2byte	4020                    # 0xfb4
	.2byte	10461                   # 0x28dd
	.2byte	15159                   # 0x3b37
	.size	inData, 320

	.type	outData,@object         # @outData
	.align	1
outData:
	.2byte	80                      # 0x50
	.2byte	10848                   # 0x2a60
	.2byte	1888                    # 0x760
	.2byte	55248                   # 0xd7d0
	.2byte	7616                    # 0x1dc0
	.2byte	29712                   # 0x7410
	.2byte	20480                   # 0x5000
	.2byte	36320                   # 0x8de0
	.2byte	46592                   # 0xb600
	.2byte	35728                   # 0x8b90
	.2byte	33520                   # 0x82f0
	.2byte	1600                    # 0x640
	.2byte	15744                   # 0x3d80
	.2byte	62448                   # 0xf3f0
	.2byte	48128                   # 0xbc00
	.2byte	43408                   # 0xa990
	.2byte	6800                    # 0x1a90
	.2byte	52256                   # 0xcc20
	.2byte	3824                    # 0xef0
	.2byte	49264                   # 0xc070
	.2byte	63968                   # 0xf9e0
	.2byte	53008                   # 0xcf10
	.2byte	38432                   # 0x9620
	.2byte	65344                   # 0xff40
	.2byte	39952                   # 0x9c10
	.2byte	4704                    # 0x1260
	.2byte	35104                   # 0x8920
	.2byte	15264                   # 0x3ba0
	.2byte	64144                   # 0xfa90
	.2byte	11424                   # 0x2ca0
	.2byte	11376                   # 0x2c70
	.2byte	4992                    # 0x1380
	.2byte	7872                    # 0x1ec0
	.2byte	63632                   # 0xf890
	.2byte	9120                    # 0x23a0
	.2byte	33808                   # 0x8410
	.2byte	40304                   # 0x9d70
	.2byte	51376                   # 0xc8b0
	.2byte	17984                   # 0x4640
	.2byte	32432                   # 0x7eb0
	.2byte	53104                   # 0xcf70
	.2byte	49600                   # 0xc1c0
	.2byte	43920                   # 0xab90
	.2byte	7904                    # 0x1ee0
	.2byte	1696                    # 0x6a0
	.2byte	28112                   # 0x6dd0
	.2byte	3616                    # 0xe20
	.2byte	22128                   # 0x5670
	.2byte	59968                   # 0xea40
	.2byte	45424                   # 0xb170
	.2byte	12384                   # 0x3060
	.2byte	9184                    # 0x23e0
	.2byte	40992                   # 0xa020
	.2byte	12480                   # 0x30c0
	.2byte	21552                   # 0x5430
	.2byte	47696                   # 0xba50
	.2byte	51888                   # 0xcab0
	.2byte	20272                   # 0x4f30
	.2byte	9984                    # 0x2700
	.2byte	17648                   # 0x44f0
	.2byte	54080                   # 0xd340
	.2byte	48208                   # 0xbc50
	.2byte	54992                   # 0xd6d0
	.2byte	38432                   # 0x9620
	.2byte	208                     # 0xd0
	.2byte	27616                   # 0x6be0
	.2byte	2512                    # 0x9d0
	.2byte	7168                    # 0x1c00
	.2byte	35808                   # 0x8be0
	.2byte	43088                   # 0xa850
	.2byte	30032                   # 0x7550
	.2byte	63168                   # 0xf6c0
	.2byte	12816                   # 0x3210
	.2byte	304                     # 0x130
	.2byte	39984                   # 0x9c30
	.2byte	9088                    # 0x2380
	.2byte	29024                   # 0x7160
	.2byte	58976                   # 0xe660
	.2byte	45232                   # 0xb0b0
	.2byte	51184                   # 0xc7f0
	.2byte	57984                   # 0xe280
	.2byte	43824                   # 0xab30
	.2byte	61856                   # 0xf1a0
	.2byte	35456                   # 0x8a80
	.2byte	62352                   # 0xf390
	.2byte	34560                   # 0x8700
	.2byte	8576                    # 0x2180
	.2byte	27120                   # 0x69f0
	.2byte	3408                    # 0xd50
	.2byte	60928                   # 0xee00
	.2byte	43232                   # 0xa8e0
	.2byte	48400                   # 0xbd10
	.2byte	13792                   # 0x35e0
	.2byte	64448                   # 0xfbc0
	.2byte	50912                   # 0xc6e0
	.2byte	22064                   # 0x5630
	.2byte	51984                   # 0xcb10
	.2byte	32640                   # 0x7f80
	.2byte	16688                   # 0x4130
	.2byte	57056                   # 0xdee0
	.2byte	52800                   # 0xce40
	.2byte	10496                   # 0x2900
	.2byte	20752                   # 0x5110
	.2byte	6704                    # 0x1a30
	.2byte	38688                   # 0x9720
	.2byte	34528                   # 0x86e0
	.2byte	3792                    # 0xed0
	.2byte	45680                   # 0xb270
	.2byte	45104                   # 0xb030
	.2byte	35232                   # 0x89a0
	.2byte	32416                   # 0x7ea0
	.2byte	52304                   # 0xcc50
	.2byte	9008                    # 0x2330
	.2byte	18256                   # 0x4750
	.2byte	2400                    # 0x960
	.2byte	57536                   # 0xe0c0
	.2byte	50704                   # 0xc610
	.2byte	59680                   # 0xe920
	.2byte	7120                    # 0x1bd0
	.2byte	29824                   # 0x7480
	.2byte	33648                   # 0x8370
	.2byte	17952                   # 0x4620
	.2byte	28832                   # 0x70a0
	.2byte	34240                   # 0x85c0
	.2byte	31824                   # 0x7c50
	.2byte	38032                   # 0x9490
	.2byte	12272                   # 0x2ff0
	.2byte	59952                   # 0xea30
	.2byte	43424                   # 0xa9a0
	.2byte	9200                    # 0x23f0
	.2byte	42992                   # 0xa7f0
	.2byte	15680                   # 0x3d40
	.2byte	53264                   # 0xd010
	.2byte	7216                    # 0x1c30
	.2byte	41792                   # 0xa340
	.2byte	52688                   # 0xcdd0
	.2byte	59680                   # 0xe920
	.2byte	7520                    # 0x1d60
	.2byte	17344                   # 0x43c0
	.2byte	13776                   # 0x35d0
	.2byte	62448                   # 0xf3f0
	.2byte	47728                   # 0xba70
	.2byte	55888                   # 0xda50
	.2byte	26672                   # 0x6830
	.2byte	7904                    # 0x1ee0
	.2byte	2480                    # 0x9b0
	.2byte	3184                    # 0xc70
	.2byte	29408                   # 0x72e0
	.2byte	45312                   # 0xb100
	.2byte	62768                   # 0xf530
	.2byte	24080                   # 0x5e10
	.2byte	57568                   # 0xe0e0
	.2byte	55104                   # 0xd740
	.2byte	53600                   # 0xd160
	.2byte	50704                   # 0xc610
	.2byte	58880                   # 0xe600
	.2byte	32064                   # 0x7d40
	.2byte	4016                    # 0xfb0
	.2byte	10464                   # 0x28e0
	.2byte	15152                   # 0x3b30
	.size	outData, 320

	.type	$.str,@object           # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
$.str:
	.asciz	 "Result: %d\n"
	.size	$.str, 12

	.type	$.str1,@object          # @.str1
$.str1:
	.asciz	 "RESULT: PASS\n"
	.size	$.str1, 14

	.type	$.str2,@object          # @.str2
$.str2:
	.asciz	 "RESULT: FAIL\n"
	.size	$.str2, 14


