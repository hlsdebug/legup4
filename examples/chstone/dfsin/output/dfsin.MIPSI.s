	.section .mdebug.abi32
	.previous
	.file	"output/dfsin.sw.ll"
	.text
	.align	2
	.type	float64_mul,@function
	.ent	float64_mul             # @float64_mul
float64_mul:
$tmp2:
	.cfi_startproc
	.frame	$sp,56,$ra
	.mask 	0x80FF0000,-4
	.fmask	0x00000000,0
# BB#0:
	.set	noreorder
	.set	nomacro
	addiu	$sp, $sp, -56
$tmp3:
	.cfi_def_cfa_offset 56
	sw	$ra, 52($sp)
	sw	$23, 48($sp)
	sw	$22, 44($sp)
	sw	$21, 40($sp)
	sw	$20, 36($sp)
	sw	$19, 32($sp)
	sw	$18, 28($sp)
	sw	$17, 24($sp)
	sw	$16, 20($sp)
$tmp4:
	.cfi_offset 31, -4
$tmp5:
	.cfi_offset 23, -8
$tmp6:
	.cfi_offset 22, -12
$tmp7:
	.cfi_offset 21, -16
$tmp8:
	.cfi_offset 20, -20
$tmp9:
	.cfi_offset 19, -24
$tmp10:
	.cfi_offset 18, -28
$tmp11:
	.cfi_offset 17, -32
$tmp12:
	.cfi_offset 16, -36
	addu	$17, $zero, $6
	addu	$16, $zero, $4
	lui	$2, 15
	ori	$2, $2, 65535
	srl	$4, $5, 20
	srl	$3, $7, 20
	xor	$6, $7, $5
	srl	$18, $6, 31
	andi	$22, $4, 2047
	andi	$21, $3, 2047
	and	$20, $5, $2
	and	$19, $7, $2
	addiu	$2, $zero, 2047
	bne	$22, $2, $BB0_21
	nop
# BB#1:
	or	$2, $16, $20
	bne	$2, $zero, $BB0_4
	nop
# BB#2:
	addiu	$2, $zero, 2047
	bne	$21, $2, $BB0_19
	nop
# BB#3:
	or	$2, $17, $19
	beq	$2, $zero, $BB0_19
	nop
$BB0_4:
	lui	$3, 32760
	lui	$2, 32752
	ori	$3, $3, 0
	and	$3, $5, $3
	ori	$2, $2, 0
	xor	$2, $3, $2
	beq	$2, $zero, $BB0_6
	nop
# BB#5:
	addiu	$2, $zero, 0
	j	$BB0_7
	nop
$BB0_6:
	lui	$2, 7
	ori	$2, $2, 65535
	and	$2, $5, $2
	or	$2, $16, $2
	addiu	$3, $zero, 0
	xor	$2, $2, $3
	sltu	$2, $zero, $2
$BB0_7:                                 # %float64_is_signaling_nan.exit1.i10
	lui	$3, 32767
	addiu	$4, $zero, 0
	lui	$6, 32752
	ori	$3, $3, 65535
	lui	$8, 32760
	ori	$6, $6, 0
	and	$3, $7, $3
	ori	$8, $8, 0
	xor	$9, $3, $6
	beq	$9, $zero, $BB0_9
	nop
# BB#8:
	sltu	$3, $6, $3
	j	$BB0_10
	nop
$BB0_9:                                 # %float64_is_signaling_nan.exit1.i10
	xor	$3, $17, $4
	sltu	$3, $zero, $3
$BB0_10:                                # %float64_is_signaling_nan.exit1.i10
	and	$8, $7, $8
	xor	$6, $8, $6
	bne	$6, $zero, $BB0_12
	nop
# BB#11:
	lui	$4, 7
	ori	$4, $4, 65535
	and	$4, $7, $4
	or	$4, $17, $4
	addiu	$6, $zero, 0
	xor	$4, $4, $6
	sltu	$4, $zero, $4
$BB0_12:                                # %float64_is_signaling_nan.exit.i11
	or	$2, $2, $4
	addiu	$6, $zero, 1
	bne	$2, $6, $BB0_14
	nop
# BB#13:                                # %.thread.i12
	lui	$2, %hi(float_exception_flags)
	lw	$3, %lo(float_exception_flags)($2)
	nop
	ori	$3, $3, 16
	sw	$3, %lo(float_exception_flags)($2)
	addu	$3, $zero, $4
$BB0_14:                                # %propagateFloat64NaN.exit16
	lui	$2, 8
	beq	$3, $zero, $BB0_16
	nop
# BB#15:                                # %propagateFloat64NaN.exit16
	addu	$5, $zero, $7
$BB0_16:                                # %propagateFloat64NaN.exit16
	beq	$3, $zero, $BB0_18
	nop
$BB0_17:                                # %propagateFloat64NaN.exit16
	addu	$16, $zero, $17
$BB0_18:                                # %propagateFloat64NaN.exit16
	ori	$2, $2, 0
	or	$3, $5, $2
	j	$BB0_65
	nop
$BB0_19:
	or	$2, $21, $17
	or	$2, $19, $2
	bne	$2, $zero, $BB0_37
	nop
$BB0_20:
	lui	$2, %hi(float_exception_flags)
	lw	$4, %lo(float_exception_flags)($2)
	nop
	lui	$3, 32767
	ori	$4, $4, 16
	addiu	$16, $zero, -1
	ori	$3, $3, 65535
	sw	$4, %lo(float_exception_flags)($2)
	j	$BB0_65
	nop
$BB0_21:
	addiu	$2, $zero, 2047
	bne	$21, $2, $BB0_38
	nop
# BB#22:
	or	$2, $17, $19
	beq	$2, $zero, $BB0_36
	nop
# BB#23:
	lui	$3, 32760
	lui	$2, 32752
	ori	$3, $3, 0
	and	$3, $5, $3
	ori	$2, $2, 0
	xor	$2, $3, $2
	beq	$2, $zero, $BB0_25
	nop
# BB#24:
	addiu	$2, $zero, 0
	j	$BB0_26
	nop
$BB0_25:
	lui	$2, 7
	ori	$2, $2, 65535
	and	$2, $5, $2
	or	$2, $16, $2
	addiu	$3, $zero, 0
	xor	$2, $2, $3
	sltu	$2, $zero, $2
$BB0_26:                                # %float64_is_signaling_nan.exit1.i
	lui	$3, 32767
	addiu	$4, $zero, 0
	lui	$6, 32752
	ori	$3, $3, 65535
	lui	$8, 32760
	ori	$6, $6, 0
	and	$3, $7, $3
	ori	$8, $8, 0
	xor	$9, $3, $6
	beq	$9, $zero, $BB0_28
	nop
# BB#27:
	sltu	$3, $6, $3
	j	$BB0_29
	nop
$BB0_28:                                # %float64_is_signaling_nan.exit1.i
	xor	$3, $17, $4
	sltu	$3, $zero, $3
$BB0_29:                                # %float64_is_signaling_nan.exit1.i
	and	$8, $7, $8
	xor	$6, $8, $6
	bne	$6, $zero, $BB0_31
	nop
# BB#30:
	lui	$4, 7
	ori	$4, $4, 65535
	and	$4, $7, $4
	or	$4, $17, $4
	addiu	$6, $zero, 0
	xor	$4, $4, $6
	sltu	$4, $zero, $4
$BB0_31:                                # %float64_is_signaling_nan.exit.i
	or	$2, $2, $4
	addiu	$6, $zero, 1
	bne	$2, $6, $BB0_33
	nop
# BB#32:                                # %.thread.i
	lui	$2, %hi(float_exception_flags)
	lw	$3, %lo(float_exception_flags)($2)
	nop
	ori	$3, $3, 16
	sw	$3, %lo(float_exception_flags)($2)
	addu	$3, $zero, $4
$BB0_33:                                # %propagateFloat64NaN.exit
	lui	$2, 8
	beq	$3, $zero, $BB0_35
	nop
# BB#34:                                # %propagateFloat64NaN.exit
	addu	$5, $zero, $7
$BB0_35:                                # %propagateFloat64NaN.exit
	bne	$3, $zero, $BB0_17
	nop
	j	$BB0_18
	nop
$BB0_36:
	or	$2, $22, $16
	or	$2, $20, $2
	beq	$2, $zero, $BB0_20
	nop
$BB0_37:
	lui	$3, 32752
	sll	$2, $18, 31
	ori	$3, $3, 0
	addiu	$16, $zero, 0
	or	$3, $2, $3
	j	$BB0_65
	nop
$BB0_38:
	bne	$22, $zero, $BB0_48
	nop
# BB#39:
	or	$2, $16, $20
	bne	$2, $zero, $BB0_41
	nop
$BB0_40:
	addiu	$16, $zero, 0
	sll	$3, $18, 31
	j	$BB0_65
	nop
$BB0_41:
	lui	$3, 1
	addu	$2, $zero, $20
	bne	$20, $zero, $BB0_43
	nop
# BB#42:
	addu	$2, $zero, $16
$BB0_43:
	ori	$3, $3, 0
	lui	$4, 255
	sltu	$3, $2, $3
	beq	$3, $zero, $BB0_45
	nop
# BB#44:
	sll	$2, $2, 16
$BB0_45:
	ori	$4, $4, 65535
	sll	$3, $3, 4
	sltu	$4, $4, $2
	bne	$4, $zero, $BB0_47
	nop
# BB#46:
	ori	$3, $3, 8
	sll	$2, $2, 8
$BB0_47:                                # %normalizeFloat64Subnormal.exit9
	addiu	$4, $zero, 0
	xor	$4, $20, $4
	sltu	$4, $4, 1
	sll	$4, $4, 5
	lui	$6, %hi(countLeadingZeros32.countLeadingZerosHigh)
	srl	$5, $2, 22
	addiu	$2, $6, %lo(countLeadingZeros32.countLeadingZerosHigh)
	andi	$5, $5, 1020
	addu	$5, $2, $5
	addu	$2, $3, $4
	lw	$3, 0($5)
	nop
	addu	$22, $2, $3
	addiu	$23, $zero, 12
	addiu	$6, $22, -11
	addu	$4, $zero, $16
	addu	$5, $zero, $20
	jal	__ashldi3
	nop
	addu	$16, $zero, $2
	addu	$20, $zero, $3
	subu	$22, $23, $22
$BB0_48:
	bne	$21, $zero, $BB0_57
	nop
# BB#49:
	or	$2, $17, $19
	beq	$2, $zero, $BB0_40
	nop
# BB#50:
	lui	$3, 1
	addu	$2, $zero, $19
	bne	$19, $zero, $BB0_52
	nop
# BB#51:
	addu	$2, $zero, $17
$BB0_52:
	ori	$3, $3, 0
	lui	$4, 255
	sltu	$3, $2, $3
	beq	$3, $zero, $BB0_54
	nop
# BB#53:
	sll	$2, $2, 16
$BB0_54:
	ori	$4, $4, 65535
	sll	$3, $3, 4
	sltu	$4, $4, $2
	bne	$4, $zero, $BB0_56
	nop
# BB#55:
	ori	$3, $3, 8
	sll	$2, $2, 8
$BB0_56:                                # %normalizeFloat64Subnormal.exit
	addiu	$4, $zero, 0
	xor	$4, $19, $4
	sltu	$4, $4, 1
	sll	$4, $4, 5
	lui	$6, %hi(countLeadingZeros32.countLeadingZerosHigh)
	srl	$5, $2, 22
	addiu	$2, $6, %lo(countLeadingZeros32.countLeadingZerosHigh)
	andi	$5, $5, 1020
	addu	$5, $2, $5
	addu	$2, $3, $4
	lw	$3, 0($5)
	nop
	addu	$21, $2, $3
	addiu	$23, $zero, 12
	addiu	$6, $21, -11
	addu	$4, $zero, $17
	addu	$5, $zero, $19
	jal	__ashldi3
	nop
	addu	$17, $zero, $2
	addu	$19, $zero, $3
	subu	$21, $23, $21
$BB0_57:
	lui	$3, 32768
	lui	$2, 16384
	srl	$4, $17, 21
	sll	$6, $19, 11
	srl	$5, $16, 22
	sll	$7, $20, 10
	or	$4, $4, $6
	ori	$6, $3, 0
	or	$3, $5, $7
	ori	$5, $2, 0
	or	$2, $4, $6
	sll	$4, $16, 10
	sll	$7, $17, 11
	or	$3, $3, $5
	multu	$2, $4
	mflo	$9
	mfhi	$6
	multu	$7, $3
	mflo	$8
	mfhi	$5
	addu	$10, $9, $8
	multu	$7, $4
	mflo	$7
	mfhi	$4
	addu	$9, $10, $4
	sltu	$8, $10, $8
	sltu	$10, $9, $4
	addiu	$4, $zero, 0
	addu	$10, $10, $4
	addu	$11, $8, $5
	or	$8, $7, $9
	addu	$7, $6, $11
	addu	$6, $4, $10
	multu	$2, $3
	mflo	$3
	mfhi	$2
	addu	$9, $6, $3
	xor	$8, $8, $4
	sltu	$6, $7, $5
	sltu	$5, $9, $3
	addu	$3, $9, $7
	sltu	$8, $zero, $8
	addu	$9, $6, $4
	or	$6, $3, $8
	addu	$8, $5, $4
	sltu	$5, $3, $7
	addu	$4, $4, $9
	addu	$3, $6, $6
	addu	$2, $2, $8
	addu	$5, $5, $4
	sltu	$4, $3, $6
	addu	$7, $2, $5
	addu	$2, $4, $7
	addu	$2, $7, $2
	addiu	$4, $zero, -1
	slt	$5, $4, $2
	bne	$5, $zero, $BB0_59
	nop
# BB#58:
	addiu	$4, $zero, -1023
	j	$BB0_60
	nop
$BB0_59:
	addiu	$4, $zero, -1024
$BB0_60:
	beq	$5, $zero, $BB0_62
	nop
# BB#61:
	addu	$6, $zero, $3
$BB0_62:
	beq	$5, $zero, $BB0_64
	nop
# BB#63:
	addu	$7, $zero, $2
$BB0_64:
	addu	$2, $21, $22
	addu	$5, $2, $4
	addu	$4, $zero, $18
	jal	roundAndPackFloat64
	nop
	addu	$16, $zero, $2
$BB0_65:
	addu	$2, $zero, $16
	lw	$16, 20($sp)
	nop
	lw	$17, 24($sp)
	nop
	lw	$18, 28($sp)
	nop
	lw	$19, 32($sp)
	nop
	lw	$20, 36($sp)
	nop
	lw	$21, 40($sp)
	nop
	lw	$22, 44($sp)
	nop
	lw	$23, 48($sp)
	nop
	lw	$ra, 52($sp)
	nop
	addiu	$sp, $sp, 56
	jr	$ra
	nop
	.set	macro
	.set	reorder
	.end	float64_mul
$tmp13:
	.size	float64_mul, ($tmp13)-float64_mul
$tmp14:
	.cfi_endproc
$eh_func_end0:

	.align	2
	.type	roundAndPackFloat64,@function
	.ent	roundAndPackFloat64     # @roundAndPackFloat64
roundAndPackFloat64:
$tmp17:
	.cfi_startproc
	.frame	$sp,40,$ra
	.mask 	0x801F0000,-4
	.fmask	0x00000000,0
# BB#0:
	.set	noreorder
	.set	nomacro
	addiu	$sp, $sp, -40
$tmp18:
	.cfi_def_cfa_offset 40
	sw	$ra, 36($sp)
	sw	$20, 32($sp)
	sw	$19, 28($sp)
	sw	$18, 24($sp)
	sw	$17, 20($sp)
	sw	$16, 16($sp)
$tmp19:
	.cfi_offset 31, -4
$tmp20:
	.cfi_offset 20, -8
$tmp21:
	.cfi_offset 19, -12
$tmp22:
	.cfi_offset 18, -16
$tmp23:
	.cfi_offset 17, -20
$tmp24:
	.cfi_offset 16, -24
	addu	$19, $zero, $7
	addu	$17, $zero, $6
	addu	$18, $zero, $5
	addu	$16, $zero, $4
	andi	$3, $18, 65535
	andi	$2, $17, 1023
	sltiu	$3, $3, 2045
	bne	$3, $zero, $BB1_13
	nop
# BB#1:
	addiu	$3, $zero, 2045
	slt	$3, $3, $18
	bne	$3, $zero, $BB1_5
	nop
# BB#2:
	addiu	$3, $zero, 2045
	bne	$18, $3, $BB1_6
	nop
# BB#3:
	addiu	$3, $17, 512
	addiu	$4, $zero, 512
	sltu	$3, $3, $4
	addiu	$4, $zero, 0
	addu	$4, $3, $4
	addiu	$3, $zero, -1
	addu	$4, $19, $4
	slt	$3, $3, $4
	beq	$3, $zero, $BB1_5
	nop
# BB#4:
	addiu	$18, $zero, 2045
	j	$BB1_13
	nop
$BB1_5:
	lui	$4, %hi(float_exception_flags)
	lui	$5, 32752
	lw	$2, %lo(float_exception_flags)($4)
	nop
	sll	$3, $16, 31
	ori	$6, $5, 0
	ori	$5, $2, 9
	addiu	$2, $zero, 0
	or	$3, $3, $6
	sw	$5, %lo(float_exception_flags)($4)
	j	$BB1_20
	nop
$BB1_6:
	addiu	$3, $zero, -1
	slt	$3, $3, $18
	bne	$3, $zero, $BB1_13
	nop
# BB#7:
	slti	$2, $18, -63
	bne	$2, $zero, $BB1_9
	nop
# BB#8:
	andi	$6, $18, 63
	addu	$4, $zero, $17
	addu	$5, $zero, $19
	jal	__ashldi3
	nop
	addiu	$4, $zero, 0
	or	$2, $2, $3
	xor	$20, $2, $4
	subu	$6, $4, $18
	addu	$4, $zero, $17
	addu	$5, $zero, $19
	jal	__lshrdi3
	nop
	sltu	$4, $zero, $20
	or	$17, $4, $2
	j	$BB1_10
	nop
$BB1_9:
	addiu	$3, $zero, 0
	or	$2, $17, $19
	xor	$2, $2, $3
	sltu	$17, $zero, $2
$BB1_10:                                # %shift64RightJamming.exit
	andi	$2, $17, 1023
	bne	$2, $zero, $BB1_12
	nop
# BB#11:
	addiu	$2, $zero, 0
	addu	$18, $zero, $2
	j	$BB1_17
	nop
$BB1_12:
	lui	$4, %hi(float_exception_flags)
	lw	$5, %lo(float_exception_flags)($4)
	nop
	addiu	$18, $zero, 0
	ori	$5, $5, 4
	sw	$5, %lo(float_exception_flags)($4)
	addu	$19, $zero, $3
$BB1_13:                                # %.thread
	bne	$2, $zero, $BB1_15
	nop
# BB#14:
	addiu	$2, $zero, 0
	j	$BB1_16
	nop
$BB1_15:
	lui	$3, %hi(float_exception_flags)
	lw	$4, %lo(float_exception_flags)($3)
	nop
	ori	$4, $4, 1
	sw	$4, %lo(float_exception_flags)($3)
$BB1_16:
	addu	$3, $zero, $19
$BB1_17:                                # %.thread6
	addiu	$5, $17, 512
	addiu	$7, $zero, 512
	sltu	$6, $5, $7
	addiu	$4, $zero, 0
	xor	$2, $2, $7
	addu	$6, $6, $4
	addu	$3, $3, $6
	sltu	$7, $2, 1
	srl	$2, $5, 10
	sll	$6, $3, 22
	nor	$5, $7, $zero
	addiu	$7, $zero, -2
	or	$2, $2, $6
	or	$5, $5, $7
	and	$2, $2, $5
	srl	$3, $3, 10
	sll	$5, $16, 31
	or	$6, $2, $3
	beq	$6, $zero, $BB1_19
	nop
# BB#18:
	sll	$4, $18, 20
$BB1_19:                                # %.thread6
	or	$3, $3, $5
	addu	$3, $4, $3
$BB1_20:
	lw	$16, 16($sp)
	nop
	lw	$17, 20($sp)
	nop
	lw	$18, 24($sp)
	nop
	lw	$19, 28($sp)
	nop
	lw	$20, 32($sp)
	nop
	lw	$ra, 36($sp)
	nop
	addiu	$sp, $sp, 40
	jr	$ra
	nop
	.set	macro
	.set	reorder
	.end	roundAndPackFloat64
$tmp25:
	.size	roundAndPackFloat64, ($tmp25)-roundAndPackFloat64
$tmp26:
	.cfi_endproc
$eh_func_end1:

	.section	_main_section,"ax",@progbits
	.globl	main
	.align	2
	.type	main,@function
	.ent	main                    # @main
main:
$tmp29:
	.cfi_startproc
	.frame	$sp,120,$ra
	.mask 	0x80FF0000,-4
	.fmask	0x00000000,0
# BB#0:
	.set	noreorder
	.set	nomacro
	addiu	$sp, $sp, -120
$tmp30:
	.cfi_def_cfa_offset 120
	sw	$ra, 116($sp)
	sw	$23, 112($sp)
	sw	$22, 108($sp)
	sw	$21, 104($sp)
	sw	$20, 100($sp)
	sw	$19, 96($sp)
	sw	$18, 92($sp)
	sw	$17, 88($sp)
	sw	$16, 84($sp)
$tmp31:
	.cfi_offset 31, -4
$tmp32:
	.cfi_offset 23, -8
$tmp33:
	.cfi_offset 22, -12
$tmp34:
	.cfi_offset 21, -16
$tmp35:
	.cfi_offset 20, -20
$tmp36:
	.cfi_offset 19, -24
$tmp37:
	.cfi_offset 18, -28
$tmp38:
	.cfi_offset 17, -32
$tmp39:
	.cfi_offset 16, -36
	addiu	$18, $zero, 0
	addu	$17, $zero, $18
$BB2_1:                                 # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_2 Depth 2
                                        #       Child Loop BB2_100 Depth 3
                                        #       Child Loop BB2_111 Depth 3
	sw	$18, 44($sp)
	sw	$17, 48($sp)
	lui	$2, %hi(test_in)
	addiu	$2, $2, %lo(test_in)
	sll	$3, $18, 3
	sw	$3, 32($sp)
	subu	$2, $2, $3
	lw	$19, 0($2)
	nop
	sw	$19, 36($sp)
	lw	$18, 4($2)
	nop
	sw	$18, 40($sp)
	lui	$16, 32768
	addu	$4, $zero, $19
	addu	$5, $zero, $18
	addu	$6, $zero, $19
	addu	$7, $zero, $18
	jal	float64_mul
	nop
	sw	$2, 56($sp)
	ori	$2, $16, 0
	addiu	$20, $zero, 2
	xor	$2, $3, $2
	sw	$2, 52($sp)
	sw	$19, 80($sp)
	addu	$21, $zero, $18
	sw	$19, 76($sp)
	sw	$18, 72($sp)
$BB2_2:                                 #   Parent Loop BB2_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_100 Depth 3
                                        #       Child Loop BB2_111 Depth 3
	sw	$20, 64($sp)
	addiu	$19, $20, 1
	lw	$4, 80($sp)
	nop
	addu	$5, $zero, $21
	lw	$6, 56($sp)
	nop
	lw	$7, 52($sp)
	nop
	jal	float64_mul
	nop
	sw	$2, 80($sp)
	addu	$16, $zero, $3
	addiu	$18, $zero, 0
	mult	$19, $20
	mflo	$3
	addu	$2, $zero, $18
	beq	$3, $zero, $BB2_8
	nop
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=2
	sra	$2, $3, 31
	addu	$4, $3, $2
	lui	$5, 1
	xor	$4, $4, $2
	ori	$2, $5, 0
	lui	$6, 255
	sltu	$5, $4, $2
	addu	$2, $zero, $4
	beq	$5, $zero, $BB2_5
	nop
# BB#4:                                 #   in Loop: Header=BB2_2 Depth=2
	sll	$2, $4, 16
$BB2_5:                                 #   in Loop: Header=BB2_2 Depth=2
	ori	$6, $6, 65535
	sll	$5, $5, 4
	sltu	$6, $6, $2
	bne	$6, $zero, $BB2_7
	nop
# BB#6:                                 #   in Loop: Header=BB2_2 Depth=2
	ori	$5, $5, 8
	sll	$2, $2, 8
$BB2_7:                                 # %countLeadingZeros32.exit.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	srl	$17, $3, 31
	lui	$6, %hi(countLeadingZeros32.countLeadingZerosHigh)
	srl	$3, $2, 22
	addiu	$2, $6, %lo(countLeadingZeros32.countLeadingZerosHigh)
	andi	$3, $3, 1020
	addu	$2, $2, $3
	lw	$2, 0($2)
	nop
	addu	$19, $2, $5
	addiu	$20, $zero, 1053
	addiu	$5, $zero, 0
	addiu	$6, $19, 21
	jal	__ashldi3
	nop
	addu	$18, $zero, $2
	sll	$2, $17, 31
	subu	$4, $20, $19
	addu	$2, $3, $2
	sll	$3, $4, 20
	addu	$2, $2, $3
$BB2_8:                                 # %int32_to_float64.exit.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lui	$3, 15
	ori	$3, $3, 65535
	srl	$5, $16, 20
	srl	$4, $2, 20
	xor	$6, $2, $16
	srl	$6, $6, 31
	sw	$6, 68($sp)
	andi	$23, $5, 2047
	andi	$17, $4, 2047
	and	$22, $16, $3
	and	$20, $2, $3
	addiu	$3, $zero, 2047
	lw	$9, 76($sp)
	nop
	lw	$10, 72($sp)
	nop
	bne	$23, $3, $BB2_42
	nop
# BB#9:                                 #   in Loop: Header=BB2_2 Depth=2
	lw	$3, 80($sp)
	nop
	or	$3, $3, $22
	beq	$3, $zero, $BB2_25
	nop
# BB#10:                                #   in Loop: Header=BB2_2 Depth=2
	lui	$4, 32760
	lui	$3, 32752
	ori	$4, $4, 0
	and	$4, $16, $4
	ori	$3, $3, 0
	xor	$3, $4, $3
	beq	$3, $zero, $BB2_12
	nop
# BB#11:                                #   in Loop: Header=BB2_2 Depth=2
	addiu	$3, $zero, 0
	j	$BB2_13
	nop
$BB2_12:                                #   in Loop: Header=BB2_2 Depth=2
	lui	$3, 7
	ori	$3, $3, 65535
	and	$3, $16, $3
	lw	$4, 80($sp)
	nop
	or	$3, $4, $3
	addiu	$4, $zero, 0
	xor	$3, $3, $4
	sltu	$3, $zero, $3
$BB2_13:                                # %float64_is_signaling_nan.exit1.i17.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lui	$4, 32767
	addiu	$5, $zero, 0
	lui	$6, 32752
	ori	$4, $4, 65535
	lui	$7, 32760
	ori	$6, $6, 0
	and	$4, $2, $4
	ori	$7, $7, 0
	xor	$8, $4, $6
	beq	$8, $zero, $BB2_15
	nop
# BB#14:                                #   in Loop: Header=BB2_2 Depth=2
	sltu	$4, $6, $4
	j	$BB2_16
	nop
$BB2_15:                                # %float64_is_signaling_nan.exit1.i17.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	xor	$4, $18, $5
	sltu	$4, $zero, $4
$BB2_16:                                # %float64_is_signaling_nan.exit1.i17.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	and	$7, $2, $7
	xor	$6, $7, $6
	bne	$6, $zero, $BB2_18
	nop
# BB#17:                                #   in Loop: Header=BB2_2 Depth=2
	lui	$5, 7
	ori	$5, $5, 65535
	and	$5, $2, $5
	or	$5, $18, $5
	addiu	$6, $zero, 0
	xor	$5, $5, $6
	sltu	$5, $zero, $5
$BB2_18:                                # %float64_is_signaling_nan.exit.i18.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	or	$3, $3, $5
	addiu	$6, $zero, 1
	bne	$3, $6, $BB2_20
	nop
# BB#19:                                # %.thread.i19.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lui	$3, %hi(float_exception_flags)
	lw	$4, %lo(float_exception_flags)($3)
	nop
	ori	$4, $4, 16
	sw	$4, %lo(float_exception_flags)($3)
	addu	$4, $zero, $5
$BB2_20:                                # %propagateFloat64NaN.exit23.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lui	$3, 8
	beq	$4, $zero, $BB2_22
	nop
# BB#21:                                # %propagateFloat64NaN.exit23.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	addu	$16, $zero, $2
$BB2_22:                                # %propagateFloat64NaN.exit23.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	beq	$4, $zero, $BB2_24
	nop
$BB2_23:                                # %propagateFloat64NaN.exit23.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	sw	$18, 80($sp)
$BB2_24:                                # %propagateFloat64NaN.exit23.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	ori	$2, $3, 0
	or	$21, $16, $2
	j	$BB2_114
	nop
$BB2_25:                                #   in Loop: Header=BB2_2 Depth=2
	addiu	$3, $zero, 2047
	bne	$17, $3, $BB2_41
	nop
# BB#26:                                #   in Loop: Header=BB2_2 Depth=2
	or	$3, $18, $20
	beq	$3, $zero, $BB2_40
	nop
# BB#27:                                #   in Loop: Header=BB2_2 Depth=2
	lui	$4, 32760
	lui	$3, 32752
	ori	$4, $4, 0
	and	$4, $16, $4
	ori	$3, $3, 0
	xor	$3, $4, $3
	beq	$3, $zero, $BB2_29
	nop
# BB#28:                                #   in Loop: Header=BB2_2 Depth=2
	addiu	$3, $zero, 0
	j	$BB2_30
	nop
$BB2_29:                                #   in Loop: Header=BB2_2 Depth=2
	lui	$3, 7
	ori	$3, $3, 65535
	and	$3, $16, $3
	lw	$4, 80($sp)
	nop
	or	$3, $4, $3
	addiu	$4, $zero, 0
	xor	$3, $3, $4
	sltu	$3, $zero, $3
$BB2_30:                                # %float64_is_signaling_nan.exit1.i10.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lui	$4, 32767
	addiu	$5, $zero, 0
	lui	$6, 32752
	ori	$4, $4, 65535
	lui	$7, 32760
	ori	$6, $6, 0
	and	$4, $2, $4
	ori	$7, $7, 0
	xor	$8, $4, $6
	beq	$8, $zero, $BB2_32
	nop
# BB#31:                                #   in Loop: Header=BB2_2 Depth=2
	sltu	$4, $6, $4
	j	$BB2_33
	nop
$BB2_32:                                # %float64_is_signaling_nan.exit1.i10.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	xor	$4, $18, $5
	sltu	$4, $zero, $4
$BB2_33:                                # %float64_is_signaling_nan.exit1.i10.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	and	$7, $2, $7
	xor	$6, $7, $6
	bne	$6, $zero, $BB2_35
	nop
# BB#34:                                #   in Loop: Header=BB2_2 Depth=2
	lui	$5, 7
	ori	$5, $5, 65535
	and	$5, $2, $5
	or	$5, $18, $5
	addiu	$6, $zero, 0
	xor	$5, $5, $6
	sltu	$5, $zero, $5
$BB2_35:                                # %float64_is_signaling_nan.exit.i11.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	or	$3, $3, $5
	addiu	$6, $zero, 1
	bne	$3, $6, $BB2_37
	nop
# BB#36:                                # %.thread.i12.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lui	$3, %hi(float_exception_flags)
	lw	$4, %lo(float_exception_flags)($3)
	nop
	ori	$4, $4, 16
	sw	$4, %lo(float_exception_flags)($3)
	addu	$4, $zero, $5
$BB2_37:                                # %propagateFloat64NaN.exit16.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lui	$3, 8
	beq	$4, $zero, $BB2_39
	nop
# BB#38:                                # %propagateFloat64NaN.exit16.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	addu	$16, $zero, $2
$BB2_39:                                # %propagateFloat64NaN.exit16.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	bne	$4, $zero, $BB2_23
	nop
	j	$BB2_24
	nop
$BB2_40:                                #   in Loop: Header=BB2_2 Depth=2
	lui	$2, %hi(float_exception_flags)
	lw	$3, %lo(float_exception_flags)($2)
	nop
	lui	$4, 32767
	ori	$3, $3, 16
	addiu	$5, $zero, -1
	sw	$5, 80($sp)
	ori	$21, $4, 65535
	sw	$3, %lo(float_exception_flags)($2)
	j	$BB2_114
	nop
$BB2_41:                                #   in Loop: Header=BB2_2 Depth=2
	lui	$3, 32752
	lw	$2, 68($sp)
	nop
	sll	$2, $2, 31
	ori	$3, $3, 0
	addiu	$4, $zero, 0
	sw	$4, 80($sp)
	or	$21, $2, $3
	j	$BB2_114
	nop
$BB2_42:                                #   in Loop: Header=BB2_2 Depth=2
	beq	$17, $zero, $BB2_58
	nop
$BB2_43:                                #   in Loop: Header=BB2_2 Depth=2
	addiu	$3, $zero, 2047
	bne	$17, $3, $BB2_70
	nop
$BB2_44:                                #   in Loop: Header=BB2_2 Depth=2
	or	$3, $18, $20
	beq	$3, $zero, $BB2_73
	nop
# BB#45:                                #   in Loop: Header=BB2_2 Depth=2
	lui	$4, 32760
	lui	$3, 32752
	ori	$4, $4, 0
	and	$4, $16, $4
	ori	$3, $3, 0
	xor	$3, $4, $3
	beq	$3, $zero, $BB2_47
	nop
# BB#46:                                #   in Loop: Header=BB2_2 Depth=2
	addiu	$3, $zero, 0
	j	$BB2_48
	nop
$BB2_47:                                #   in Loop: Header=BB2_2 Depth=2
	lui	$3, 7
	ori	$3, $3, 65535
	and	$3, $16, $3
	lw	$4, 80($sp)
	nop
	or	$3, $4, $3
	addiu	$4, $zero, 0
	xor	$3, $3, $4
	sltu	$3, $zero, $3
$BB2_48:                                # %float64_is_signaling_nan.exit1.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lui	$4, 32767
	addiu	$5, $zero, 0
	lui	$6, 32752
	ori	$4, $4, 65535
	lui	$7, 32760
	ori	$6, $6, 0
	and	$4, $2, $4
	ori	$7, $7, 0
	xor	$8, $4, $6
	beq	$8, $zero, $BB2_50
	nop
# BB#49:                                #   in Loop: Header=BB2_2 Depth=2
	sltu	$4, $6, $4
	j	$BB2_51
	nop
$BB2_50:                                # %float64_is_signaling_nan.exit1.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	xor	$4, $18, $5
	sltu	$4, $zero, $4
$BB2_51:                                # %float64_is_signaling_nan.exit1.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	and	$7, $2, $7
	xor	$6, $7, $6
	bne	$6, $zero, $BB2_53
	nop
# BB#52:                                #   in Loop: Header=BB2_2 Depth=2
	lui	$5, 7
	ori	$5, $5, 65535
	and	$5, $2, $5
	or	$5, $18, $5
	addiu	$6, $zero, 0
	xor	$5, $5, $6
	sltu	$5, $zero, $5
$BB2_53:                                # %float64_is_signaling_nan.exit.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	or	$3, $3, $5
	addiu	$6, $zero, 1
	bne	$3, $6, $BB2_55
	nop
# BB#54:                                # %.thread.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lui	$3, %hi(float_exception_flags)
	lw	$4, %lo(float_exception_flags)($3)
	nop
	ori	$4, $4, 16
	sw	$4, %lo(float_exception_flags)($3)
	addu	$4, $zero, $5
$BB2_55:                                # %propagateFloat64NaN.exit.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lui	$3, 8
	beq	$4, $zero, $BB2_57
	nop
# BB#56:                                # %propagateFloat64NaN.exit.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	addu	$16, $zero, $2
$BB2_57:                                # %propagateFloat64NaN.exit.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	bne	$4, $zero, $BB2_23
	nop
	j	$BB2_24
	nop
$BB2_58:                                #   in Loop: Header=BB2_2 Depth=2
	or	$2, $18, $20
	bne	$2, $zero, $BB2_63
	nop
# BB#59:                                #   in Loop: Header=BB2_2 Depth=2
	lui	$2, %hi(float_exception_flags)
	lw	$2, %lo(float_exception_flags)($2)
	nop
	lw	$3, 80($sp)
	nop
	or	$3, $23, $3
	or	$3, $22, $3
	bne	$3, $zero, $BB2_62
	nop
# BB#60:                                #   in Loop: Header=BB2_2 Depth=2
	lui	$3, 32767
	addiu	$4, $zero, -1
	sw	$4, 80($sp)
	ori	$21, $3, 65535
	ori	$2, $2, 16
$BB2_61:                                #   in Loop: Header=BB2_2 Depth=2
	lui	$3, %hi(float_exception_flags)
	sw	$2, %lo(float_exception_flags)($3)
	j	$BB2_114
	nop
$BB2_62:                                #   in Loop: Header=BB2_2 Depth=2
	lui	$4, 32752
	lw	$3, 68($sp)
	nop
	sll	$3, $3, 31
	ori	$4, $4, 0
	addiu	$5, $zero, 0
	sw	$5, 80($sp)
	or	$21, $3, $4
	ori	$2, $2, 2
	j	$BB2_61
	nop
$BB2_63:                                #   in Loop: Header=BB2_2 Depth=2
	lui	$3, 1
	addu	$2, $zero, $20
	bne	$20, $zero, $BB2_65
	nop
# BB#64:                                #   in Loop: Header=BB2_2 Depth=2
	addu	$2, $zero, $18
$BB2_65:                                #   in Loop: Header=BB2_2 Depth=2
	ori	$3, $3, 0
	lui	$4, 255
	sltu	$3, $2, $3
	beq	$3, $zero, $BB2_67
	nop
# BB#66:                                #   in Loop: Header=BB2_2 Depth=2
	sll	$2, $2, 16
$BB2_67:                                #   in Loop: Header=BB2_2 Depth=2
	ori	$4, $4, 65535
	sll	$3, $3, 4
	addu	$21, $zero, $10
	addu	$19, $zero, $9
	sltu	$4, $4, $2
	bne	$4, $zero, $BB2_69
	nop
# BB#68:                                #   in Loop: Header=BB2_2 Depth=2
	ori	$3, $3, 8
	sll	$2, $2, 8
$BB2_69:                                # %normalizeFloat64Subnormal.exit9.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	addiu	$4, $zero, 0
	xor	$4, $20, $4
	sltu	$4, $4, 1
	sll	$4, $4, 5
	lui	$6, %hi(countLeadingZeros32.countLeadingZerosHigh)
	srl	$5, $2, 22
	addiu	$2, $6, %lo(countLeadingZeros32.countLeadingZerosHigh)
	andi	$5, $5, 1020
	addu	$5, $2, $5
	addu	$2, $3, $4
	lw	$3, 0($5)
	nop
	addu	$16, $2, $3
	addiu	$17, $zero, 12
	addiu	$6, $16, -11
	addu	$4, $zero, $18
	addu	$5, $zero, $20
	jal	__ashldi3
	nop
	addu	$18, $zero, $2
	addu	$20, $zero, $3
	subu	$17, $17, $16
	addu	$9, $zero, $19
	addu	$10, $zero, $21
$BB2_70:                                #   in Loop: Header=BB2_2 Depth=2
	beq	$23, $zero, $BB2_72
	nop
# BB#71:                                #   in Loop: Header=BB2_2 Depth=2
	sw	$10, 72($sp)
	sw	$9, 76($sp)
	j	$BB2_82
	nop
$BB2_72:                                #   in Loop: Header=BB2_2 Depth=2
	lw	$2, 80($sp)
	nop
	or	$2, $2, $22
	bne	$2, $zero, $BB2_74
	nop
$BB2_73:                                #   in Loop: Header=BB2_2 Depth=2
	addiu	$2, $zero, 0
	sw	$2, 80($sp)
	lw	$2, 68($sp)
	nop
	sll	$21, $2, 31
	j	$BB2_114
	nop
$BB2_74:                                #   in Loop: Header=BB2_2 Depth=2
	lui	$3, 1
	addu	$2, $zero, $22
	bne	$22, $zero, $BB2_76
	nop
# BB#75:                                #   in Loop: Header=BB2_2 Depth=2
	lw	$2, 80($sp)
	nop
$BB2_76:                                #   in Loop: Header=BB2_2 Depth=2
	ori	$3, $3, 0
	lui	$4, 255
	sltu	$3, $2, $3
	beq	$3, $zero, $BB2_78
	nop
# BB#77:                                #   in Loop: Header=BB2_2 Depth=2
	sll	$2, $2, 16
$BB2_78:                                #   in Loop: Header=BB2_2 Depth=2
	ori	$4, $4, 65535
	sll	$3, $3, 4
	sltu	$4, $4, $2
	beq	$4, $zero, $BB2_80
	nop
# BB#79:                                #   in Loop: Header=BB2_2 Depth=2
	sw	$10, 72($sp)
	sw	$9, 76($sp)
	j	$BB2_81
	nop
$BB2_80:                                #   in Loop: Header=BB2_2 Depth=2
	sw	$10, 72($sp)
	sw	$9, 76($sp)
	ori	$3, $3, 8
	sll	$2, $2, 8
$BB2_81:                                # %normalizeFloat64Subnormal.exit.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	addiu	$4, $zero, 0
	xor	$4, $22, $4
	sltu	$4, $4, 1
	sll	$4, $4, 5
	lui	$6, %hi(countLeadingZeros32.countLeadingZerosHigh)
	srl	$5, $2, 22
	addiu	$2, $6, %lo(countLeadingZeros32.countLeadingZerosHigh)
	andi	$5, $5, 1020
	addu	$5, $2, $5
	addu	$2, $3, $4
	lw	$3, 0($5)
	nop
	addu	$16, $2, $3
	addiu	$19, $zero, 12
	addiu	$6, $16, -11
	lw	$4, 80($sp)
	nop
	addu	$5, $zero, $22
	jal	__ashldi3
	nop
	sw	$2, 80($sp)
	addu	$22, $zero, $3
	subu	$23, $19, $16
$BB2_82:                                #   in Loop: Header=BB2_2 Depth=2
	lw	$2, 80($sp)
	nop
	sll	$19, $2, 10
	lui	$3, 16384
	sll	$4, $22, 10
	srl	$5, $2, 22
	addu	$2, $19, $19
	or	$4, $4, $5
	ori	$3, $3, 0
	or	$21, $4, $3
	sltu	$4, $2, $19
	lui	$3, 32768
	sll	$5, $20, 11
	srl	$6, $18, 21
	addu	$4, $4, $21
	or	$5, $5, $6
	ori	$6, $3, 0
	sll	$18, $18, 11
	subu	$3, $23, $17
	or	$16, $5, $6
	addu	$4, $21, $4
	addiu	$7, $3, 1021
	xor	$5, $16, $4
	beq	$5, $zero, $BB2_84
	nop
# BB#83:                                #   in Loop: Header=BB2_2 Depth=2
	sltu	$2, $4, $16
	j	$BB2_85
	nop
$BB2_84:                                #   in Loop: Header=BB2_2 Depth=2
	sltu	$2, $2, $18
$BB2_85:                                #   in Loop: Header=BB2_2 Depth=2
	bne	$2, $zero, $BB2_87
	nop
# BB#86:                                #   in Loop: Header=BB2_2 Depth=2
	srl	$2, $19, 1
	sll	$4, $21, 31
	addiu	$7, $3, 1022
	or	$19, $2, $4
	srl	$21, $21, 1
$BB2_87:                                #   in Loop: Header=BB2_2 Depth=2
	sltu	$2, $21, $16
	addiu	$6, $zero, -1
	xor	$3, $16, $21
	beq	$3, $zero, $BB2_89
	nop
# BB#88:                                #   in Loop: Header=BB2_2 Depth=2
	xori	$4, $2, 1
	j	$BB2_90
	nop
$BB2_89:                                #   in Loop: Header=BB2_2 Depth=2
	sltu	$4, $19, $18
	xori	$4, $4, 1
$BB2_90:                                #   in Loop: Header=BB2_2 Depth=2
	sw	$7, 80($sp)
	addu	$7, $zero, $6
	bne	$4, $zero, $BB2_113
	nop
# BB#91:                                #   in Loop: Header=BB2_2 Depth=2
	addiu	$20, $zero, 0
	addiu	$22, $zero, -1
	beq	$3, $zero, $BB2_93
	nop
# BB#92:                                #   in Loop: Header=BB2_2 Depth=2
	xori	$2, $2, 1
	j	$BB2_94
	nop
$BB2_93:                                #   in Loop: Header=BB2_2 Depth=2
	addiu	$2, $zero, 1
$BB2_94:                                #   in Loop: Header=BB2_2 Depth=2
	addu	$17, $zero, $20
	bne	$2, $zero, $BB2_96
	nop
# BB#95:                                #   in Loop: Header=BB2_2 Depth=2
	addiu	$17, $zero, 0
	addu	$4, $zero, $19
	addu	$5, $zero, $21
	addu	$6, $zero, $16
	addu	$7, $zero, $20
	jal	__udivdi3
	nop
	addu	$22, $zero, $2
$BB2_96:                                #   in Loop: Header=BB2_2 Depth=2
	sw	$17, 60($sp)
	addiu	$2, $zero, -2048
	and	$17, $18, $2
	multu	$22, $16
	mflo	$6
	mfhi	$5
	addiu	$23, $zero, 0
	mult	$22, $20
	mflo	$8
	multu	$22, $17
	mflo	$2
	mfhi	$7
	addiu	$3, $zero, -1
	addu	$4, $zero, $23
	beq	$2, $zero, $BB2_98
	nop
# BB#97:                                #   in Loop: Header=BB2_2 Depth=2
	addu	$4, $zero, $3
$BB2_98:                                #   in Loop: Header=BB2_2 Depth=2
	subu	$9, $19, $6
	sltu	$6, $19, $6
	addu	$8, $5, $8
	subu	$5, $9, $7
	addu	$5, $5, $4
	sltu	$7, $9, $7
	addu	$9, $6, $8
	sltu	$8, $23, $23
	sltu	$6, $5, $4
	subu	$9, $21, $9
	addu	$10, $7, $23
	addu	$7, $8, $2
	subu	$2, $9, $10
	addu	$4, $6, $4
	addu	$2, $2, $4
	subu	$4, $23, $7
	slt	$3, $3, $2
	bne	$3, $zero, $BB2_101
	nop
# BB#99:                                # %.lr.ph.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	subu	$3, $23, $23
	addiu	$6, $zero, 0
$BB2_100:                               #   Parent Loop BB2_1 Depth=1
                                        #     Parent Loop BB2_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	addu	$3, $3, $6
	sltu	$7, $3, $6
	addu	$7, $7, $18
	addu	$4, $4, $7
	sltu	$8, $4, $18
	addiu	$7, $zero, 0
	addu	$8, $8, $7
	addu	$8, $6, $8
	addu	$9, $5, $16
	andi	$8, $8, 1
	addu	$5, $9, $8
	sltu	$9, $9, $16
	sltu	$8, $5, $8
	addu	$9, $9, $20
	addu	$2, $2, $9
	addu	$7, $8, $7
	addu	$2, $2, $7
	addiu	$22, $22, -1
	bltz	$2, $BB2_100
	nop
$BB2_101:                               # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	addiu	$2, $zero, -1
	addiu	$3, $zero, 0
	xor	$6, $16, $5
	beq	$6, $zero, $BB2_103
	nop
# BB#102:                               #   in Loop: Header=BB2_2 Depth=2
	sltu	$6, $5, $16
	j	$BB2_104
	nop
$BB2_103:                               # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	sltu	$6, $4, $20
$BB2_104:                               # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	xori	$6, $6, 1
	bne	$6, $zero, $BB2_106
	nop
# BB#105:                               #   in Loop: Header=BB2_2 Depth=2
	addu	$6, $zero, $16
	addu	$7, $zero, $20
	jal	__udivdi3
	nop
$BB2_106:                               # %estimateDiv128To64.exit.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lw	$4, 60($sp)
	nop
	or	$6, $2, $4
	or	$7, $3, $22
	addiu	$2, $zero, 2
	andi	$3, $6, 511
	sltu	$2, $2, $3
	bne	$2, $zero, $BB2_113
	nop
# BB#107:                               #   in Loop: Header=BB2_2 Depth=2
	multu	$7, $17
	mflo	$3
	mfhi	$4
	multu	$6, $16
	mflo	$11
	mfhi	$10
	multu	$6, $17
	mflo	$2
	mfhi	$5
	mult	$6, $23
	mflo	$8
	addu	$14, $3, $11
	addu	$17, $5, $8
	addu	$5, $14, $17
	addiu	$3, $zero, 0
	mult	$6, $20
	mflo	$25
	mult	$7, $23
	mflo	$24
	multu	$7, $16
	mflo	$13
	mfhi	$12
	mult	$7, $20
	mflo	$15
	addiu	$8, $zero, -1
	or	$20, $2, $5
	addu	$9, $zero, $3
	beq	$20, $zero, $BB2_109
	nop
# BB#108:                               #   in Loop: Header=BB2_2 Depth=2
	addu	$9, $zero, $8
$BB2_109:                               #   in Loop: Header=BB2_2 Depth=2
	sltu	$17, $5, $17
	addu	$17, $17, $3
	addu	$17, $3, $17
	sltu	$14, $14, $11
	addu	$25, $10, $25
	sll	$11, $17, 31
	addu	$4, $4, $24
	addu	$10, $14, $25
	addu	$10, $4, $10
	subu	$4, $19, $13
	sra	$14, $11, 31
	addu	$11, $4, $14
	sltu	$24, $10, $25
	sltu	$4, $19, $13
	addu	$13, $12, $15
	sltu	$15, $11, $14
	addu	$12, $24, $3
	addu	$13, $4, $13
	subu	$4, $11, $10
	addu	$4, $4, $9
	subu	$13, $21, $13
	addu	$14, $15, $14
	sltu	$10, $11, $10
	addu	$15, $3, $12
	sltu	$12, $3, $2
	sltu	$11, $4, $9
	addu	$13, $13, $14
	addu	$14, $10, $15
	addu	$10, $12, $5
	subu	$5, $13, $14
	addu	$9, $11, $9
	addu	$5, $5, $9
	subu	$2, $3, $2
	subu	$3, $3, $10
	slt	$8, $8, $5
	bne	$8, $zero, $BB2_112
	nop
# BB#110:                               # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB2_2 Depth=2
	addiu	$8, $zero, 0
$BB2_111:                               # %.lr.ph.i.i
                                        #   Parent Loop BB2_1 Depth=1
                                        #     Parent Loop BB2_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	addu	$2, $2, $18
	sltu	$9, $2, $18
	addu	$9, $9, $16
	addu	$3, $3, $9
	sltu	$9, $3, $16
	addiu	$10, $zero, 0
	addu	$9, $9, $10
	addu	$9, $8, $9
	andi	$9, $9, 1
	addu	$9, $9, $4
	addiu	$6, $6, -1
	addiu	$11, $zero, -1
	sltu	$12, $9, $4
	sltu	$4, $6, $11
	addu	$10, $12, $10
	addu	$4, $4, $11
	addu	$5, $5, $10
	addu	$7, $7, $4
	addu	$4, $zero, $9
	bltz	$5, $BB2_111
	nop
$BB2_112:                               # %._crit_edge.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	or	$2, $2, $3
	addiu	$3, $zero, 0
	xor	$2, $2, $3
	sltu	$2, $zero, $2
	or	$6, $2, $6
$BB2_113:                               # %estimateDiv128To64.exit.thread.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lw	$4, 68($sp)
	nop
	lw	$5, 80($sp)
	nop
	jal	roundAndPackFloat64
	nop
	sw	$2, 80($sp)
	addu	$21, $zero, $3
	lw	$9, 76($sp)
	nop
	lw	$10, 72($sp)
	nop
$BB2_114:                               # %float64_div.exit.i
                                        #   in Loop: Header=BB2_2 Depth=2
	srl	$3, $10, 20
	srl	$2, $21, 20
	andi	$16, $3, 2047
	andi	$23, $2, 2047
	srl	$4, $10, 31
	sw	$4, 76($sp)
	subu	$2, $16, $23
	srl	$3, $21, 31
	bne	$4, $3, $BB2_190
	nop
# BB#115:                               #   in Loop: Header=BB2_2 Depth=2
	lui	$3, 8191
	sll	$5, $10, 9
	srl	$7, $9, 23
	sll	$4, $21, 9
	lw	$8, 80($sp)
	nop
	srl	$6, $8, 23
	or	$5, $5, $7
	ori	$3, $3, 65535
	or	$4, $4, $6
	sll	$20, $9, 9
	and	$18, $5, $3
	sll	$19, $8, 9
	and	$22, $4, $3
	slti	$3, $2, 1
	bne	$3, $zero, $BB2_139
	nop
# BB#116:                               #   in Loop: Header=BB2_2 Depth=2
	addiu	$3, $zero, 2047
	bne	$16, $3, $BB2_133
	nop
# BB#117:                               #   in Loop: Header=BB2_2 Depth=2
	or	$2, $20, $18
	beq	$2, $zero, $BB2_278
	nop
# BB#118:                               #   in Loop: Header=BB2_2 Depth=2
	lui	$3, 32760
	lui	$2, 32752
	ori	$3, $3, 0
	and	$3, $10, $3
	ori	$2, $2, 0
	xor	$2, $3, $2
	beq	$2, $zero, $BB2_120
	nop
# BB#119:                               #   in Loop: Header=BB2_2 Depth=2
	addiu	$2, $zero, 0
	j	$BB2_121
	nop
$BB2_120:                               #   in Loop: Header=BB2_2 Depth=2
	lui	$2, 7
	ori	$2, $2, 65535
	and	$2, $10, $2
	or	$2, $9, $2
	addiu	$3, $zero, 0
	xor	$2, $2, $3
	sltu	$2, $zero, $2
$BB2_121:                               # %float64_is_signaling_nan.exit1.i10.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lui	$3, 32767
	addiu	$4, $zero, 0
	lui	$5, 32752
	ori	$3, $3, 65535
	lui	$6, 32760
	ori	$5, $5, 0
	and	$3, $21, $3
	ori	$6, $6, 0
	xor	$7, $3, $5
	beq	$7, $zero, $BB2_123
	nop
# BB#122:                               #   in Loop: Header=BB2_2 Depth=2
	sltu	$3, $5, $3
	j	$BB2_124
	nop
$BB2_123:                               # %float64_is_signaling_nan.exit1.i10.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lw	$3, 80($sp)
	nop
	xor	$3, $3, $4
	sltu	$3, $zero, $3
$BB2_124:                               # %float64_is_signaling_nan.exit1.i10.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	and	$6, $21, $6
	xor	$5, $6, $5
	bne	$5, $zero, $BB2_126
	nop
# BB#125:                               #   in Loop: Header=BB2_2 Depth=2
	lui	$4, 7
	ori	$4, $4, 65535
	and	$4, $21, $4
	lw	$5, 80($sp)
	nop
	or	$4, $5, $4
	addiu	$5, $zero, 0
	xor	$4, $4, $5
	sltu	$4, $zero, $4
$BB2_126:                               # %float64_is_signaling_nan.exit.i11.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	or	$2, $2, $4
	addiu	$5, $zero, 1
	bne	$2, $5, $BB2_128
	nop
# BB#127:                               # %.thread.i12.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lui	$2, %hi(float_exception_flags)
	lw	$3, %lo(float_exception_flags)($2)
	nop
	ori	$3, $3, 16
	sw	$3, %lo(float_exception_flags)($2)
	addu	$3, $zero, $4
$BB2_128:                               # %propagateFloat64NaN.exit16.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lui	$2, 8
	beq	$3, $zero, $BB2_130
	nop
# BB#129:                               # %propagateFloat64NaN.exit16.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	addu	$10, $zero, $21
$BB2_130:                               # %propagateFloat64NaN.exit16.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	beq	$3, $zero, $BB2_132
	nop
$BB2_131:                               # %propagateFloat64NaN.exit16.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lw	$9, 80($sp)
	nop
$BB2_132:                               # %propagateFloat64NaN.exit16.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	ori	$2, $2, 0
	or	$10, $10, $2
	j	$BB2_278
	nop
$BB2_133:                               #   in Loop: Header=BB2_2 Depth=2
	lui	$4, 8192
	addiu	$3, $zero, 0
	xor	$3, $23, $3
	beq	$23, $zero, $BB2_135
	nop
# BB#134:                               #   in Loop: Header=BB2_2 Depth=2
	ori	$4, $4, 0
	or	$22, $22, $4
$BB2_135:                               #   in Loop: Header=BB2_2 Depth=2
	addu	$17, $zero, $21
	sltu	$3, $3, 1
	subu	$23, $2, $3
	bne	$23, $zero, $BB2_137
	nop
# BB#136:                               #   in Loop: Header=BB2_2 Depth=2
	addu	$3, $zero, $22
	j	$BB2_183
	nop
$BB2_137:                               #   in Loop: Header=BB2_2 Depth=2
	lw	$2, 80($sp)
	nop
	addiu	$2, $zero, 63
	slt	$2, $2, $23
	bne	$2, $zero, $BB2_182
	nop
# BB#138:                               #   in Loop: Header=BB2_2 Depth=2
	addiu	$21, $zero, 0
	subu	$2, $21, $23
	andi	$6, $2, 63
	addu	$4, $zero, $19
	addu	$5, $zero, $22
	jal	__ashldi3
	nop
	or	$2, $2, $3
	xor	$21, $2, $21
	addu	$4, $zero, $19
	addu	$5, $zero, $22
	addu	$6, $zero, $23
	jal	__lshrdi3
	nop
	sltu	$4, $zero, $21
	or	$19, $4, $2
	j	$BB2_183
	nop
$BB2_139:                               #   in Loop: Header=BB2_2 Depth=2
	addiu	$3, $zero, -1
	slt	$3, $3, $2
	bne	$3, $zero, $BB2_164
	nop
# BB#140:                               #   in Loop: Header=BB2_2 Depth=2
	addiu	$3, $zero, 2047
	bne	$23, $3, $BB2_157
	nop
# BB#141:                               #   in Loop: Header=BB2_2 Depth=2
	or	$2, $19, $22
	beq	$2, $zero, $BB2_155
	nop
# BB#142:                               #   in Loop: Header=BB2_2 Depth=2
	lui	$3, 32760
	lui	$2, 32752
	ori	$3, $3, 0
	and	$3, $10, $3
	ori	$2, $2, 0
	xor	$2, $3, $2
	beq	$2, $zero, $BB2_144
	nop
# BB#143:                               #   in Loop: Header=BB2_2 Depth=2
	addiu	$2, $zero, 0
	j	$BB2_145
	nop
$BB2_144:                               #   in Loop: Header=BB2_2 Depth=2
	lui	$2, 7
	ori	$2, $2, 65535
	and	$2, $10, $2
	or	$2, $9, $2
	addiu	$3, $zero, 0
	xor	$2, $2, $3
	sltu	$2, $zero, $2
$BB2_145:                               # %float64_is_signaling_nan.exit1.i1.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lui	$3, 32767
	addiu	$4, $zero, 0
	lui	$5, 32752
	ori	$3, $3, 65535
	lui	$6, 32760
	ori	$5, $5, 0
	and	$3, $21, $3
	ori	$6, $6, 0
	xor	$7, $3, $5
	beq	$7, $zero, $BB2_147
	nop
# BB#146:                               #   in Loop: Header=BB2_2 Depth=2
	sltu	$3, $5, $3
	j	$BB2_148
	nop
$BB2_147:                               # %float64_is_signaling_nan.exit1.i1.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lw	$3, 80($sp)
	nop
	xor	$3, $3, $4
	sltu	$3, $zero, $3
$BB2_148:                               # %float64_is_signaling_nan.exit1.i1.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	and	$6, $21, $6
	xor	$5, $6, $5
	bne	$5, $zero, $BB2_150
	nop
# BB#149:                               #   in Loop: Header=BB2_2 Depth=2
	lui	$4, 7
	ori	$4, $4, 65535
	and	$4, $21, $4
	lw	$5, 80($sp)
	nop
	or	$4, $5, $4
	addiu	$5, $zero, 0
	xor	$4, $4, $5
	sltu	$4, $zero, $4
$BB2_150:                               # %float64_is_signaling_nan.exit.i2.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	or	$2, $2, $4
	addiu	$5, $zero, 1
	bne	$2, $5, $BB2_152
	nop
# BB#151:                               # %.thread.i3.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lui	$2, %hi(float_exception_flags)
	lw	$3, %lo(float_exception_flags)($2)
	nop
	ori	$3, $3, 16
	sw	$3, %lo(float_exception_flags)($2)
	addu	$3, $zero, $4
$BB2_152:                               # %propagateFloat64NaN.exit7.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lui	$2, 8
	beq	$3, $zero, $BB2_154
	nop
# BB#153:                               # %propagateFloat64NaN.exit7.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	addu	$10, $zero, $21
$BB2_154:                               # %propagateFloat64NaN.exit7.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	bne	$3, $zero, $BB2_131
	nop
	j	$BB2_132
	nop
$BB2_155:                               #   in Loop: Header=BB2_2 Depth=2
	lui	$3, 32752
	lw	$2, 76($sp)
	nop
$BB2_156:                               #   in Loop: Header=BB2_2 Depth=2
	sll	$2, $2, 31
	ori	$3, $3, 0
	addiu	$9, $zero, 0
	or	$10, $2, $3
	j	$BB2_278
	nop
$BB2_157:                               #   in Loop: Header=BB2_2 Depth=2
	addiu	$3, $zero, 0
	xor	$5, $16, $3
	lui	$4, 8192
	sltu	$5, $5, 1
	addu	$2, $2, $5
	beq	$16, $zero, $BB2_159
	nop
# BB#158:                               #   in Loop: Header=BB2_2 Depth=2
	ori	$4, $4, 0
	or	$18, $18, $4
$BB2_159:                               #   in Loop: Header=BB2_2 Depth=2
	bne	$2, $zero, $BB2_161
	nop
$BB2_160:                               #   in Loop: Header=BB2_2 Depth=2
	addu	$3, $zero, $22
	addu	$16, $zero, $23
	j	$BB2_184
	nop
$BB2_161:                               #   in Loop: Header=BB2_2 Depth=2
	slti	$4, $2, -63
	bne	$4, $zero, $BB2_163
	nop
# BB#162:                               #   in Loop: Header=BB2_2 Depth=2
	subu	$16, $3, $2
	andi	$6, $2, 63
	addu	$4, $zero, $20
	addu	$5, $zero, $18
	jal	__ashldi3
	nop
	or	$2, $2, $3
	addiu	$3, $zero, 0
	addu	$17, $zero, $21
	xor	$21, $2, $3
	addu	$4, $zero, $20
	addu	$5, $zero, $18
	addu	$6, $zero, $16
	jal	__lshrdi3
	nop
	addu	$18, $zero, $3
	sltu	$3, $zero, $21
	addu	$21, $zero, $17
	or	$20, $3, $2
	j	$BB2_160
	nop
$BB2_163:                               #   in Loop: Header=BB2_2 Depth=2
	addiu	$2, $zero, 0
	or	$3, $20, $18
	xor	$3, $3, $2
	sltu	$20, $zero, $3
	addu	$18, $zero, $2
	j	$BB2_160
	nop
$BB2_164:                               #   in Loop: Header=BB2_2 Depth=2
	beq	$16, $zero, $BB2_180
	nop
$BB2_165:                               #   in Loop: Header=BB2_2 Depth=2
	addiu	$2, $zero, 2047
	bne	$16, $2, $BB2_181
	nop
$BB2_166:                               #   in Loop: Header=BB2_2 Depth=2
	or	$2, $19, $20
	or	$3, $22, $18
	or	$2, $2, $3
	beq	$2, $zero, $BB2_278
	nop
# BB#167:                               #   in Loop: Header=BB2_2 Depth=2
	lui	$3, 32760
	lui	$2, 32752
	ori	$3, $3, 0
	and	$3, $10, $3
	ori	$2, $2, 0
	xor	$2, $3, $2
	beq	$2, $zero, $BB2_169
	nop
# BB#168:                               #   in Loop: Header=BB2_2 Depth=2
	addiu	$2, $zero, 0
	j	$BB2_170
	nop
$BB2_169:                               #   in Loop: Header=BB2_2 Depth=2
	lui	$2, 7
	ori	$2, $2, 65535
	and	$2, $10, $2
	or	$2, $9, $2
	addiu	$3, $zero, 0
	xor	$2, $2, $3
	sltu	$2, $zero, $2
$BB2_170:                               # %float64_is_signaling_nan.exit1.i.i4.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lui	$3, 32767
	addiu	$4, $zero, 0
	lui	$5, 32752
	ori	$3, $3, 65535
	lui	$6, 32760
	ori	$5, $5, 0
	and	$3, $21, $3
	ori	$6, $6, 0
	xor	$7, $3, $5
	beq	$7, $zero, $BB2_172
	nop
# BB#171:                               #   in Loop: Header=BB2_2 Depth=2
	sltu	$3, $5, $3
	j	$BB2_173
	nop
$BB2_172:                               # %float64_is_signaling_nan.exit1.i.i4.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lw	$3, 80($sp)
	nop
	xor	$3, $3, $4
	sltu	$3, $zero, $3
$BB2_173:                               # %float64_is_signaling_nan.exit1.i.i4.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	and	$6, $21, $6
	xor	$5, $6, $5
	bne	$5, $zero, $BB2_175
	nop
# BB#174:                               #   in Loop: Header=BB2_2 Depth=2
	lui	$4, 7
	ori	$4, $4, 65535
	and	$4, $21, $4
	lw	$5, 80($sp)
	nop
	or	$4, $5, $4
	addiu	$5, $zero, 0
	xor	$4, $4, $5
	sltu	$4, $zero, $4
$BB2_175:                               # %float64_is_signaling_nan.exit.i.i5.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	or	$2, $2, $4
	addiu	$5, $zero, 1
	bne	$2, $5, $BB2_177
	nop
# BB#176:                               # %.thread.i.i6.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lui	$2, %hi(float_exception_flags)
	lw	$3, %lo(float_exception_flags)($2)
	nop
	ori	$3, $3, 16
	sw	$3, %lo(float_exception_flags)($2)
	addu	$3, $zero, $4
$BB2_177:                               # %propagateFloat64NaN.exit.i10.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lui	$2, 8
	beq	$3, $zero, $BB2_179
	nop
# BB#178:                               # %propagateFloat64NaN.exit.i10.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	addu	$10, $zero, $21
$BB2_179:                               # %propagateFloat64NaN.exit.i10.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	bne	$3, $zero, $BB2_131
	nop
	j	$BB2_132
	nop
$BB2_180:                               #   in Loop: Header=BB2_2 Depth=2
	addu	$2, $19, $20
	sltu	$3, $2, $20
	addu	$3, $3, $18
	addu	$3, $22, $3
	srl	$2, $2, 9
	sll	$4, $3, 23
	srl	$3, $3, 9
	lw	$5, 76($sp)
	nop
	sll	$5, $5, 31
	or	$9, $2, $4
	or	$10, $3, $5
	j	$BB2_278
	nop
$BB2_181:                               #   in Loop: Header=BB2_2 Depth=2
	addu	$6, $20, $19
	lui	$2, 16384
	ori	$2, $2, 0
	sltu	$3, $6, $19
	or	$2, $18, $2
	addu	$3, $3, $22
	addu	$7, $2, $3
	j	$BB2_189
	nop
$BB2_182:                               #   in Loop: Header=BB2_2 Depth=2
	addiu	$3, $zero, 0
	or	$2, $19, $22
	xor	$2, $2, $3
	sltu	$19, $zero, $2
$BB2_183:                               #   in Loop: Header=BB2_2 Depth=2
	addu	$21, $zero, $17
$BB2_184:                               # %shift64RightJamming.exit9.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	addu	$4, $20, $19
	lui	$2, 8192
	ori	$5, $2, 0
	sltu	$2, $4, $19
	addu	$6, $4, $4
	or	$5, $18, $5
	addu	$2, $2, $3
	addu	$3, $5, $2
	sltu	$2, $6, $4
	addu	$2, $2, $3
	addu	$2, $3, $2
	slti	$5, $2, 0
	beq	$5, $zero, $BB2_186
	nop
# BB#185:                               # %shift64RightJamming.exit9.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	addu	$6, $zero, $4
$BB2_186:                               # %shift64RightJamming.exit9.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	addu	$7, $zero, $2
	beq	$5, $zero, $BB2_188
	nop
# BB#187:                               # %shift64RightJamming.exit9.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	addu	$7, $zero, $3
$BB2_188:                               # %shift64RightJamming.exit9.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	addiu	$3, $zero, -1
	slt	$2, $3, $2
	subu	$16, $16, $2
$BB2_189:                               #   in Loop: Header=BB2_2 Depth=2
	lw	$4, 76($sp)
	nop
	addu	$5, $zero, $16
	j	$BB2_277
	nop
$BB2_190:                               #   in Loop: Header=BB2_2 Depth=2
	lui	$3, 16383
	sll	$5, $10, 10
	srl	$7, $9, 22
	sll	$4, $21, 10
	lw	$8, 80($sp)
	nop
	srl	$6, $8, 22
	or	$5, $5, $7
	ori	$3, $3, 65535
	or	$4, $4, $6
	sll	$20, $9, 10
	and	$19, $5, $3
	sll	$22, $8, 10
	and	$18, $4, $3
	bgtz	$2, $BB2_243
	nop
# BB#191:                               #   in Loop: Header=BB2_2 Depth=2
	bltz	$2, $BB2_219
	nop
# BB#192:                               #   in Loop: Header=BB2_2 Depth=2
	beq	$16, $zero, $BB2_209
	nop
$BB2_193:                               #   in Loop: Header=BB2_2 Depth=2
	addiu	$2, $zero, 2047
	bne	$16, $2, $BB2_210
	nop
$BB2_194:                               #   in Loop: Header=BB2_2 Depth=2
	or	$2, $22, $20
	or	$3, $18, $19
	or	$2, $2, $3
	beq	$2, $zero, $BB2_208
	nop
# BB#195:                               #   in Loop: Header=BB2_2 Depth=2
	lui	$3, 32760
	lui	$2, 32752
	ori	$3, $3, 0
	and	$3, $10, $3
	ori	$2, $2, 0
	xor	$2, $3, $2
	beq	$2, $zero, $BB2_197
	nop
# BB#196:                               #   in Loop: Header=BB2_2 Depth=2
	addiu	$2, $zero, 0
	j	$BB2_198
	nop
$BB2_197:                               #   in Loop: Header=BB2_2 Depth=2
	lui	$2, 7
	ori	$2, $2, 65535
	and	$2, $10, $2
	or	$2, $9, $2
	addiu	$3, $zero, 0
	xor	$2, $2, $3
	sltu	$2, $zero, $2
$BB2_198:                               # %float64_is_signaling_nan.exit1.i11.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lui	$3, 32767
	addiu	$4, $zero, 0
	lui	$5, 32752
	ori	$3, $3, 65535
	lui	$6, 32760
	ori	$5, $5, 0
	and	$3, $21, $3
	ori	$6, $6, 0
	xor	$7, $3, $5
	beq	$7, $zero, $BB2_200
	nop
# BB#199:                               #   in Loop: Header=BB2_2 Depth=2
	sltu	$3, $5, $3
	j	$BB2_201
	nop
$BB2_200:                               # %float64_is_signaling_nan.exit1.i11.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lw	$3, 80($sp)
	nop
	xor	$3, $3, $4
	sltu	$3, $zero, $3
$BB2_201:                               # %float64_is_signaling_nan.exit1.i11.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	and	$6, $21, $6
	xor	$5, $6, $5
	bne	$5, $zero, $BB2_203
	nop
# BB#202:                               #   in Loop: Header=BB2_2 Depth=2
	lui	$4, 7
	ori	$4, $4, 65535
	and	$4, $21, $4
	lw	$5, 80($sp)
	nop
	or	$4, $5, $4
	addiu	$5, $zero, 0
	xor	$4, $4, $5
	sltu	$4, $zero, $4
$BB2_203:                               # %float64_is_signaling_nan.exit.i12.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	or	$2, $2, $4
	addiu	$5, $zero, 1
	bne	$2, $5, $BB2_205
	nop
# BB#204:                               # %.thread.i13.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lui	$2, %hi(float_exception_flags)
	lw	$3, %lo(float_exception_flags)($2)
	nop
	ori	$3, $3, 16
	sw	$3, %lo(float_exception_flags)($2)
	addu	$3, $zero, $4
$BB2_205:                               # %propagateFloat64NaN.exit17.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lui	$2, 8
	beq	$3, $zero, $BB2_207
	nop
# BB#206:                               # %propagateFloat64NaN.exit17.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	addu	$10, $zero, $21
$BB2_207:                               # %propagateFloat64NaN.exit17.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	bne	$3, $zero, $BB2_131
	nop
	j	$BB2_132
	nop
$BB2_208:                               #   in Loop: Header=BB2_2 Depth=2
	lui	$2, %hi(float_exception_flags)
	lw	$3, %lo(float_exception_flags)($2)
	nop
	lui	$4, 32767
	ori	$3, $3, 16
	addiu	$9, $zero, -1
	ori	$10, $4, 65535
	sw	$3, %lo(float_exception_flags)($2)
	j	$BB2_278
	nop
$BB2_209:                               #   in Loop: Header=BB2_2 Depth=2
	addiu	$16, $zero, 1
	addu	$23, $zero, $16
$BB2_210:                               #   in Loop: Header=BB2_2 Depth=2
	xor	$2, $18, $19
	beq	$2, $zero, $BB2_212
	nop
# BB#211:                               #   in Loop: Header=BB2_2 Depth=2
	sltu	$3, $18, $19
	j	$BB2_213
	nop
$BB2_212:                               #   in Loop: Header=BB2_2 Depth=2
	sltu	$3, $22, $20
$BB2_213:                               #   in Loop: Header=BB2_2 Depth=2
	beq	$3, $zero, $BB2_215
	nop
# BB#214:                               #   in Loop: Header=BB2_2 Depth=2
	addu	$3, $zero, $18
	j	$BB2_266
	nop
$BB2_215:                               #   in Loop: Header=BB2_2 Depth=2
	addiu	$9, $zero, 0
	lw	$3, 76($sp)
	nop
	beq	$2, $zero, $BB2_217
	nop
# BB#216:                               #   in Loop: Header=BB2_2 Depth=2
	sltu	$2, $19, $18
	j	$BB2_218
	nop
$BB2_217:                               #   in Loop: Header=BB2_2 Depth=2
	sltu	$2, $20, $22
$BB2_218:                               #   in Loop: Header=BB2_2 Depth=2
	addu	$10, $zero, $9
	bne	$2, $zero, $BB2_242
	nop
	j	$BB2_278
	nop
$BB2_219:                               #   in Loop: Header=BB2_2 Depth=2
	addiu	$3, $zero, 2047
	bne	$23, $3, $BB2_235
	nop
# BB#220:                               #   in Loop: Header=BB2_2 Depth=2
	or	$2, $22, $18
	beq	$2, $zero, $BB2_234
	nop
# BB#221:                               #   in Loop: Header=BB2_2 Depth=2
	lui	$3, 32760
	lui	$2, 32752
	ori	$3, $3, 0
	and	$3, $10, $3
	ori	$2, $2, 0
	xor	$2, $3, $2
	beq	$2, $zero, $BB2_223
	nop
# BB#222:                               #   in Loop: Header=BB2_2 Depth=2
	addiu	$2, $zero, 0
	j	$BB2_224
	nop
$BB2_223:                               #   in Loop: Header=BB2_2 Depth=2
	lui	$2, 7
	ori	$2, $2, 65535
	and	$2, $10, $2
	or	$2, $9, $2
	addiu	$3, $zero, 0
	xor	$2, $2, $3
	sltu	$2, $zero, $2
$BB2_224:                               # %float64_is_signaling_nan.exit1.i4.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lui	$3, 32767
	addiu	$4, $zero, 0
	lui	$5, 32752
	ori	$3, $3, 65535
	lui	$6, 32760
	ori	$5, $5, 0
	and	$3, $21, $3
	ori	$6, $6, 0
	xor	$7, $3, $5
	beq	$7, $zero, $BB2_226
	nop
# BB#225:                               #   in Loop: Header=BB2_2 Depth=2
	sltu	$3, $5, $3
	j	$BB2_227
	nop
$BB2_226:                               # %float64_is_signaling_nan.exit1.i4.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lw	$3, 80($sp)
	nop
	xor	$3, $3, $4
	sltu	$3, $zero, $3
$BB2_227:                               # %float64_is_signaling_nan.exit1.i4.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	and	$6, $21, $6
	xor	$5, $6, $5
	bne	$5, $zero, $BB2_229
	nop
# BB#228:                               #   in Loop: Header=BB2_2 Depth=2
	lui	$4, 7
	ori	$4, $4, 65535
	and	$4, $21, $4
	lw	$5, 80($sp)
	nop
	or	$4, $5, $4
	addiu	$5, $zero, 0
	xor	$4, $4, $5
	sltu	$4, $zero, $4
$BB2_229:                               # %float64_is_signaling_nan.exit.i5.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	or	$2, $2, $4
	addiu	$5, $zero, 1
	bne	$2, $5, $BB2_231
	nop
# BB#230:                               # %.thread.i6.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lui	$2, %hi(float_exception_flags)
	lw	$3, %lo(float_exception_flags)($2)
	nop
	ori	$3, $3, 16
	sw	$3, %lo(float_exception_flags)($2)
	addu	$3, $zero, $4
$BB2_231:                               # %propagateFloat64NaN.exit10.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lui	$2, 8
	beq	$3, $zero, $BB2_233
	nop
# BB#232:                               # %propagateFloat64NaN.exit10.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	addu	$10, $zero, $21
$BB2_233:                               # %propagateFloat64NaN.exit10.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	bne	$3, $zero, $BB2_131
	nop
	j	$BB2_132
	nop
$BB2_234:                               #   in Loop: Header=BB2_2 Depth=2
	lw	$2, 76($sp)
	nop
	nor	$2, $2, $zero
	lui	$3, 32752
	j	$BB2_156
	nop
$BB2_235:                               #   in Loop: Header=BB2_2 Depth=2
	addiu	$3, $zero, 0
	xor	$5, $16, $3
	lui	$4, 16384
	sltu	$5, $5, 1
	addu	$2, $2, $5
	beq	$16, $zero, $BB2_237
	nop
# BB#236:                               #   in Loop: Header=BB2_2 Depth=2
	ori	$4, $4, 0
	or	$19, $19, $4
$BB2_237:                               #   in Loop: Header=BB2_2 Depth=2
	beq	$2, $zero, $BB2_241
	nop
# BB#238:                               #   in Loop: Header=BB2_2 Depth=2
	slti	$4, $2, -63
	bne	$4, $zero, $BB2_240
	nop
# BB#239:                               #   in Loop: Header=BB2_2 Depth=2
	subu	$16, $3, $2
	andi	$6, $2, 63
	addu	$4, $zero, $20
	addu	$5, $zero, $19
	jal	__ashldi3
	nop
	or	$2, $2, $3
	addiu	$3, $zero, 0
	addu	$17, $zero, $21
	xor	$21, $2, $3
	addu	$4, $zero, $20
	addu	$5, $zero, $19
	addu	$6, $zero, $16
	jal	__lshrdi3
	nop
	addu	$19, $zero, $3
	sltu	$3, $zero, $21
	addu	$21, $zero, $17
	or	$20, $3, $2
	j	$BB2_241
	nop
$BB2_240:                               #   in Loop: Header=BB2_2 Depth=2
	addiu	$2, $zero, 0
	or	$3, $20, $19
	xor	$3, $3, $2
	sltu	$20, $zero, $3
	addu	$19, $zero, $2
$BB2_241:                               # %shift64RightJamming.exit3.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lui	$2, 16384
	ori	$2, $2, 0
	or	$18, $18, $2
	lw	$3, 76($sp)
	nop
$BB2_242:                               #   in Loop: Header=BB2_2 Depth=2
	sltu	$2, $22, $20
	addu	$2, $2, $19
	xori	$3, $3, 1
	sw	$3, 76($sp)
	subu	$4, $22, $20
	subu	$5, $18, $2
	j	$BB2_267
	nop
$BB2_243:                               #   in Loop: Header=BB2_2 Depth=2
	addiu	$3, $zero, 2047
	bne	$16, $3, $BB2_258
	nop
# BB#244:                               #   in Loop: Header=BB2_2 Depth=2
	or	$2, $20, $19
	beq	$2, $zero, $BB2_278
	nop
# BB#245:                               #   in Loop: Header=BB2_2 Depth=2
	lui	$3, 32760
	lui	$2, 32752
	ori	$3, $3, 0
	and	$3, $10, $3
	ori	$2, $2, 0
	xor	$2, $3, $2
	beq	$2, $zero, $BB2_247
	nop
# BB#246:                               #   in Loop: Header=BB2_2 Depth=2
	addiu	$2, $zero, 0
	j	$BB2_248
	nop
$BB2_247:                               #   in Loop: Header=BB2_2 Depth=2
	lui	$2, 7
	ori	$2, $2, 65535
	and	$2, $10, $2
	or	$2, $9, $2
	addiu	$3, $zero, 0
	xor	$2, $2, $3
	sltu	$2, $zero, $2
$BB2_248:                               # %float64_is_signaling_nan.exit1.i.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lui	$3, 32767
	addiu	$4, $zero, 0
	lui	$5, 32752
	ori	$3, $3, 65535
	lui	$6, 32760
	ori	$5, $5, 0
	and	$3, $21, $3
	ori	$6, $6, 0
	xor	$7, $3, $5
	beq	$7, $zero, $BB2_250
	nop
# BB#249:                               #   in Loop: Header=BB2_2 Depth=2
	sltu	$3, $5, $3
	j	$BB2_251
	nop
$BB2_250:                               # %float64_is_signaling_nan.exit1.i.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lw	$3, 80($sp)
	nop
	xor	$3, $3, $4
	sltu	$3, $zero, $3
$BB2_251:                               # %float64_is_signaling_nan.exit1.i.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	and	$6, $21, $6
	xor	$5, $6, $5
	bne	$5, $zero, $BB2_253
	nop
# BB#252:                               #   in Loop: Header=BB2_2 Depth=2
	lui	$4, 7
	ori	$4, $4, 65535
	and	$4, $21, $4
	lw	$5, 80($sp)
	nop
	or	$4, $5, $4
	addiu	$5, $zero, 0
	xor	$4, $4, $5
	sltu	$4, $zero, $4
$BB2_253:                               # %float64_is_signaling_nan.exit.i.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	or	$2, $2, $4
	addiu	$5, $zero, 1
	bne	$2, $5, $BB2_255
	nop
# BB#254:                               # %.thread.i.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lui	$2, %hi(float_exception_flags)
	lw	$3, %lo(float_exception_flags)($2)
	nop
	ori	$3, $3, 16
	sw	$3, %lo(float_exception_flags)($2)
	addu	$3, $zero, $4
$BB2_255:                               # %propagateFloat64NaN.exit.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lui	$2, 8
	beq	$3, $zero, $BB2_257
	nop
# BB#256:                               # %propagateFloat64NaN.exit.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	addu	$10, $zero, $21
$BB2_257:                               # %propagateFloat64NaN.exit.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	bne	$3, $zero, $BB2_131
	nop
	j	$BB2_132
	nop
$BB2_258:                               #   in Loop: Header=BB2_2 Depth=2
	lui	$4, 16384
	addiu	$3, $zero, 0
	xor	$3, $23, $3
	beq	$23, $zero, $BB2_260
	nop
# BB#259:                               #   in Loop: Header=BB2_2 Depth=2
	ori	$4, $4, 0
	or	$18, $18, $4
$BB2_260:                               #   in Loop: Header=BB2_2 Depth=2
	sltu	$3, $3, 1
	subu	$23, $2, $3
	bne	$23, $zero, $BB2_262
	nop
# BB#261:                               #   in Loop: Header=BB2_2 Depth=2
	addu	$3, $zero, $18
	j	$BB2_265
	nop
$BB2_262:                               #   in Loop: Header=BB2_2 Depth=2
	addiu	$2, $zero, 63
	slt	$2, $2, $23
	bne	$2, $zero, $BB2_264
	nop
# BB#263:                               #   in Loop: Header=BB2_2 Depth=2
	addu	$17, $zero, $21
	addiu	$21, $zero, 0
	subu	$2, $21, $23
	andi	$6, $2, 63
	addu	$4, $zero, $22
	addu	$5, $zero, $18
	jal	__ashldi3
	nop
	or	$2, $2, $3
	xor	$21, $2, $21
	addu	$4, $zero, $22
	addu	$5, $zero, $18
	addu	$6, $zero, $23
	jal	__lshrdi3
	nop
	sltu	$4, $zero, $21
	addu	$21, $zero, $17
	or	$22, $4, $2
	j	$BB2_265
	nop
$BB2_264:                               #   in Loop: Header=BB2_2 Depth=2
	addiu	$3, $zero, 0
	or	$2, $22, $18
	xor	$2, $2, $3
	sltu	$22, $zero, $2
$BB2_265:                               # %shift64RightJamming.exit.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lui	$2, 16384
	ori	$2, $2, 0
	or	$19, $19, $2
$BB2_266:                               #   in Loop: Header=BB2_2 Depth=2
	sltu	$2, $20, $22
	addu	$2, $2, $3
	subu	$4, $20, $22
	subu	$5, $19, $2
	addu	$23, $zero, $16
$BB2_267:                               #   in Loop: Header=BB2_2 Depth=2
	lui	$3, 1
	addu	$2, $zero, $5
	bne	$5, $zero, $BB2_269
	nop
# BB#268:                               #   in Loop: Header=BB2_2 Depth=2
	addu	$2, $zero, $4
$BB2_269:                               #   in Loop: Header=BB2_2 Depth=2
	ori	$3, $3, 0
	lui	$6, 255
	sltu	$3, $2, $3
	beq	$3, $zero, $BB2_271
	nop
# BB#270:                               #   in Loop: Header=BB2_2 Depth=2
	sll	$2, $2, 16
$BB2_271:                               #   in Loop: Header=BB2_2 Depth=2
	ori	$6, $6, 65535
	sll	$3, $3, 4
	sltu	$6, $6, $2
	bne	$6, $zero, $BB2_273
	nop
# BB#272:                               #   in Loop: Header=BB2_2 Depth=2
	ori	$3, $3, 8
	sll	$2, $2, 8
$BB2_273:                               # %normalizeRoundAndPackFloat64.exit.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	addiu	$18, $23, -1
	lui	$6, %hi(countLeadingZeros32.countLeadingZerosHigh)
	srl	$2, $2, 22
	beq	$5, $zero, $BB2_275
	nop
# BB#274:                               #   in Loop: Header=BB2_2 Depth=2
	addiu	$7, $zero, -1
	j	$BB2_276
	nop
$BB2_275:                               # %normalizeRoundAndPackFloat64.exit.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	addiu	$7, $zero, 31
$BB2_276:                               # %normalizeRoundAndPackFloat64.exit.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	addiu	$6, $6, %lo(countLeadingZeros32.countLeadingZerosHigh)
	andi	$2, $2, 1020
	addu	$6, $6, $2
	addu	$2, $3, $7
	lw	$3, 0($6)
	nop
	addu	$16, $2, $3
	addu	$6, $zero, $16
	jal	__ashldi3
	nop
	subu	$5, $18, $16
	lw	$4, 76($sp)
	nop
	addu	$6, $zero, $2
	addu	$7, $zero, $3
$BB2_277:                               # %normalizeRoundAndPackFloat64.exit.i.i.i
                                        #   in Loop: Header=BB2_2 Depth=2
	jal	roundAndPackFloat64
	nop
	addu	$9, $zero, $2
	addu	$10, $zero, $3
$BB2_278:                               # %float64_add.exit.i
                                        #   in Loop: Header=BB2_2 Depth=2
	lui	$2, 15
	lui	$3, 32752
	ori	$3, $3, 0
	and	$4, $21, $3
	bne	$4, $3, $BB2_281
	nop
# BB#279:                               # %float64_add.exit.i
                                        #   in Loop: Header=BB2_2 Depth=2
	ori	$2, $2, 65535
	and	$2, $21, $2
	lw	$3, 80($sp)
	nop
	or	$2, $3, $2
	beq	$2, $zero, $BB2_281
	nop
# BB#280:                               #   in Loop: Header=BB2_1 Depth=1
	sw	$10, 72($sp)
	sw	$9, 76($sp)
	lui	$2, %hi(float_exception_flags)
	lw	$3, %lo(float_exception_flags)($2)
	nop
	ori	$3, $3, 16
	sw	$3, %lo(float_exception_flags)($2)
	j	$BB2_285
	nop
$BB2_281:                               # %float64_ge.exit.i
                                        #   in Loop: Header=BB2_2 Depth=2
	sw	$10, 72($sp)
	sw	$9, 76($sp)
	lui	$3, 32767
	lui	$2, 35043
	lui	$5, 16100
	ori	$4, $3, 65535
	ori	$3, $5, 63669
	and	$4, $21, $4
	lw	$20, 64($sp)
	nop
	addiu	$20, $20, 2
	xor	$5, $4, $3
	beq	$5, $zero, $BB2_283
	nop
# BB#282:                               #   in Loop: Header=BB2_2 Depth=2
	sltu	$2, $3, $4
	j	$BB2_284
	nop
$BB2_283:                               # %float64_ge.exit.i
                                        #   in Loop: Header=BB2_2 Depth=2
	ori	$2, $2, 26864
	lw	$3, 80($sp)
	nop
	sltu	$2, $2, $3
$BB2_284:                               # %float64_ge.exit.i
                                        #   in Loop: Header=BB2_2 Depth=2
	bne	$2, $zero, $BB2_2
	nop
$BB2_285:                               # %dfsin.exit
                                        #   in Loop: Header=BB2_1 Depth=1
	lui	$2, %hi(test_out)
	addiu	$2, $2, %lo(test_out)
	lw	$3, 32($sp)
	nop
	subu	$2, $2, $3
	lw	$3, 0($2)
	nop
	lw	$2, 4($2)
	nop
	lw	$6, 76($sp)
	nop
	xor	$4, $6, $3
	lw	$7, 72($sp)
	nop
	xor	$5, $7, $2
	or	$4, $4, $5
	addiu	$5, $zero, 0
	xor	$5, $4, $5
	lui	$4, %hi($.str)
	sltu	$5, $5, 1
	lw	$18, 44($sp)
	nop
	addiu	$18, $18, -1
	lw	$17, 48($sp)
	nop
	addu	$17, $5, $17
	sw	$3, 16($sp)
	sw	$2, 20($sp)
	sw	$6, 24($sp)
	sw	$7, 28($sp)
	addiu	$4, $4, %lo($.str)
	addiu	$16, $zero, -36
	lw	$6, 36($sp)
	nop
	lw	$7, 40($sp)
	nop
	jal	printf
	nop
	bne	$18, $16, $BB2_1
	nop
# BB#286:
	lui	$2, %hi($.str1)
	addiu	$4, $2, %lo($.str1)
	addiu	$16, $zero, 36
	addu	$5, $zero, $17
	jal	printf
	nop
	bne	$17, $16, $BB2_288
	nop
# BB#287:
	lui	$2, %hi($.str2)
	addiu	$4, $2, %lo($.str2)
	j	$BB2_289
	nop
$BB2_288:
	lui	$2, %hi($.str3)
	addiu	$4, $2, %lo($.str3)
$BB2_289:
	jal	printf
	nop
	addu	$2, $zero, $17
	lw	$16, 84($sp)
	nop
	lw	$17, 88($sp)
	nop
	lw	$18, 92($sp)
	nop
	lw	$19, 96($sp)
	nop
	lw	$20, 100($sp)
	nop
	lw	$21, 104($sp)
	nop
	lw	$22, 108($sp)
	nop
	lw	$23, 112($sp)
	nop
	lw	$ra, 116($sp)
	nop
	addiu	$sp, $sp, 120
	jr	$ra
	nop
	.set	macro
	.set	reorder
	.end	main
$tmp40:
	.size	main, ($tmp40)-main
$tmp41:
	.cfi_endproc
$eh_func_end2:

	.type	float_exception_flags,@object # @float_exception_flags
	.local	float_exception_flags
	.comm	float_exception_flags,4,4
	.type	test_in,@object         # @test_in
	.section	.rodata,"a",@progbits
	.align	3
test_in:
	.4byte	0                       # 0x0
	.4byte	0
	.4byte	4243412417              # 0x3fc65717fced55c1
	.4byte	1069963031
	.4byte	4243412417              # 0x3fd65717fced55c1
	.4byte	1071011607
	.4byte	4256301137              # 0x3fe0c151fdb20051
	.4byte	1071694161
	.4byte	4243412417              # 0x3fe65717fced55c1
	.4byte	1072060183
	.4byte	4230523697              # 0x3febecddfc28ab31
	.4byte	1072426205
	.4byte	4256301137              # 0x3ff0c151fdb20051
	.4byte	1072742737
	.4byte	4249856777              # 0x3ff38c34fd4fab09
	.4byte	1072925748
	.4byte	4243412417              # 0x3ff65717fced55c1
	.4byte	1073108759
	.4byte	4236968057              # 0x3ff921fafc8b0079
	.4byte	1073291770
	.4byte	4230523697              # 0x3ffbecddfc28ab31
	.4byte	1073474781
	.4byte	4224079337              # 0x3ffeb7c0fbc655e9
	.4byte	1073657792
	.4byte	4256301137              # 0x4000c151fdb20051
	.4byte	1073791313
	.4byte	2105595309              # 0x400226c37d80d5ad
	.4byte	1073882819
	.4byte	4249856777              # 0x40038c34fd4fab09
	.4byte	1073974324
	.4byte	2099150949              # 0x4004f1a67d1e8065
	.4byte	1074065830
	.4byte	4243412417              # 0x40065717fced55c1
	.4byte	1074157335
	.4byte	2092706589              # 0x4007bc897cbc2b1d
	.4byte	1074248841
	.4byte	4236968057              # 0x400921fafc8b0079
	.4byte	1074340346
	.4byte	2086262229              # 0x400a876c7c59d5d5
	.4byte	1074431852
	.4byte	4230523697              # 0x400becddfc28ab31
	.4byte	1074523357
	.4byte	2079817869              # 0x400d524f7bf7808d
	.4byte	1074614863
	.4byte	4224079337              # 0x400eb7c0fbc655e9
	.4byte	1074706368
	.4byte	1036686755              # 0x40100e993dca95a3
	.4byte	1074794137
	.4byte	4256301137              # 0x4010c151fdb20051
	.4byte	1074839889
	.4byte	3180948223              # 0x4011740abd996aff
	.4byte	1074885642
	.4byte	2105595309              # 0x401226c37d80d5ad
	.4byte	1074931395
	.4byte	1030242395              # 0x4012d97c3d68405b
	.4byte	1074977148
	.4byte	4249856777              # 0x40138c34fd4fab09
	.4byte	1075022900
	.4byte	3174503863              # 0x40143eedbd3715b7
	.4byte	1075068653
	.4byte	2099150949              # 0x4014f1a67d1e8065
	.4byte	1075114406
	.4byte	1023798035              # 0x4015a45f3d05eb13
	.4byte	1075160159
	.4byte	4243412417              # 0x40165717fced55c1
	.4byte	1075205911
	.4byte	3168059503              # 0x401709d0bcd4c06f
	.4byte	1075251664
	.4byte	2092706589              # 0x4017bc897cbc2b1d
	.4byte	1075297417
	.4byte	1017353675              # 0x40186f423ca395cb
	.4byte	1075343170
	.size	test_in, 288

	.type	test_out,@object        # @test_out
	.align	3
test_out:
	.4byte	0                       # 0x0
	.4byte	0
	.4byte	861580749               # 0x3fc63a1a335aadcd
	.4byte	1069955610
	.4byte	722059070               # 0x3fd5e3a82b09bf3e
	.4byte	1070982056
	.4byte	2449058449              # 0x3fdfffff91f9aa91
	.4byte	1071644671
	.4byte	381829859               # 0x3fe491b716c242e3
	.4byte	1071944119
	.4byte	1730548902              # 0x3fe8836f672614a6
	.4byte	1072202607
	.4byte	3289066477              # 0x3febb67ac40b2bed
	.4byte	1072412282
	.4byte	310257837               # 0x3fee11f6127e28ad
	.4byte	1072566774
	.4byte	1793063616              # 0x3fef838b6adffac0
	.4byte	1072661387
	.4byte	3788232618              # 0x3fefffffe1cbd7aa
	.4byte	1072693247
	.4byte	2954131849              # 0x3fef838bb0147989
	.4byte	1072661387
	.4byte	2463720116              # 0x3fee11f692d962b4
	.4byte	1072566774
	.4byte	2009076781              # 0x3febb67b77c0142d
	.4byte	1072412283
	.4byte	2639177833              # 0x3fe883709d4ea869
	.4byte	1072202608
	.4byte	494065896               # 0x3fe491b81d72d8e8
	.4byte	1071944120
	.4byte	3932111816              # 0x3fe00000ea5f43c8
	.4byte	1071644672
	.4byte	1308987589              # 0x3fd5e3aa4e0590c5
	.4byte	1070982058
	.4byte	562648364               # 0x3fc63a1d2189552c
	.4byte	1069955613
	.4byte	4232399761              # 0x3ea6aedffc454b91
	.4byte	1051111135
	.4byte	1155380092              # 0xbfc63a1444ddb37c
	.4byte	3217439252
	.4byte	3868167998              # 0xbfd5e3a4e68f8f3e
	.4byte	3218465700
	.4byte	1229781355              # 0xbfdffffd494cf96b
	.4byte	3219128317
	.4byte	481928147               # 0xbfe491b61cb9a3d3
	.4byte	3219427766
	.4byte	3000825877              # 0xbfe8836eb2dcf815
	.4byte	3219686254
	.4byte	1946857010              # 0xbfebb67a740aae32
	.4byte	3219895930
	.4byte	2435653975              # 0xbfee11f5912d2157
	.4byte	3220050421
	.4byte	449202940               # 0xbfef838b1ac64afc
	.4byte	3220145035
	.4byte	3269844111              # 0xbfefffffc2e5dc8f
	.4byte	3220176895
	.4byte	1587734506              # 0xbfef838b5ea2e7ea
	.4byte	3220145035
	.4byte	288206375               # 0xbfee11f7112dae27
	.4byte	3220050423
	.4byte	741460810               # 0xbfebb67c2c31cb4a
	.4byte	3219895932
	.4byte	1852823425              # 0xbfe883716e6fd781
	.4byte	3219686257
	.4byte	3441122646              # 0xbfe491b9cd1b5d56
	.4byte	3219427769
	.4byte	487367437               # 0xbfe000021d0ca30d
	.4byte	3219128322
	.4byte	174705399               # 0xbfd5e3ad0a69caf7
	.4byte	3218465709
	.4byte	3297272797              # 0xbfc63a23c48863dd
	.4byte	3217439267
	.size	test_out, 288

	.type	$.str,@object           # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
$.str:
	.asciz	 "input=%016llx expected=%016llx output=%016llx\n"
	.size	$.str, 47

	.type	$.str1,@object          # @.str1
$.str1:
	.asciz	 "Result: %d\n"
	.size	$.str1, 12

	.type	$.str2,@object          # @.str2
$.str2:
	.asciz	 "RESULT: PASS\n"
	.size	$.str2, 14

	.type	$.str3,@object          # @.str3
$.str3:
	.asciz	 "RESULT: FAIL\n"
	.size	$.str3, 14

	.type	countLeadingZeros32.countLeadingZerosHigh,@object # @countLeadingZeros32.countLeadingZerosHigh
	.section	.rodata,"a",@progbits
	.align	2
countLeadingZeros32.countLeadingZerosHigh:
	.4byte	8                       # 0x8
	.4byte	7                       # 0x7
	.4byte	6                       # 0x6
	.4byte	6                       # 0x6
	.4byte	5                       # 0x5
	.4byte	5                       # 0x5
	.4byte	5                       # 0x5
	.4byte	5                       # 0x5
	.4byte	4                       # 0x4
	.4byte	4                       # 0x4
	.4byte	4                       # 0x4
	.4byte	4                       # 0x4
	.4byte	4                       # 0x4
	.4byte	4                       # 0x4
	.4byte	4                       # 0x4
	.4byte	4                       # 0x4
	.4byte	3                       # 0x3
	.4byte	3                       # 0x3
	.4byte	3                       # 0x3
	.4byte	3                       # 0x3
	.4byte	3                       # 0x3
	.4byte	3                       # 0x3
	.4byte	3                       # 0x3
	.4byte	3                       # 0x3
	.4byte	3                       # 0x3
	.4byte	3                       # 0x3
	.4byte	3                       # 0x3
	.4byte	3                       # 0x3
	.4byte	3                       # 0x3
	.4byte	3                       # 0x3
	.4byte	3                       # 0x3
	.4byte	3                       # 0x3
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.size	countLeadingZeros32.countLeadingZerosHigh, 1024


