	.section .mdebug.abi32
	.previous
	.file	"output/aes.sw.ll"
	.text
	.align	2
	.type	KeySchedule,@function
	.ent	KeySchedule             # @KeySchedule
KeySchedule:
$tmp2:
	.cfi_startproc
	.frame	$sp,16,$ra
	.mask 	0x00070000,-4
	.fmask	0x00000000,0
# BB#0:
	.set	noreorder
	.set	nomacro
	addiu	$sp, $sp, -16
$tmp3:
	.cfi_def_cfa_offset 16
	sw	$18, 12($sp)
	sw	$17, 8($sp)
	sw	$16, 4($sp)
$tmp4:
	.cfi_offset 18, -4
$tmp5:
	.cfi_offset 17, -8
$tmp6:
	.cfi_offset 16, -12
	addiu	$2, $zero, 0
$BB0_1:                                 # %.preheader4
                                        # =>This Inner Loop Header: Depth=1
	lui	$3, %hi(key)
	lui	$4, %hi(word)
	addiu	$3, $3, %lo(key)
	sll	$5, $2, 4
	addu	$3, $3, $5
	addiu	$4, $4, %lo(word)
	sll	$6, $2, 2
	lw	$5, 0($3)
	nop
	addu	$4, $4, $6
	sw	$5, 0($4)
	lw	$5, 4($3)
	nop
	sw	$5, 480($4)
	lw	$5, 8($3)
	nop
	sw	$5, 960($4)
	lw	$5, 12($3)
	nop
	addiu	$2, $2, 1
	addiu	$3, $zero, 4
	sw	$5, 1440($4)
	bne	$2, $3, $BB0_1
	nop
# BB#2:                                 # %.lr.ph..lr.ph.split_crit_edge.preheader
	lui	$2, %hi(word)
	addiu	$2, $2, %lo(word)
	lw	$4, 12($2)
	nop
	addiu	$2, $zero, 0
	addiu	$3, $zero, 4
	addiu	$18, $zero, -4
$BB0_3:                                 # %.lr.ph..lr.ph.split_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	sra	$5, $3, 31
	lui	$6, %hi(word)
	srl	$5, $5, 30
	addiu	$6, $6, %lo(word)
	addu	$6, $6, $2
	addu	$5, $3, $5
	addiu	$7, $zero, -4
	and	$7, $5, $7
	subu	$7, $3, $7
	bne	$7, $zero, $BB0_7
	nop
# BB#4:                                 # %.thread
                                        #   in Loop: Header=BB0_3 Depth=1
	lui	$7, %hi(word)
	lw	$8, 492($6)
	nop
	addiu	$6, $7, %lo(word)
	addu	$6, $6, $2
	sra	$7, $8, 31
	srl	$9, $7, 28
	lw	$7, 972($6)
	nop
	lw	$6, 1452($6)
	nop
	lui	$10, 16383
	addu	$9, $8, $9
	sra	$13, $7, 31
	sra	$12, $6, 31
	sra	$15, $4, 31
	ori	$10, $10, 65520
	and	$11, $9, $10
	srl	$14, $13, 28
	srl	$13, $12, 28
	lui	$12, %hi(Sbox)
	srl	$24, $15, 28
	sll	$15, $9, 2
	addiu	$9, $zero, -64
	addu	$14, $7, $14
	addu	$13, $6, $13
	addu	$16, $4, $24
	lui	$25, %hi(Rcon0)
	subu	$11, $8, $11
	and	$17, $15, $9
	addiu	$8, $12, %lo(Sbox)
	sll	$24, $14, 2
	and	$15, $14, $10
	sll	$14, $13, 2
	and	$13, $13, $10
	sll	$12, $16, 2
	and	$10, $16, $10
	and	$5, $5, $18
	addiu	$16, $25, %lo(Rcon0)
	addu	$25, $8, $17
	sll	$11, $11, 2
	addu	$16, $5, $16
	addu	$25, $25, $11
	and	$24, $24, $9
	subu	$15, $7, $15
	and	$11, $14, $9
	subu	$7, $6, $13
	and	$6, $12, $9
	subu	$5, $4, $10
	lw	$4, -4($16)
	nop
	lw	$10, 0($25)
	nop
	addu	$9, $8, $24
	sll	$12, $15, 2
	addu	$11, $8, $11
	sll	$7, $7, 2
	addu	$6, $8, $6
	sll	$8, $5, 2
	xor	$4, $4, $10
	addu	$5, $9, $12
	addu	$7, $11, $7
	addu	$6, $6, $8
$BB0_5:                                 # %.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	lui	$8, %hi(word)
	addiu	$8, $8, %lo(word)
	addu	$8, $8, $2
	lw	$9, 0($8)
	nop
	xor	$4, $4, $9
	lw	$9, 0($5)
	nop
	lw	$7, 0($7)
	nop
	lw	$5, 0($6)
	nop
	sw	$4, 16($8)
	lw	$6, 480($8)
	nop
	xor	$6, $9, $6
	sw	$6, 496($8)
	lw	$6, 960($8)
	nop
	xor	$6, $7, $6
	sw	$6, 976($8)
	lw	$6, 1440($8)
	nop
	addiu	$2, $2, 4
	addiu	$3, $3, 1
	xor	$6, $5, $6
	addiu	$5, $zero, 160
	sw	$6, 1456($8)
	bne	$2, $5, $BB0_3
	nop
# BB#6:                                 # %.loopexit
	lw	$16, 4($sp)
	nop
	lw	$17, 8($sp)
	nop
	lw	$18, 12($sp)
	nop
	addiu	$sp, $sp, 16
	jr	$ra
	nop
$BB0_7:                                 #   in Loop: Header=BB0_3 Depth=1
	addiu	$5, $6, 492
	lui	$6, %hi(word)
	addiu	$6, $6, %lo(word)
	addu	$7, $6, $2
	addiu	$6, $7, 1452
	addiu	$7, $7, 972
	j	$BB0_5
	nop
	.set	macro
	.set	reorder
	.end	KeySchedule
$tmp7:
	.size	KeySchedule, ($tmp7)-KeySchedule
$tmp8:
	.cfi_endproc
$eh_func_end0:

	.align	2
	.type	ByteSub_ShiftRow,@function
	.ent	ByteSub_ShiftRow        # @ByteSub_ShiftRow
ByteSub_ShiftRow:
$tmp9:
	.cfi_startproc
	.frame	$sp,0,$ra
	.mask 	0x00000000,0
	.fmask	0x00000000,0
# BB#0:
	.set	noreorder
	.set	nomacro
	lui	$5, %hi(statemt)
	addiu	$2, $5, %lo(statemt)
	lw	$6, 20($2)
	nop
	lui	$3, %hi(Sbox)
	sll	$7, $6, 2
	addiu	$4, $zero, -64
	andi	$6, $6, 15
	addiu	$3, $3, %lo(Sbox)
	and	$7, $7, $4
	addu	$7, $3, $7
	sll	$6, $6, 2
	addu	$7, $7, $6
	lw	$6, 4($2)
	nop
	lw	$7, 0($7)
	nop
	sw	$7, 4($2)
	lw	$7, 36($2)
	nop
	sll	$8, $7, 2
	and	$9, $8, $4
	andi	$8, $7, 15
	addu	$7, $3, $9
	sll	$8, $8, 2
	addu	$7, $7, $8
	lw	$7, 0($7)
	nop
	sw	$7, 20($2)
	lw	$7, 52($2)
	nop
	sll	$8, $7, 2
	sll	$10, $6, 2
	and	$9, $8, $4
	andi	$8, $7, 15
	and	$7, $10, $4
	andi	$6, $6, 15
	addu	$9, $3, $9
	sll	$8, $8, 2
	addu	$8, $9, $8
	addu	$7, $3, $7
	sll	$6, $6, 2
	addu	$6, $7, $6
	lw	$7, 0($8)
	nop
	lw	$6, 0($6)
	nop
	sw	$7, 36($2)
	sw	$6, 52($2)
	lw	$7, 40($2)
	nop
	lw	$6, 8($2)
	nop
	sll	$8, $7, 2
	sll	$10, $6, 2
	and	$9, $8, $4
	andi	$8, $7, 15
	and	$7, $10, $4
	andi	$6, $6, 15
	addu	$9, $3, $9
	sll	$8, $8, 2
	addu	$8, $9, $8
	addu	$7, $3, $7
	sll	$6, $6, 2
	addu	$6, $7, $6
	lw	$7, 0($8)
	nop
	lw	$6, 0($6)
	nop
	sw	$7, 8($2)
	sw	$6, 40($2)
	lw	$7, 56($2)
	nop
	lw	$6, 24($2)
	nop
	sll	$8, $7, 2
	sll	$10, $6, 2
	and	$9, $8, $4
	andi	$8, $7, 15
	and	$7, $10, $4
	andi	$6, $6, 15
	addu	$9, $3, $9
	sll	$8, $8, 2
	addu	$8, $9, $8
	addu	$7, $3, $7
	sll	$6, $6, 2
	addu	$6, $7, $6
	lw	$7, 0($8)
	nop
	lw	$6, 0($6)
	nop
	sw	$7, 24($2)
	sw	$6, 56($2)
	lw	$6, 60($2)
	nop
	sll	$7, $6, 2
	and	$8, $7, $4
	andi	$7, $6, 15
	addu	$6, $3, $8
	sll	$7, $7, 2
	addu	$7, $6, $7
	lw	$6, 12($2)
	nop
	lw	$7, 0($7)
	nop
	sw	$7, 12($2)
	lw	$7, 44($2)
	nop
	sll	$8, $7, 2
	and	$9, $8, $4
	andi	$8, $7, 15
	addu	$7, $3, $9
	sll	$8, $8, 2
	addu	$7, $7, $8
	lw	$7, 0($7)
	nop
	sw	$7, 60($2)
	lw	$7, 28($2)
	nop
	sll	$8, $7, 2
	sll	$10, $6, 2
	and	$9, $8, $4
	andi	$8, $7, 15
	and	$7, $10, $4
	andi	$6, $6, 15
	addu	$9, $3, $9
	sll	$8, $8, 2
	addu	$8, $9, $8
	addu	$7, $3, $7
	sll	$6, $6, 2
	addu	$6, $7, $6
	lw	$7, 0($8)
	nop
	lw	$6, 0($6)
	nop
	sw	$7, 44($2)
	sw	$6, 28($2)
	lw	$6, %lo(statemt)($5)
	nop
	sll	$7, $6, 2
	and	$8, $7, $4
	andi	$7, $6, 15
	addu	$6, $3, $8
	sll	$7, $7, 2
	addu	$6, $6, $7
	lw	$6, 0($6)
	nop
	sw	$6, %lo(statemt)($5)
	lw	$5, 16($2)
	nop
	sll	$6, $5, 2
	and	$7, $6, $4
	andi	$6, $5, 15
	addu	$5, $3, $7
	sll	$6, $6, 2
	addu	$5, $5, $6
	lw	$5, 0($5)
	nop
	sw	$5, 16($2)
	lw	$5, 32($2)
	nop
	sll	$6, $5, 2
	and	$7, $6, $4
	andi	$6, $5, 15
	addu	$5, $3, $7
	sll	$6, $6, 2
	addu	$5, $5, $6
	lw	$5, 0($5)
	nop
	sw	$5, 32($2)
	lw	$5, 48($2)
	nop
	sll	$6, $5, 2
	and	$6, $6, $4
	andi	$4, $5, 15
	addu	$3, $3, $6
	sll	$4, $4, 2
	addu	$3, $3, $4
	lw	$3, 0($3)
	nop
	sw	$3, 48($2)
	jr	$ra
	nop
	.set	macro
	.set	reorder
	.end	ByteSub_ShiftRow
$tmp10:
	.size	ByteSub_ShiftRow, ($tmp10)-ByteSub_ShiftRow
$tmp11:
	.cfi_endproc
$eh_func_end1:

	.align	2
	.type	InversShiftRow_ByteSub,@function
	.ent	InversShiftRow_ByteSub  # @InversShiftRow_ByteSub
InversShiftRow_ByteSub:
$tmp12:
	.cfi_startproc
	.frame	$sp,0,$ra
	.mask 	0x00000000,0
	.fmask	0x00000000,0
# BB#0:
	.set	noreorder
	.set	nomacro
	lui	$5, %hi(statemt)
	addiu	$2, $5, %lo(statemt)
	lw	$6, 36($2)
	nop
	lui	$3, %hi(invSbox)
	sll	$7, $6, 2
	addiu	$4, $zero, -64
	andi	$6, $6, 15
	addiu	$3, $3, %lo(invSbox)
	and	$7, $7, $4
	addu	$7, $3, $7
	sll	$6, $6, 2
	addu	$7, $7, $6
	lw	$6, 52($2)
	nop
	lw	$7, 0($7)
	nop
	sw	$7, 52($2)
	lw	$7, 20($2)
	nop
	sll	$8, $7, 2
	and	$9, $8, $4
	andi	$8, $7, 15
	addu	$7, $3, $9
	sll	$8, $8, 2
	addu	$7, $7, $8
	lw	$7, 0($7)
	nop
	sw	$7, 36($2)
	lw	$7, 4($2)
	nop
	sll	$8, $7, 2
	sll	$10, $6, 2
	and	$9, $8, $4
	andi	$8, $7, 15
	and	$7, $10, $4
	andi	$6, $6, 15
	addu	$9, $3, $9
	sll	$8, $8, 2
	addu	$8, $9, $8
	addu	$7, $3, $7
	sll	$6, $6, 2
	addu	$6, $7, $6
	lw	$7, 0($8)
	nop
	lw	$6, 0($6)
	nop
	sw	$7, 20($2)
	sw	$6, 4($2)
	lw	$7, 24($2)
	nop
	lw	$6, 56($2)
	nop
	sll	$8, $7, 2
	sll	$10, $6, 2
	and	$9, $8, $4
	andi	$8, $7, 15
	and	$7, $10, $4
	andi	$6, $6, 15
	addu	$9, $3, $9
	sll	$8, $8, 2
	addu	$8, $9, $8
	addu	$7, $3, $7
	sll	$6, $6, 2
	addu	$6, $7, $6
	lw	$7, 0($8)
	nop
	lw	$6, 0($6)
	nop
	sw	$7, 56($2)
	sw	$6, 24($2)
	lw	$7, 40($2)
	nop
	lw	$6, 8($2)
	nop
	sll	$8, $7, 2
	sll	$10, $6, 2
	and	$9, $8, $4
	andi	$8, $7, 15
	and	$7, $10, $4
	andi	$6, $6, 15
	addu	$9, $3, $9
	sll	$8, $8, 2
	addu	$8, $9, $8
	addu	$7, $3, $7
	sll	$6, $6, 2
	addu	$6, $7, $6
	lw	$7, 0($8)
	nop
	lw	$6, 0($6)
	nop
	sw	$7, 8($2)
	sw	$6, 40($2)
	lw	$6, 12($2)
	nop
	sll	$7, $6, 2
	and	$8, $7, $4
	andi	$7, $6, 15
	addu	$6, $3, $8
	sll	$7, $7, 2
	addu	$7, $6, $7
	lw	$6, 60($2)
	nop
	lw	$7, 0($7)
	nop
	sw	$7, 60($2)
	lw	$7, 28($2)
	nop
	sll	$8, $7, 2
	and	$9, $8, $4
	andi	$8, $7, 15
	addu	$7, $3, $9
	sll	$8, $8, 2
	addu	$7, $7, $8
	lw	$7, 0($7)
	nop
	sw	$7, 12($2)
	lw	$7, 44($2)
	nop
	sll	$8, $7, 2
	sll	$10, $6, 2
	and	$9, $8, $4
	andi	$8, $7, 15
	and	$7, $10, $4
	andi	$6, $6, 15
	addu	$9, $3, $9
	sll	$8, $8, 2
	addu	$8, $9, $8
	addu	$7, $3, $7
	sll	$6, $6, 2
	addu	$6, $7, $6
	lw	$7, 0($8)
	nop
	lw	$6, 0($6)
	nop
	sw	$7, 28($2)
	sw	$6, 44($2)
	lw	$6, %lo(statemt)($5)
	nop
	sll	$7, $6, 2
	and	$8, $7, $4
	andi	$7, $6, 15
	addu	$6, $3, $8
	sll	$7, $7, 2
	addu	$6, $6, $7
	lw	$6, 0($6)
	nop
	sw	$6, %lo(statemt)($5)
	lw	$5, 16($2)
	nop
	sll	$6, $5, 2
	and	$7, $6, $4
	andi	$6, $5, 15
	addu	$5, $3, $7
	sll	$6, $6, 2
	addu	$5, $5, $6
	lw	$5, 0($5)
	nop
	sw	$5, 16($2)
	lw	$5, 32($2)
	nop
	sll	$6, $5, 2
	and	$7, $6, $4
	andi	$6, $5, 15
	addu	$5, $3, $7
	sll	$6, $6, 2
	addu	$5, $5, $6
	lw	$5, 0($5)
	nop
	sw	$5, 32($2)
	lw	$5, 48($2)
	nop
	sll	$6, $5, 2
	and	$6, $6, $4
	andi	$4, $5, 15
	addu	$3, $3, $6
	sll	$4, $4, 2
	addu	$3, $3, $4
	lw	$3, 0($3)
	nop
	sw	$3, 48($2)
	jr	$ra
	nop
	.set	macro
	.set	reorder
	.end	InversShiftRow_ByteSub
$tmp13:
	.size	InversShiftRow_ByteSub, ($tmp13)-InversShiftRow_ByteSub
$tmp14:
	.cfi_endproc
$eh_func_end2:

	.section	_main_section,"ax",@progbits
	.globl	main
	.align	2
	.type	main,@function
	.ent	main                    # @main
main:
$tmp17:
	.cfi_startproc
	.frame	$sp,176,$ra
	.mask 	0x803F0000,-4
	.fmask	0x00000000,0
# BB#0:
	.set	noreorder
	.set	nomacro
	addiu	$sp, $sp, -176
$tmp18:
	.cfi_def_cfa_offset 176
	sw	$ra, 172($sp)
	sw	$21, 168($sp)
	sw	$20, 164($sp)
	sw	$19, 160($sp)
	sw	$18, 156($sp)
	sw	$17, 152($sp)
	sw	$16, 148($sp)
$tmp19:
	.cfi_offset 31, -4
$tmp20:
	.cfi_offset 21, -8
$tmp21:
	.cfi_offset 20, -12
$tmp22:
	.cfi_offset 19, -16
$tmp23:
	.cfi_offset 18, -20
$tmp24:
	.cfi_offset 17, -24
$tmp25:
	.cfi_offset 16, -28
	lui	$2, %hi(statemt)
	addiu	$5, $zero, 50
	addiu	$3, $2, %lo(statemt)
	addiu	$4, $zero, 67
	sw	$5, %lo(statemt)($2)
	addiu	$2, $zero, 246
	sw	$4, 4($3)
	addiu	$4, $zero, 168
	sw	$2, 8($3)
	addiu	$2, $zero, 136
	sw	$4, 12($3)
	addiu	$5, $zero, 90
	sw	$2, 16($3)
	addiu	$4, $zero, 48
	sw	$5, 20($3)
	addiu	$5, $zero, 141
	sw	$4, 24($3)
	addiu	$4, $zero, 49
	sw	$5, 28($3)
	sw	$4, 32($3)
	addiu	$6, $zero, 152
	sw	$4, 36($3)
	addiu	$5, $zero, 162
	sw	$6, 40($3)
	addiu	$4, $zero, 224
	sw	$5, 44($3)
	addiu	$5, $zero, 55
	sw	$4, 48($3)
	addiu	$4, $zero, 7
	sw	$5, 52($3)
	addiu	$6, $zero, 52
	sw	$4, 56($3)
	lui	$4, %hi(key)
	addiu	$5, $zero, 43
	sw	$6, 60($3)
	addiu	$3, $4, %lo(key)
	addiu	$6, $zero, 126
	sw	$5, %lo(key)($4)
	addiu	$4, $zero, 21
	sw	$6, 4($3)
	addiu	$6, $zero, 22
	sw	$4, 8($3)
	addiu	$5, $zero, 40
	sw	$6, 12($3)
	addiu	$6, $zero, 174
	sw	$5, 16($3)
	addiu	$5, $zero, 210
	sw	$6, 20($3)
	addiu	$6, $zero, 166
	sw	$5, 24($3)
	addiu	$5, $zero, 171
	sw	$6, 28($3)
	addiu	$6, $zero, 247
	sw	$5, 32($3)
	sw	$6, 36($3)
	sw	$4, 40($3)
	addiu	$5, $zero, 9
	sw	$2, 44($3)
	addiu	$4, $zero, 207
	sw	$5, 48($3)
	addiu	$2, $zero, 79
	sw	$4, 52($3)
	addiu	$4, $zero, 60
	sw	$2, 56($3)
	sw	$4, 60($3)
	addiu	$16, $zero, 0
	jal	KeySchedule
	nop
$BB3_1:                                 # =>This Inner Loop Header: Depth=1
	lui	$2, %hi(statemt)
	lui	$3, %hi(word)
	addiu	$2, $2, %lo(statemt)
	sll	$4, $16, 4
	addiu	$3, $3, %lo(word)
	sll	$5, $16, 2
	addu	$2, $2, $4
	addu	$3, $3, $5
	lw	$4, 0($3)
	nop
	lw	$5, 0($2)
	nop
	xor	$4, $5, $4
	sw	$4, 0($2)
	lw	$4, 480($3)
	nop
	lw	$5, 4($2)
	nop
	xor	$4, $5, $4
	sw	$4, 4($2)
	lw	$4, 960($3)
	nop
	lw	$5, 8($2)
	nop
	xor	$4, $5, $4
	sw	$4, 8($2)
	lw	$3, 1440($3)
	nop
	lw	$4, 12($2)
	nop
	addiu	$16, $16, 1
	xor	$4, $4, $3
	addiu	$3, $zero, 4
	sw	$4, 12($2)
	bne	$16, $3, $BB3_1
	nop
# BB#2:                                 # %AddRoundKey.exit.i.preheader
	addiu	$16, $zero, 9
	jal	ByteSub_ShiftRow
	nop
$BB3_3:                                 # %.lr.ph24.i.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_22 Depth 2
                                        #     Child Loop BB3_4 Depth 2
	addiu	$2, $zero, 0
	sll	$3, $16, 2
	subu	$3, $2, $3
$BB3_4:                                 #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	lui	$4, %hi(statemt)
	addiu	$4, $4, %lo(statemt)
	sll	$10, $2, 2
	addu	$9, $4, $10
	lw	$4, 4($9)
	nop
	sll	$12, $4, 1
	lw	$8, 0($9)
	nop
	lui	$5, %hi(word)
	addiu	$6, $zero, -256
	xor	$14, $12, $4
	addiu	$5, $5, %lo(word)
	addu	$11, $2, $5
	sll	$24, $3, 2
	and	$13, $14, $6
	addiu	$7, $zero, 256
	lw	$5, 8($9)
	nop
	lw	$15, 12($9)
	nop
	addu	$9, $11, $24
	xor	$11, $13, $7
	lw	$25, 160($9)
	nop
	bne	$11, $zero, $BB3_6
	nop
# BB#5:                                 #   in Loop: Header=BB3_4 Depth=2
	xori	$14, $14, 283
$BB3_6:                                 #   in Loop: Header=BB3_4 Depth=2
	sll	$11, $8, 1
	and	$13, $11, $6
	xor	$13, $13, $7
	addu	$18, $zero, $11
	bne	$13, $zero, $BB3_8
	nop
# BB#7:                                 #   in Loop: Header=BB3_4 Depth=2
	xori	$18, $11, 283
$BB3_8:                                 #   in Loop: Header=BB3_4 Depth=2
	sll	$13, $5, 1
	xor	$19, $15, $5
	xor	$24, $13, $5
	addiu	$17, $sp, 20
	xor	$25, $19, $25
	xor	$18, $14, $18
	and	$14, $24, $6
	addu	$10, $17, $10
	xor	$25, $25, $18
	sw	$25, 0($10)
	xor	$14, $14, $7
	lw	$25, 640($9)
	nop
	bne	$14, $zero, $BB3_10
	nop
# BB#9:                                 #   in Loop: Header=BB3_4 Depth=2
	xori	$24, $24, 283
$BB3_10:                                #   in Loop: Header=BB3_4 Depth=2
	and	$14, $12, $6
	xor	$14, $14, $7
	bne	$14, $zero, $BB3_12
	nop
# BB#11:                                #   in Loop: Header=BB3_4 Depth=2
	xori	$12, $12, 283
$BB3_12:                                #   in Loop: Header=BB3_4 Depth=2
	sll	$14, $15, 1
	xor	$18, $8, $15
	xor	$15, $14, $15
	xor	$25, $18, $25
	xor	$24, $24, $12
	and	$12, $15, $6
	xor	$24, $25, $24
	sw	$24, 4($10)
	xor	$24, $12, $7
	lw	$12, 1120($9)
	nop
	bne	$24, $zero, $BB3_14
	nop
# BB#13:                                #   in Loop: Header=BB3_4 Depth=2
	xori	$15, $15, 283
$BB3_14:                                #   in Loop: Header=BB3_4 Depth=2
	and	$24, $13, $6
	xor	$24, $24, $7
	bne	$24, $zero, $BB3_16
	nop
# BB#15:                                #   in Loop: Header=BB3_4 Depth=2
	xori	$13, $13, 283
$BB3_16:                                #   in Loop: Header=BB3_4 Depth=2
	xor	$24, $4, $8
	xor	$8, $11, $8
	xor	$12, $24, $12
	xor	$13, $15, $13
	and	$11, $8, $6
	xor	$12, $12, $13
	sw	$12, 8($10)
	xor	$11, $11, $7
	lw	$9, 1600($9)
	nop
	bne	$11, $zero, $BB3_18
	nop
# BB#17:                                #   in Loop: Header=BB3_4 Depth=2
	xori	$8, $8, 283
$BB3_18:                                #   in Loop: Header=BB3_4 Depth=2
	and	$6, $14, $6
	xor	$6, $6, $7
	bne	$6, $zero, $BB3_20
	nop
# BB#19:                                #   in Loop: Header=BB3_4 Depth=2
	xori	$14, $14, 283
$BB3_20:                                #   in Loop: Header=BB3_4 Depth=2
	xor	$4, $5, $4
	xor	$4, $4, $9
	xor	$5, $8, $14
	addiu	$2, $2, 4
	xor	$5, $4, $5
	addiu	$4, $zero, 16
	sw	$5, 12($10)
	bne	$2, $4, $BB3_4
	nop
# BB#21:                                #   in Loop: Header=BB3_3 Depth=1
	addiu	$2, $zero, 0
$BB3_22:                                # %.lr.ph.i.i
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	lui	$5, %hi(statemt)
	sll	$3, $2, 4
	addu	$4, $17, $3
	addiu	$6, $5, %lo(statemt)
	lw	$5, 0($4)
	nop
	addu	$3, $6, $3
	sw	$5, 0($3)
	lw	$5, 4($4)
	nop
	sw	$5, 4($3)
	lw	$5, 8($4)
	nop
	sw	$5, 8($3)
	lw	$5, 12($4)
	nop
	addiu	$2, $2, 1
	addiu	$4, $zero, 4
	sw	$5, 12($3)
	bne	$2, $4, $BB3_22
	nop
# BB#23:                                # %MixColumn_AddRoundKey.exit.i
                                        #   in Loop: Header=BB3_3 Depth=1
	addiu	$16, $16, -1
	addiu	$18, $zero, 0
	jal	ByteSub_ShiftRow
	nop
	bne	$16, $zero, $BB3_3
	nop
$BB3_24:                                # %AddRoundKey.exit._crit_edge.i
                                        # =>This Inner Loop Header: Depth=1
	lui	$2, %hi(statemt)
	lui	$3, %hi(word)
	addiu	$2, $2, %lo(statemt)
	sll	$4, $18, 4
	addiu	$3, $3, %lo(word)
	sll	$5, $18, 2
	addu	$2, $2, $4
	addu	$3, $3, $5
	lw	$4, 160($3)
	nop
	lw	$5, 0($2)
	nop
	xor	$4, $5, $4
	sw	$4, 0($2)
	lw	$4, 640($3)
	nop
	lw	$5, 4($2)
	nop
	xor	$4, $5, $4
	sw	$4, 4($2)
	lw	$4, 1120($3)
	nop
	lw	$5, 8($2)
	nop
	xor	$4, $5, $4
	sw	$4, 8($2)
	lw	$3, 1600($3)
	nop
	lw	$4, 12($2)
	nop
	addiu	$18, $18, 1
	xor	$4, $4, $3
	addiu	$3, $zero, 4
	sw	$4, 12($2)
	bne	$18, $3, $BB3_24
	nop
# BB#25:                                # %.lr.ph.i.preheader
	lui	$2, %hi($.str)
	addiu	$18, $zero, 0
	addiu	$4, $2, %lo($.str)
	jal	printf
	nop
	addiu	$19, $zero, 16
$BB3_26:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	lui	$2, %hi(statemt)
	addiu	$3, $2, %lo(statemt)
	sll	$2, $18, 2
	addu	$3, $3, $2
	lw	$5, 0($3)
	nop
	addiu	$3, $zero, 15
	slt	$3, $3, $5
	bne	$3, $zero, $BB3_28
	nop
# BB#27:                                #   in Loop: Header=BB3_26 Depth=1
	lui	$3, %hi(statemt)
	lui	$4, %hi($.str1)
	addiu	$3, $3, %lo(statemt)
	addiu	$4, $4, %lo($.str1)
	addu	$16, $3, $2
	jal	printf
	nop
	lw	$5, 0($16)
	nop
$BB3_28:                                #   in Loop: Header=BB3_26 Depth=1
	lui	$2, %hi($.str2)
	addiu	$18, $18, 1
	addiu	$16, $zero, 0
	addiu	$4, $2, %lo($.str2)
	jal	printf
	nop
	bne	$18, $19, $BB3_26
	nop
# BB#29:
	addu	$2, $zero, $16
$BB3_30:                                # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	lui	$3, %hi(statemt)
	lui	$5, %hi(encrypt.out_enc_statemt)
	addiu	$4, $3, %lo(statemt)
	sll	$3, $2, 2
	addiu	$6, $5, %lo(encrypt.out_enc_statemt)
	addu	$5, $4, $3
	addu	$4, $6, $3
	lw	$3, 0($5)
	nop
	lw	$4, 0($4)
	nop
	xor	$3, $3, $4
	sltu	$3, $3, 1
	addiu	$2, $2, 1
	addu	$16, $3, $16
	addiu	$3, $zero, 16
	bne	$2, $3, $BB3_30
	nop
# BB#31:                                # %encrypt.exit
	addiu	$18, $zero, 0
	jal	KeySchedule
	nop
$BB3_32:                                # =>This Inner Loop Header: Depth=1
	lui	$2, %hi(statemt)
	lui	$3, %hi(word)
	addiu	$2, $2, %lo(statemt)
	sll	$4, $18, 4
	addiu	$3, $3, %lo(word)
	sll	$5, $18, 2
	addu	$2, $2, $4
	addu	$3, $3, $5
	lw	$4, 160($3)
	nop
	lw	$5, 0($2)
	nop
	xor	$4, $5, $4
	sw	$4, 0($2)
	lw	$4, 640($3)
	nop
	lw	$5, 4($2)
	nop
	xor	$4, $5, $4
	sw	$4, 4($2)
	lw	$4, 1120($3)
	nop
	lw	$5, 8($2)
	nop
	xor	$4, $5, $4
	sw	$4, 8($2)
	lw	$3, 1600($3)
	nop
	lw	$4, 12($2)
	nop
	addiu	$18, $18, 1
	xor	$4, $4, $3
	addiu	$3, $zero, 4
	sw	$4, 12($2)
	bne	$18, $3, $BB3_32
	nop
# BB#33:                                # %.lr.ph10.i.preheader
	addiu	$18, $zero, 0
	jal	InversShiftRow_ByteSub
	nop
	addiu	$19, $zero, 0
$BB3_34:                                # %.lr.ph24.i.i8
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_67 Depth 2
                                        #     Child Loop BB3_37 Depth 2
                                        #       Child Loop BB3_38 Depth 3
                                        #     Child Loop BB3_35 Depth 2
	sll	$3, $18, 2
	addiu	$4, $zero, 0
	addu	$5, $zero, $4
$BB3_35:                                #   Parent Loop BB3_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	lui	$6, %hi(word)
	lui	$2, %hi(statemt)
	addiu	$7, $6, %lo(word)
	addiu	$2, $2, %lo(statemt)
	sll	$6, $5, 4
	addu	$7, $7, $4
	sll	$8, $3, 2
	addu	$6, $2, $6
	addu	$7, $7, $8
	lw	$8, 144($7)
	nop
	lw	$9, 0($6)
	nop
	xor	$8, $9, $8
	sw	$8, 0($6)
	lw	$8, 624($7)
	nop
	lw	$9, 4($6)
	nop
	xor	$8, $9, $8
	sw	$8, 4($6)
	lw	$8, 1104($7)
	nop
	lw	$9, 8($6)
	nop
	xor	$8, $9, $8
	sw	$8, 8($6)
	lw	$7, 1584($7)
	nop
	lw	$8, 12($6)
	nop
	addiu	$5, $5, 1
	addiu	$4, $4, 4
	xor	$8, $8, $7
	addiu	$7, $zero, 4
	sw	$8, 12($6)
	bne	$5, $7, $BB3_35
	nop
# BB#36:                                #   in Loop: Header=BB3_34 Depth=1
	addiu	$3, $zero, 0
	addu	$4, $zero, $17
$BB3_37:                                # %.preheader17.i.i
                                        #   Parent Loop BB3_34 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_38 Depth 3
	addiu	$10, $zero, 0
$BB3_38:                                #   Parent Loop BB3_34 Depth=1
                                        #     Parent Loop BB3_37 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	addiu	$6, $zero, 3
	addiu	$5, $10, 1
	lui	$11, 16383
	xor	$6, $10, $6
	addu	$7, $zero, $5
	bne	$6, $zero, $BB3_40
	nop
# BB#39:                                #   in Loop: Header=BB3_38 Depth=3
	addiu	$7, $zero, 0
$BB3_40:                                #   in Loop: Header=BB3_38 Depth=3
	sll	$6, $10, 2
	addu	$9, $2, $6
	addu	$7, $3, $7
	lui	$8, %hi(statemt)
	lw	$12, 0($9)
	nop
	sll	$7, $7, 2
	addiu	$14, $8, %lo(statemt)
	sll	$13, $12, 1
	addiu	$8, $zero, -256
	addu	$7, $14, $7
	lw	$7, 0($7)
	nop
	and	$15, $13, $8
	addiu	$9, $zero, 256
	xor	$15, $15, $9
	bne	$15, $zero, $BB3_42
	nop
# BB#41:                                #   in Loop: Header=BB3_38 Depth=3
	xori	$13, $13, 283
$BB3_42:                                #   in Loop: Header=BB3_38 Depth=3
	addiu	$15, $10, 2
	sra	$24, $15, 31
	addiu	$10, $10, 3
	srl	$25, $24, 30
	sra	$24, $10, 31
	srl	$24, $24, 30
	addu	$25, $15, $25
	ori	$11, $11, 65532
	and	$25, $25, $11
	addu	$24, $10, $24
	subu	$15, $15, $25
	and	$11, $24, $11
	addu	$15, $3, $15
	subu	$11, $10, $11
	sll	$10, $15, 2
	addu	$11, $3, $11
	sll	$15, $7, 1
	addu	$10, $14, $10
	sll	$11, $11, 2
	and	$24, $15, $8
	lw	$10, 0($10)
	nop
	addu	$11, $14, $11
	lw	$11, 0($11)
	nop
	xor	$14, $24, $9
	bne	$14, $zero, $BB3_44
	nop
# BB#43:                                #   in Loop: Header=BB3_38 Depth=3
	xori	$15, $15, 283
$BB3_44:                                #   in Loop: Header=BB3_38 Depth=3
	sll	$14, $10, 1
	and	$24, $14, $8
	xor	$24, $24, $9
	bne	$24, $zero, $BB3_46
	nop
# BB#45:                                #   in Loop: Header=BB3_38 Depth=3
	xori	$14, $14, 283
$BB3_46:                                #   in Loop: Header=BB3_38 Depth=3
	sll	$25, $11, 1
	and	$24, $25, $8
	xor	$24, $24, $9
	bne	$24, $zero, $BB3_48
	nop
# BB#47:                                #   in Loop: Header=BB3_38 Depth=3
	xori	$25, $25, 283
$BB3_48:                                #   in Loop: Header=BB3_38 Depth=3
	xor	$13, $13, $12
	sll	$24, $13, 1
	and	$13, $24, $8
	xor	$13, $13, $9
	bne	$13, $zero, $BB3_50
	nop
# BB#49:                                #   in Loop: Header=BB3_38 Depth=3
	xori	$24, $24, 283
$BB3_50:                                #   in Loop: Header=BB3_38 Depth=3
	sll	$15, $15, 1
	and	$13, $15, $8
	xor	$13, $13, $9
	bne	$13, $zero, $BB3_52
	nop
# BB#51:                                #   in Loop: Header=BB3_38 Depth=3
	xori	$15, $15, 283
$BB3_52:                                #   in Loop: Header=BB3_38 Depth=3
	sll	$13, $25, 1
	and	$25, $13, $8
	xor	$25, $25, $9
	bne	$25, $zero, $BB3_54
	nop
# BB#53:                                #   in Loop: Header=BB3_38 Depth=3
	xori	$13, $13, 283
$BB3_54:                                #   in Loop: Header=BB3_38 Depth=3
	xor	$14, $14, $10
	sll	$14, $14, 1
	and	$25, $14, $8
	xor	$25, $25, $9
	bne	$25, $zero, $BB3_56
	nop
# BB#55:                                #   in Loop: Header=BB3_38 Depth=3
	xori	$14, $14, 283
$BB3_56:                                #   in Loop: Header=BB3_38 Depth=3
	xor	$12, $24, $12
	sll	$12, $12, 1
	and	$24, $12, $8
	xor	$24, $24, $9
	bne	$24, $zero, $BB3_58
	nop
# BB#57:                                #   in Loop: Header=BB3_38 Depth=3
	xori	$12, $12, 283
$BB3_58:                                #   in Loop: Header=BB3_38 Depth=3
	xor	$15, $15, $7
	sll	$15, $15, 1
	and	$24, $15, $8
	xor	$24, $24, $9
	bne	$24, $zero, $BB3_60
	nop
# BB#59:                                #   in Loop: Header=BB3_38 Depth=3
	xori	$15, $15, 283
$BB3_60:                                #   in Loop: Header=BB3_38 Depth=3
	sll	$13, $13, 1
	and	$24, $13, $8
	xor	$24, $24, $9
	bne	$24, $zero, $BB3_62
	nop
# BB#61:                                #   in Loop: Header=BB3_38 Depth=3
	xori	$13, $13, 283
$BB3_62:                                #   in Loop: Header=BB3_38 Depth=3
	sll	$14, $14, 1
	and	$8, $14, $8
	xor	$8, $8, $9
	bne	$8, $zero, $BB3_64
	nop
# BB#63:                                #   in Loop: Header=BB3_38 Depth=3
	xori	$14, $14, 283
$BB3_64:                                #   in Loop: Header=BB3_38 Depth=3
	xor	$7, $12, $7
	xor	$7, $7, $15
	xor	$8, $7, $10
	xor	$7, $13, $11
	xor	$8, $8, $14
	xor	$7, $7, $8
	addu	$8, $4, $6
	addiu	$6, $zero, 4
	sw	$7, 0($8)
	addu	$10, $zero, $5
	bne	$5, $6, $BB3_38
	nop
# BB#65:                                #   in Loop: Header=BB3_37 Depth=2
	addiu	$3, $3, 4
	addiu	$4, $4, 16
	addiu	$2, $2, 16
	addiu	$5, $zero, 16
	bne	$3, $5, $BB3_37
	nop
# BB#66:                                #   in Loop: Header=BB3_34 Depth=1
	addiu	$2, $zero, 0
$BB3_67:                                # %.lr.ph.i.i26
                                        #   Parent Loop BB3_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	lui	$5, %hi(statemt)
	addiu	$4, $sp, 20
	sll	$3, $2, 4
	addu	$4, $4, $3
	addiu	$6, $5, %lo(statemt)
	lw	$5, 0($4)
	nop
	addu	$3, $6, $3
	sw	$5, 0($3)
	lw	$5, 4($4)
	nop
	sw	$5, 4($3)
	lw	$5, 8($4)
	nop
	sw	$5, 8($3)
	lw	$5, 12($4)
	nop
	addiu	$2, $2, 1
	addiu	$4, $zero, 4
	sw	$5, 12($3)
	bne	$2, $4, $BB3_67
	nop
# BB#68:                                # %AddRoundKey_InversMixColumn.exit.i
                                        #   in Loop: Header=BB3_34 Depth=1
	addiu	$20, $18, -1
	addiu	$21, $18, 8
	jal	InversShiftRow_ByteSub
	nop
	addu	$18, $zero, $20
	bgtz	$21, $BB3_34
	nop
$BB3_69:                                # %._crit_edge.i
                                        # =>This Inner Loop Header: Depth=1
	lui	$2, %hi(statemt)
	lui	$3, %hi(word)
	addiu	$2, $2, %lo(statemt)
	sll	$4, $19, 4
	addiu	$3, $3, %lo(word)
	sll	$5, $19, 2
	addu	$2, $2, $4
	addu	$3, $3, $5
	lw	$4, 0($3)
	nop
	lw	$5, 0($2)
	nop
	xor	$4, $5, $4
	sw	$4, 0($2)
	lw	$4, 480($3)
	nop
	lw	$5, 4($2)
	nop
	xor	$4, $5, $4
	sw	$4, 4($2)
	lw	$4, 960($3)
	nop
	lw	$5, 8($2)
	nop
	xor	$4, $5, $4
	sw	$4, 8($2)
	lw	$3, 1440($3)
	nop
	lw	$4, 12($2)
	nop
	addiu	$19, $19, 1
	xor	$4, $4, $3
	addiu	$3, $zero, 4
	sw	$4, 12($2)
	bne	$19, $3, $BB3_69
	nop
# BB#70:                                # %AddRoundKey.exit3.i30
	lui	$2, %hi($.str3)
	addiu	$17, $zero, 0
	addiu	$4, $2, %lo($.str3)
	jal	printf
	nop
	addiu	$19, $zero, 16
$BB3_71:                                # %.lr.ph.i35
                                        # =>This Inner Loop Header: Depth=1
	lui	$2, %hi(statemt)
	addiu	$3, $2, %lo(statemt)
	sll	$2, $17, 2
	addu	$3, $3, $2
	lw	$5, 0($3)
	nop
	addiu	$3, $zero, 15
	slt	$3, $3, $5
	bne	$3, $zero, $BB3_73
	nop
# BB#72:                                #   in Loop: Header=BB3_71 Depth=1
	lui	$3, %hi(statemt)
	lui	$4, %hi($.str1)
	addiu	$3, $3, %lo(statemt)
	addiu	$4, $4, %lo($.str1)
	addu	$18, $3, $2
	jal	printf
	nop
	lw	$5, 0($18)
	nop
$BB3_73:                                #   in Loop: Header=BB3_71 Depth=1
	lui	$2, %hi($.str2)
	addiu	$17, $17, 1
	addiu	$18, $zero, 0
	addiu	$4, $2, %lo($.str2)
	jal	printf
	nop
	bne	$17, $19, $BB3_71
	nop
$BB3_74:                                # %.preheader.loopexit.i32
                                        # =>This Inner Loop Header: Depth=1
	lui	$2, %hi(statemt)
	lui	$4, %hi(decrypt.out_dec_statemt)
	addiu	$3, $2, %lo(statemt)
	sll	$2, $18, 2
	addiu	$5, $4, %lo(decrypt.out_dec_statemt)
	addu	$4, $3, $2
	addu	$3, $5, $2
	lw	$2, 0($4)
	nop
	lw	$3, 0($3)
	nop
	xor	$2, $2, $3
	sltu	$2, $2, 1
	addiu	$18, $18, 1
	addu	$16, $2, $16
	addiu	$2, $zero, 16
	bne	$18, $2, $BB3_74
	nop
# BB#75:                                # %decrypt.exit
	lui	$2, %hi($.str4)
	addiu	$4, $2, %lo($.str4)
	addiu	$17, $zero, 32
	addu	$5, $zero, $16
	jal	printf
	nop
	bne	$16, $17, $BB3_77
	nop
# BB#76:
	lui	$2, %hi($.str5)
	addiu	$4, $2, %lo($.str5)
	j	$BB3_78
	nop
$BB3_77:
	lui	$2, %hi($.str6)
	addiu	$4, $2, %lo($.str6)
$BB3_78:
	jal	printf
	nop
	addu	$2, $zero, $16
	lw	$16, 148($sp)
	nop
	lw	$17, 152($sp)
	nop
	lw	$18, 156($sp)
	nop
	lw	$19, 160($sp)
	nop
	lw	$20, 164($sp)
	nop
	lw	$21, 168($sp)
	nop
	lw	$ra, 172($sp)
	nop
	addiu	$sp, $sp, 176
	jr	$ra
	nop
	.set	macro
	.set	reorder
	.end	main
$tmp26:
	.size	main, ($tmp26)-main
$tmp27:
	.cfi_endproc
$eh_func_end3:

	.type	encrypt.out_enc_statemt,@object # @encrypt.out_enc_statemt
	.section	.rodata,"a",@progbits
	.align	2
encrypt.out_enc_statemt:
	.4byte	57                      # 0x39
	.4byte	37                      # 0x25
	.4byte	132                     # 0x84
	.4byte	29                      # 0x1d
	.4byte	2                       # 0x2
	.4byte	220                     # 0xdc
	.4byte	9                       # 0x9
	.4byte	251                     # 0xfb
	.4byte	220                     # 0xdc
	.4byte	17                      # 0x11
	.4byte	133                     # 0x85
	.4byte	151                     # 0x97
	.4byte	25                      # 0x19
	.4byte	106                     # 0x6a
	.4byte	11                      # 0xb
	.4byte	50                      # 0x32
	.size	encrypt.out_enc_statemt, 64

	.type	$.str,@object           # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
$.str:
	.asciz	 "encrypted message \t"
	.size	$.str, 20

	.type	$.str1,@object          # @.str1
$.str1:
	.asciz	 "0"
	.size	$.str1, 2

	.type	$.str2,@object          # @.str2
$.str2:
	.asciz	 "%x"
	.size	$.str2, 3

	.type	decrypt.out_dec_statemt,@object # @decrypt.out_dec_statemt
	.section	.rodata,"a",@progbits
	.align	2
decrypt.out_dec_statemt:
	.4byte	50                      # 0x32
	.4byte	67                      # 0x43
	.4byte	246                     # 0xf6
	.4byte	168                     # 0xa8
	.4byte	136                     # 0x88
	.4byte	90                      # 0x5a
	.4byte	48                      # 0x30
	.4byte	141                     # 0x8d
	.4byte	49                      # 0x31
	.4byte	49                      # 0x31
	.4byte	152                     # 0x98
	.4byte	162                     # 0xa2
	.4byte	224                     # 0xe0
	.4byte	55                      # 0x37
	.4byte	7                       # 0x7
	.4byte	52                      # 0x34
	.size	decrypt.out_dec_statemt, 64

	.type	$.str3,@object          # @.str3
	.section	.rodata.str1.1,"aMS",@progbits,1
$.str3:
	.asciz	 "\ndecrypto message\t"
	.size	$.str3, 19

	.type	Rcon0,@object           # @Rcon0
	.section	.rodata,"a",@progbits
	.align	2
Rcon0:
	.4byte	1                       # 0x1
	.4byte	2                       # 0x2
	.4byte	4                       # 0x4
	.4byte	8                       # 0x8
	.4byte	16                      # 0x10
	.4byte	32                      # 0x20
	.4byte	64                      # 0x40
	.4byte	128                     # 0x80
	.4byte	27                      # 0x1b
	.4byte	54                      # 0x36
	.4byte	108                     # 0x6c
	.4byte	216                     # 0xd8
	.4byte	171                     # 0xab
	.4byte	77                      # 0x4d
	.4byte	154                     # 0x9a
	.4byte	47                      # 0x2f
	.4byte	94                      # 0x5e
	.4byte	188                     # 0xbc
	.4byte	99                      # 0x63
	.4byte	198                     # 0xc6
	.4byte	151                     # 0x97
	.4byte	53                      # 0x35
	.4byte	106                     # 0x6a
	.4byte	212                     # 0xd4
	.4byte	179                     # 0xb3
	.4byte	125                     # 0x7d
	.4byte	250                     # 0xfa
	.4byte	239                     # 0xef
	.4byte	197                     # 0xc5
	.4byte	145                     # 0x91
	.size	Rcon0, 120

	.type	word,@object            # @word
	.local	word
	.comm	word,1920,4
	.type	Sbox,@object            # @Sbox
	.align	2
Sbox:
	.4byte	99                      # 0x63
	.4byte	124                     # 0x7c
	.4byte	119                     # 0x77
	.4byte	123                     # 0x7b
	.4byte	242                     # 0xf2
	.4byte	107                     # 0x6b
	.4byte	111                     # 0x6f
	.4byte	197                     # 0xc5
	.4byte	48                      # 0x30
	.4byte	1                       # 0x1
	.4byte	103                     # 0x67
	.4byte	43                      # 0x2b
	.4byte	254                     # 0xfe
	.4byte	215                     # 0xd7
	.4byte	171                     # 0xab
	.4byte	118                     # 0x76
	.4byte	202                     # 0xca
	.4byte	130                     # 0x82
	.4byte	201                     # 0xc9
	.4byte	125                     # 0x7d
	.4byte	250                     # 0xfa
	.4byte	89                      # 0x59
	.4byte	71                      # 0x47
	.4byte	240                     # 0xf0
	.4byte	173                     # 0xad
	.4byte	212                     # 0xd4
	.4byte	162                     # 0xa2
	.4byte	175                     # 0xaf
	.4byte	156                     # 0x9c
	.4byte	164                     # 0xa4
	.4byte	114                     # 0x72
	.4byte	192                     # 0xc0
	.4byte	183                     # 0xb7
	.4byte	253                     # 0xfd
	.4byte	147                     # 0x93
	.4byte	38                      # 0x26
	.4byte	54                      # 0x36
	.4byte	63                      # 0x3f
	.4byte	247                     # 0xf7
	.4byte	204                     # 0xcc
	.4byte	52                      # 0x34
	.4byte	165                     # 0xa5
	.4byte	229                     # 0xe5
	.4byte	241                     # 0xf1
	.4byte	113                     # 0x71
	.4byte	216                     # 0xd8
	.4byte	49                      # 0x31
	.4byte	21                      # 0x15
	.4byte	4                       # 0x4
	.4byte	199                     # 0xc7
	.4byte	35                      # 0x23
	.4byte	195                     # 0xc3
	.4byte	24                      # 0x18
	.4byte	150                     # 0x96
	.4byte	5                       # 0x5
	.4byte	154                     # 0x9a
	.4byte	7                       # 0x7
	.4byte	18                      # 0x12
	.4byte	128                     # 0x80
	.4byte	226                     # 0xe2
	.4byte	235                     # 0xeb
	.4byte	39                      # 0x27
	.4byte	178                     # 0xb2
	.4byte	117                     # 0x75
	.4byte	9                       # 0x9
	.4byte	131                     # 0x83
	.4byte	44                      # 0x2c
	.4byte	26                      # 0x1a
	.4byte	27                      # 0x1b
	.4byte	110                     # 0x6e
	.4byte	90                      # 0x5a
	.4byte	160                     # 0xa0
	.4byte	82                      # 0x52
	.4byte	59                      # 0x3b
	.4byte	214                     # 0xd6
	.4byte	179                     # 0xb3
	.4byte	41                      # 0x29
	.4byte	227                     # 0xe3
	.4byte	47                      # 0x2f
	.4byte	132                     # 0x84
	.4byte	83                      # 0x53
	.4byte	209                     # 0xd1
	.4byte	0                       # 0x0
	.4byte	237                     # 0xed
	.4byte	32                      # 0x20
	.4byte	252                     # 0xfc
	.4byte	177                     # 0xb1
	.4byte	91                      # 0x5b
	.4byte	106                     # 0x6a
	.4byte	203                     # 0xcb
	.4byte	190                     # 0xbe
	.4byte	57                      # 0x39
	.4byte	74                      # 0x4a
	.4byte	76                      # 0x4c
	.4byte	88                      # 0x58
	.4byte	207                     # 0xcf
	.4byte	208                     # 0xd0
	.4byte	239                     # 0xef
	.4byte	170                     # 0xaa
	.4byte	251                     # 0xfb
	.4byte	67                      # 0x43
	.4byte	77                      # 0x4d
	.4byte	51                      # 0x33
	.4byte	133                     # 0x85
	.4byte	69                      # 0x45
	.4byte	249                     # 0xf9
	.4byte	2                       # 0x2
	.4byte	127                     # 0x7f
	.4byte	80                      # 0x50
	.4byte	60                      # 0x3c
	.4byte	159                     # 0x9f
	.4byte	168                     # 0xa8
	.4byte	81                      # 0x51
	.4byte	163                     # 0xa3
	.4byte	64                      # 0x40
	.4byte	143                     # 0x8f
	.4byte	146                     # 0x92
	.4byte	157                     # 0x9d
	.4byte	56                      # 0x38
	.4byte	245                     # 0xf5
	.4byte	188                     # 0xbc
	.4byte	182                     # 0xb6
	.4byte	218                     # 0xda
	.4byte	33                      # 0x21
	.4byte	16                      # 0x10
	.4byte	255                     # 0xff
	.4byte	243                     # 0xf3
	.4byte	210                     # 0xd2
	.4byte	205                     # 0xcd
	.4byte	12                      # 0xc
	.4byte	19                      # 0x13
	.4byte	236                     # 0xec
	.4byte	95                      # 0x5f
	.4byte	151                     # 0x97
	.4byte	68                      # 0x44
	.4byte	23                      # 0x17
	.4byte	196                     # 0xc4
	.4byte	167                     # 0xa7
	.4byte	126                     # 0x7e
	.4byte	61                      # 0x3d
	.4byte	100                     # 0x64
	.4byte	93                      # 0x5d
	.4byte	25                      # 0x19
	.4byte	115                     # 0x73
	.4byte	96                      # 0x60
	.4byte	129                     # 0x81
	.4byte	79                      # 0x4f
	.4byte	220                     # 0xdc
	.4byte	34                      # 0x22
	.4byte	42                      # 0x2a
	.4byte	144                     # 0x90
	.4byte	136                     # 0x88
	.4byte	70                      # 0x46
	.4byte	238                     # 0xee
	.4byte	184                     # 0xb8
	.4byte	20                      # 0x14
	.4byte	222                     # 0xde
	.4byte	94                      # 0x5e
	.4byte	11                      # 0xb
	.4byte	219                     # 0xdb
	.4byte	224                     # 0xe0
	.4byte	50                      # 0x32
	.4byte	58                      # 0x3a
	.4byte	10                      # 0xa
	.4byte	73                      # 0x49
	.4byte	6                       # 0x6
	.4byte	36                      # 0x24
	.4byte	92                      # 0x5c
	.4byte	194                     # 0xc2
	.4byte	211                     # 0xd3
	.4byte	172                     # 0xac
	.4byte	98                      # 0x62
	.4byte	145                     # 0x91
	.4byte	149                     # 0x95
	.4byte	228                     # 0xe4
	.4byte	121                     # 0x79
	.4byte	231                     # 0xe7
	.4byte	200                     # 0xc8
	.4byte	55                      # 0x37
	.4byte	109                     # 0x6d
	.4byte	141                     # 0x8d
	.4byte	213                     # 0xd5
	.4byte	78                      # 0x4e
	.4byte	169                     # 0xa9
	.4byte	108                     # 0x6c
	.4byte	86                      # 0x56
	.4byte	244                     # 0xf4
	.4byte	234                     # 0xea
	.4byte	101                     # 0x65
	.4byte	122                     # 0x7a
	.4byte	174                     # 0xae
	.4byte	8                       # 0x8
	.4byte	186                     # 0xba
	.4byte	120                     # 0x78
	.4byte	37                      # 0x25
	.4byte	46                      # 0x2e
	.4byte	28                      # 0x1c
	.4byte	166                     # 0xa6
	.4byte	180                     # 0xb4
	.4byte	198                     # 0xc6
	.4byte	232                     # 0xe8
	.4byte	221                     # 0xdd
	.4byte	116                     # 0x74
	.4byte	31                      # 0x1f
	.4byte	75                      # 0x4b
	.4byte	189                     # 0xbd
	.4byte	139                     # 0x8b
	.4byte	138                     # 0x8a
	.4byte	112                     # 0x70
	.4byte	62                      # 0x3e
	.4byte	181                     # 0xb5
	.4byte	102                     # 0x66
	.4byte	72                      # 0x48
	.4byte	3                       # 0x3
	.4byte	246                     # 0xf6
	.4byte	14                      # 0xe
	.4byte	97                      # 0x61
	.4byte	53                      # 0x35
	.4byte	87                      # 0x57
	.4byte	185                     # 0xb9
	.4byte	134                     # 0x86
	.4byte	193                     # 0xc1
	.4byte	29                      # 0x1d
	.4byte	158                     # 0x9e
	.4byte	225                     # 0xe1
	.4byte	248                     # 0xf8
	.4byte	152                     # 0x98
	.4byte	17                      # 0x11
	.4byte	105                     # 0x69
	.4byte	217                     # 0xd9
	.4byte	142                     # 0x8e
	.4byte	148                     # 0x94
	.4byte	155                     # 0x9b
	.4byte	30                      # 0x1e
	.4byte	135                     # 0x87
	.4byte	233                     # 0xe9
	.4byte	206                     # 0xce
	.4byte	85                      # 0x55
	.4byte	40                      # 0x28
	.4byte	223                     # 0xdf
	.4byte	140                     # 0x8c
	.4byte	161                     # 0xa1
	.4byte	137                     # 0x89
	.4byte	13                      # 0xd
	.4byte	191                     # 0xbf
	.4byte	230                     # 0xe6
	.4byte	66                      # 0x42
	.4byte	104                     # 0x68
	.4byte	65                      # 0x41
	.4byte	153                     # 0x99
	.4byte	45                      # 0x2d
	.4byte	15                      # 0xf
	.4byte	176                     # 0xb0
	.4byte	84                      # 0x54
	.4byte	187                     # 0xbb
	.4byte	22                      # 0x16
	.size	Sbox, 1024

	.type	invSbox,@object         # @invSbox
	.align	2
invSbox:
	.4byte	82                      # 0x52
	.4byte	9                       # 0x9
	.4byte	106                     # 0x6a
	.4byte	213                     # 0xd5
	.4byte	48                      # 0x30
	.4byte	54                      # 0x36
	.4byte	165                     # 0xa5
	.4byte	56                      # 0x38
	.4byte	191                     # 0xbf
	.4byte	64                      # 0x40
	.4byte	163                     # 0xa3
	.4byte	158                     # 0x9e
	.4byte	129                     # 0x81
	.4byte	243                     # 0xf3
	.4byte	215                     # 0xd7
	.4byte	251                     # 0xfb
	.4byte	124                     # 0x7c
	.4byte	227                     # 0xe3
	.4byte	57                      # 0x39
	.4byte	130                     # 0x82
	.4byte	155                     # 0x9b
	.4byte	47                      # 0x2f
	.4byte	255                     # 0xff
	.4byte	135                     # 0x87
	.4byte	52                      # 0x34
	.4byte	142                     # 0x8e
	.4byte	67                      # 0x43
	.4byte	68                      # 0x44
	.4byte	196                     # 0xc4
	.4byte	222                     # 0xde
	.4byte	233                     # 0xe9
	.4byte	203                     # 0xcb
	.4byte	84                      # 0x54
	.4byte	123                     # 0x7b
	.4byte	148                     # 0x94
	.4byte	50                      # 0x32
	.4byte	166                     # 0xa6
	.4byte	194                     # 0xc2
	.4byte	35                      # 0x23
	.4byte	61                      # 0x3d
	.4byte	238                     # 0xee
	.4byte	76                      # 0x4c
	.4byte	149                     # 0x95
	.4byte	11                      # 0xb
	.4byte	66                      # 0x42
	.4byte	250                     # 0xfa
	.4byte	195                     # 0xc3
	.4byte	78                      # 0x4e
	.4byte	8                       # 0x8
	.4byte	46                      # 0x2e
	.4byte	161                     # 0xa1
	.4byte	102                     # 0x66
	.4byte	40                      # 0x28
	.4byte	217                     # 0xd9
	.4byte	36                      # 0x24
	.4byte	178                     # 0xb2
	.4byte	118                     # 0x76
	.4byte	91                      # 0x5b
	.4byte	162                     # 0xa2
	.4byte	73                      # 0x49
	.4byte	109                     # 0x6d
	.4byte	139                     # 0x8b
	.4byte	209                     # 0xd1
	.4byte	37                      # 0x25
	.4byte	114                     # 0x72
	.4byte	248                     # 0xf8
	.4byte	246                     # 0xf6
	.4byte	100                     # 0x64
	.4byte	134                     # 0x86
	.4byte	104                     # 0x68
	.4byte	152                     # 0x98
	.4byte	22                      # 0x16
	.4byte	212                     # 0xd4
	.4byte	164                     # 0xa4
	.4byte	92                      # 0x5c
	.4byte	204                     # 0xcc
	.4byte	93                      # 0x5d
	.4byte	101                     # 0x65
	.4byte	182                     # 0xb6
	.4byte	146                     # 0x92
	.4byte	108                     # 0x6c
	.4byte	112                     # 0x70
	.4byte	72                      # 0x48
	.4byte	80                      # 0x50
	.4byte	253                     # 0xfd
	.4byte	237                     # 0xed
	.4byte	185                     # 0xb9
	.4byte	218                     # 0xda
	.4byte	94                      # 0x5e
	.4byte	21                      # 0x15
	.4byte	70                      # 0x46
	.4byte	87                      # 0x57
	.4byte	167                     # 0xa7
	.4byte	141                     # 0x8d
	.4byte	157                     # 0x9d
	.4byte	132                     # 0x84
	.4byte	144                     # 0x90
	.4byte	216                     # 0xd8
	.4byte	171                     # 0xab
	.4byte	0                       # 0x0
	.4byte	140                     # 0x8c
	.4byte	188                     # 0xbc
	.4byte	211                     # 0xd3
	.4byte	10                      # 0xa
	.4byte	247                     # 0xf7
	.4byte	228                     # 0xe4
	.4byte	88                      # 0x58
	.4byte	5                       # 0x5
	.4byte	184                     # 0xb8
	.4byte	179                     # 0xb3
	.4byte	69                      # 0x45
	.4byte	6                       # 0x6
	.4byte	208                     # 0xd0
	.4byte	44                      # 0x2c
	.4byte	30                      # 0x1e
	.4byte	143                     # 0x8f
	.4byte	202                     # 0xca
	.4byte	63                      # 0x3f
	.4byte	15                      # 0xf
	.4byte	2                       # 0x2
	.4byte	193                     # 0xc1
	.4byte	175                     # 0xaf
	.4byte	189                     # 0xbd
	.4byte	3                       # 0x3
	.4byte	1                       # 0x1
	.4byte	19                      # 0x13
	.4byte	138                     # 0x8a
	.4byte	107                     # 0x6b
	.4byte	58                      # 0x3a
	.4byte	145                     # 0x91
	.4byte	17                      # 0x11
	.4byte	65                      # 0x41
	.4byte	79                      # 0x4f
	.4byte	103                     # 0x67
	.4byte	220                     # 0xdc
	.4byte	234                     # 0xea
	.4byte	151                     # 0x97
	.4byte	242                     # 0xf2
	.4byte	207                     # 0xcf
	.4byte	206                     # 0xce
	.4byte	240                     # 0xf0
	.4byte	180                     # 0xb4
	.4byte	230                     # 0xe6
	.4byte	115                     # 0x73
	.4byte	150                     # 0x96
	.4byte	172                     # 0xac
	.4byte	116                     # 0x74
	.4byte	34                      # 0x22
	.4byte	231                     # 0xe7
	.4byte	173                     # 0xad
	.4byte	53                      # 0x35
	.4byte	133                     # 0x85
	.4byte	226                     # 0xe2
	.4byte	249                     # 0xf9
	.4byte	55                      # 0x37
	.4byte	232                     # 0xe8
	.4byte	28                      # 0x1c
	.4byte	117                     # 0x75
	.4byte	223                     # 0xdf
	.4byte	110                     # 0x6e
	.4byte	71                      # 0x47
	.4byte	241                     # 0xf1
	.4byte	26                      # 0x1a
	.4byte	113                     # 0x71
	.4byte	29                      # 0x1d
	.4byte	41                      # 0x29
	.4byte	197                     # 0xc5
	.4byte	137                     # 0x89
	.4byte	111                     # 0x6f
	.4byte	183                     # 0xb7
	.4byte	98                      # 0x62
	.4byte	14                      # 0xe
	.4byte	170                     # 0xaa
	.4byte	24                      # 0x18
	.4byte	190                     # 0xbe
	.4byte	27                      # 0x1b
	.4byte	252                     # 0xfc
	.4byte	86                      # 0x56
	.4byte	62                      # 0x3e
	.4byte	75                      # 0x4b
	.4byte	198                     # 0xc6
	.4byte	210                     # 0xd2
	.4byte	121                     # 0x79
	.4byte	32                      # 0x20
	.4byte	154                     # 0x9a
	.4byte	219                     # 0xdb
	.4byte	192                     # 0xc0
	.4byte	254                     # 0xfe
	.4byte	120                     # 0x78
	.4byte	205                     # 0xcd
	.4byte	90                      # 0x5a
	.4byte	244                     # 0xf4
	.4byte	31                      # 0x1f
	.4byte	221                     # 0xdd
	.4byte	168                     # 0xa8
	.4byte	51                      # 0x33
	.4byte	136                     # 0x88
	.4byte	7                       # 0x7
	.4byte	199                     # 0xc7
	.4byte	49                      # 0x31
	.4byte	177                     # 0xb1
	.4byte	18                      # 0x12
	.4byte	16                      # 0x10
	.4byte	89                      # 0x59
	.4byte	39                      # 0x27
	.4byte	128                     # 0x80
	.4byte	236                     # 0xec
	.4byte	95                      # 0x5f
	.4byte	96                      # 0x60
	.4byte	81                      # 0x51
	.4byte	127                     # 0x7f
	.4byte	169                     # 0xa9
	.4byte	25                      # 0x19
	.4byte	181                     # 0xb5
	.4byte	74                      # 0x4a
	.4byte	13                      # 0xd
	.4byte	45                      # 0x2d
	.4byte	229                     # 0xe5
	.4byte	122                     # 0x7a
	.4byte	159                     # 0x9f
	.4byte	147                     # 0x93
	.4byte	201                     # 0xc9
	.4byte	156                     # 0x9c
	.4byte	239                     # 0xef
	.4byte	160                     # 0xa0
	.4byte	224                     # 0xe0
	.4byte	59                      # 0x3b
	.4byte	77                      # 0x4d
	.4byte	174                     # 0xae
	.4byte	42                      # 0x2a
	.4byte	245                     # 0xf5
	.4byte	176                     # 0xb0
	.4byte	200                     # 0xc8
	.4byte	235                     # 0xeb
	.4byte	187                     # 0xbb
	.4byte	60                      # 0x3c
	.4byte	131                     # 0x83
	.4byte	83                      # 0x53
	.4byte	153                     # 0x99
	.4byte	97                      # 0x61
	.4byte	23                      # 0x17
	.4byte	43                      # 0x2b
	.4byte	4                       # 0x4
	.4byte	126                     # 0x7e
	.4byte	186                     # 0xba
	.4byte	119                     # 0x77
	.4byte	214                     # 0xd6
	.4byte	38                      # 0x26
	.4byte	225                     # 0xe1
	.4byte	105                     # 0x69
	.4byte	20                      # 0x14
	.4byte	99                      # 0x63
	.4byte	85                      # 0x55
	.4byte	33                      # 0x21
	.4byte	12                      # 0xc
	.4byte	125                     # 0x7d
	.size	invSbox, 1024

	.type	statemt,@object         # @statemt
	.local	statemt
	.comm	statemt,128,4
	.type	key,@object             # @key
	.local	key
	.comm	key,128,4
	.type	$.str4,@object          # @.str4
	.section	.rodata.str1.1,"aMS",@progbits,1
$.str4:
	.asciz	 "\nResult: %d\n"
	.size	$.str4, 13

	.type	$.str5,@object          # @.str5
$.str5:
	.asciz	 "RESULT: PASS\n"
	.size	$.str5, 14

	.type	$.str6,@object          # @.str6
$.str6:
	.asciz	 "RESULT: FAIL\n"
	.size	$.str6, 14


