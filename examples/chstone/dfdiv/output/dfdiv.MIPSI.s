	.section .mdebug.abi32
	.previous
	.file	"output/dfdiv.sw.ll"
	.section	_main_section,"ax",@progbits
	.globl	main
	.align	2
	.type	main,@function
	.ent	main                    # @main
main:
$tmp2:
	.cfi_startproc
	.frame	$sp,120,$ra
	.mask 	0x80FF0000,-4
	.fmask	0x00000000,0
# BB#0:
	.set	noreorder
	.set	nomacro
	addiu	$sp, $sp, -120
$tmp3:
	.cfi_def_cfa_offset 120
	sw	$ra, 116($sp)
	sw	$23, 112($sp)
	sw	$22, 108($sp)
	sw	$21, 104($sp)
	sw	$20, 100($sp)
	sw	$19, 96($sp)
	sw	$18, 92($sp)
	sw	$17, 88($sp)
	sw	$16, 84($sp)
$tmp4:
	.cfi_offset 31, -4
$tmp5:
	.cfi_offset 23, -8
$tmp6:
	.cfi_offset 22, -12
$tmp7:
	.cfi_offset 21, -16
$tmp8:
	.cfi_offset 20, -20
$tmp9:
	.cfi_offset 19, -24
$tmp10:
	.cfi_offset 18, -28
$tmp11:
	.cfi_offset 17, -32
$tmp12:
	.cfi_offset 16, -36
	addiu	$4, $zero, 0
	addu	$18, $zero, $4
	sw	$4, 52($sp)
$BB0_1:                                 # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_106 Depth 2
                                        #     Child Loop BB0_118 Depth 2
	sw	$4, 48($sp)
	lui	$2, %hi(b_input)
	lui	$3, %hi(a_input)
	addiu	$2, $2, %lo(b_input)
	sll	$4, $4, 3
	sw	$4, 44($sp)
	addiu	$3, $3, %lo(a_input)
	subu	$2, $2, $4
	subu	$4, $3, $4
	lw	$23, 4($2)
	nop
	lw	$20, 4($4)
	nop
	lui	$3, 15
	ori	$3, $3, 65535
	srl	$7, $20, 20
	srl	$6, $23, 20
	xor	$5, $23, $20
	lw	$22, 0($4)
	nop
	lw	$8, 0($2)
	nop
	andi	$21, $7, 2047
	andi	$19, $6, 2047
	and	$17, $20, $3
	and	$16, $23, $3
	srl	$3, $5, 31
	addiu	$2, $zero, 2047
	bne	$21, $2, $BB0_44
	nop
# BB#2:                                 #   in Loop: Header=BB0_1 Depth=1
	or	$2, $22, $17
	beq	$2, $zero, $BB0_22
	nop
# BB#3:                                 #   in Loop: Header=BB0_1 Depth=1
	addu	$7, $zero, $8
	lui	$3, 32760
	lui	$2, 32752
	ori	$3, $3, 0
	and	$3, $20, $3
	ori	$2, $2, 0
	xor	$2, $3, $2
	beq	$2, $zero, $BB0_5
	nop
# BB#4:                                 #   in Loop: Header=BB0_1 Depth=1
	addiu	$2, $zero, 0
	j	$BB0_6
	nop
$BB0_5:                                 #   in Loop: Header=BB0_1 Depth=1
	lui	$2, 7
	ori	$2, $2, 65535
	and	$2, $20, $2
	or	$2, $22, $2
	addiu	$3, $zero, 0
	xor	$2, $2, $3
	sltu	$2, $zero, $2
$BB0_6:                                 # %float64_is_signaling_nan.exit1.i19.i
                                        #   in Loop: Header=BB0_1 Depth=1
	lui	$4, 32760
	lui	$3, 32752
	ori	$4, $4, 0
	and	$4, $23, $4
	ori	$3, $3, 0
	xor	$3, $4, $3
	beq	$3, $zero, $BB0_8
	nop
# BB#7:                                 #   in Loop: Header=BB0_1 Depth=1
	addiu	$4, $zero, 0
	j	$BB0_9
	nop
$BB0_8:                                 #   in Loop: Header=BB0_1 Depth=1
	lui	$3, 7
	ori	$3, $3, 65535
	and	$3, $23, $3
	or	$3, $7, $3
	addiu	$4, $zero, 0
	xor	$3, $3, $4
	sltu	$4, $zero, $3
$BB0_9:                                 # %float64_is_signaling_nan.exit.i20.i
                                        #   in Loop: Header=BB0_1 Depth=1
	lui	$5, 32767
	lui	$3, 32752
	ori	$5, $5, 65535
	ori	$3, $3, 0
	and	$5, $23, $5
	xor	$6, $5, $3
	beq	$6, $zero, $BB0_11
	nop
# BB#10:                                #   in Loop: Header=BB0_1 Depth=1
	sltu	$3, $3, $5
	j	$BB0_12
	nop
$BB0_11:                                # %float64_is_signaling_nan.exit.i20.i
                                        #   in Loop: Header=BB0_1 Depth=1
	addiu	$3, $zero, 0
	xor	$3, $7, $3
	sltu	$3, $zero, $3
$BB0_12:                                # %float64_is_signaling_nan.exit.i20.i
                                        #   in Loop: Header=BB0_1 Depth=1
	or	$2, $2, $4
	beq	$2, $zero, $BB0_14
	nop
# BB#13:                                # %float64_is_signaling_nan.exit.i20.i
                                        #   in Loop: Header=BB0_1 Depth=1
	addu	$3, $zero, $4
$BB0_14:                                # %float64_is_signaling_nan.exit.i20.i
                                        #   in Loop: Header=BB0_1 Depth=1
	lui	$4, 8
	addu	$5, $zero, $20
	beq	$3, $zero, $BB0_16
	nop
# BB#15:                                # %float64_is_signaling_nan.exit.i20.i
                                        #   in Loop: Header=BB0_1 Depth=1
	addu	$5, $zero, $23
$BB0_16:                                # %float64_is_signaling_nan.exit.i20.i
                                        #   in Loop: Header=BB0_1 Depth=1
	beq	$2, $zero, $BB0_18
	nop
# BB#17:                                # %float64_is_signaling_nan.exit.i20.i
                                        #   in Loop: Header=BB0_1 Depth=1
	ori	$18, $18, 16
$BB0_18:                                # %float64_is_signaling_nan.exit.i20.i
                                        #   in Loop: Header=BB0_1 Depth=1
	addu	$2, $zero, $22
	beq	$3, $zero, $BB0_20
	nop
# BB#19:                                # %float64_is_signaling_nan.exit.i20.i
                                        #   in Loop: Header=BB0_1 Depth=1
	addu	$2, $zero, $7
$BB0_20:                                # %float64_is_signaling_nan.exit.i20.i
                                        #   in Loop: Header=BB0_1 Depth=1
	sw	$7, 80($sp)
$BB0_21:                                # %float64_is_signaling_nan.exit.i20.i
                                        #   in Loop: Header=BB0_1 Depth=1
	ori	$3, $4, 0
	or	$3, $5, $3
	j	$BB0_141
	nop
$BB0_22:                                #   in Loop: Header=BB0_1 Depth=1
	addiu	$2, $zero, 2047
	bne	$19, $2, $BB0_43
	nop
# BB#23:                                #   in Loop: Header=BB0_1 Depth=1
	or	$2, $8, $16
	beq	$2, $zero, $BB0_42
	nop
# BB#24:                                #   in Loop: Header=BB0_1 Depth=1
	lui	$3, 32760
	lui	$2, 32752
	ori	$3, $3, 0
	and	$3, $20, $3
	ori	$2, $2, 0
	xor	$2, $3, $2
	beq	$2, $zero, $BB0_26
	nop
# BB#25:                                #   in Loop: Header=BB0_1 Depth=1
	addiu	$2, $zero, 0
	j	$BB0_27
	nop
$BB0_26:                                #   in Loop: Header=BB0_1 Depth=1
	lui	$2, 7
	ori	$2, $2, 65535
	and	$2, $20, $2
	or	$2, $22, $2
	addiu	$3, $zero, 0
	xor	$2, $2, $3
	sltu	$2, $zero, $2
$BB0_27:                                # %float64_is_signaling_nan.exit1.i12.i
                                        #   in Loop: Header=BB0_1 Depth=1
	lui	$4, 32760
	lui	$3, 32752
	ori	$4, $4, 0
	and	$4, $23, $4
	ori	$3, $3, 0
	xor	$3, $4, $3
	beq	$3, $zero, $BB0_29
	nop
# BB#28:                                #   in Loop: Header=BB0_1 Depth=1
	addiu	$4, $zero, 0
	j	$BB0_30
	nop
$BB0_29:                                #   in Loop: Header=BB0_1 Depth=1
	lui	$3, 7
	ori	$3, $3, 65535
	and	$3, $23, $3
	or	$3, $8, $3
	addiu	$4, $zero, 0
	xor	$3, $3, $4
	sltu	$4, $zero, $3
$BB0_30:                                # %float64_is_signaling_nan.exit.i13.i
                                        #   in Loop: Header=BB0_1 Depth=1
	lui	$5, 32767
	lui	$3, 32752
	ori	$5, $5, 65535
	ori	$3, $3, 0
	and	$5, $23, $5
	xor	$6, $5, $3
	beq	$6, $zero, $BB0_32
	nop
# BB#31:                                #   in Loop: Header=BB0_1 Depth=1
	sltu	$3, $3, $5
	j	$BB0_33
	nop
$BB0_32:                                # %float64_is_signaling_nan.exit.i13.i
                                        #   in Loop: Header=BB0_1 Depth=1
	addiu	$3, $zero, 0
	xor	$3, $8, $3
	sltu	$3, $zero, $3
$BB0_33:                                # %float64_is_signaling_nan.exit.i13.i
                                        #   in Loop: Header=BB0_1 Depth=1
	or	$2, $2, $4
	beq	$2, $zero, $BB0_35
	nop
# BB#34:                                # %float64_is_signaling_nan.exit.i13.i
                                        #   in Loop: Header=BB0_1 Depth=1
	addu	$3, $zero, $4
$BB0_35:                                # %float64_is_signaling_nan.exit.i13.i
                                        #   in Loop: Header=BB0_1 Depth=1
	lui	$4, 8
	addu	$5, $zero, $20
	beq	$3, $zero, $BB0_37
	nop
# BB#36:                                # %float64_is_signaling_nan.exit.i13.i
                                        #   in Loop: Header=BB0_1 Depth=1
	addu	$5, $zero, $23
$BB0_37:                                # %float64_is_signaling_nan.exit.i13.i
                                        #   in Loop: Header=BB0_1 Depth=1
	beq	$2, $zero, $BB0_39
	nop
$BB0_38:                                # %float64_is_signaling_nan.exit.i13.i
                                        #   in Loop: Header=BB0_1 Depth=1
	ori	$18, $18, 16
$BB0_39:                                # %float64_is_signaling_nan.exit.i13.i
                                        #   in Loop: Header=BB0_1 Depth=1
	addu	$2, $zero, $22
	beq	$3, $zero, $BB0_41
	nop
# BB#40:                                # %float64_is_signaling_nan.exit.i13.i
                                        #   in Loop: Header=BB0_1 Depth=1
	addu	$2, $zero, $8
$BB0_41:                                # %float64_is_signaling_nan.exit.i13.i
                                        #   in Loop: Header=BB0_1 Depth=1
	sw	$8, 80($sp)
	j	$BB0_21
	nop
$BB0_42:                                #   in Loop: Header=BB0_1 Depth=1
	sw	$8, 80($sp)
	j	$BB0_65
	nop
$BB0_43:                                #   in Loop: Header=BB0_1 Depth=1
	sw	$8, 80($sp)
	lui	$2, 32752
	sll	$3, $3, 31
	ori	$4, $2, 0
	j	$BB0_67
	nop
$BB0_44:                                #   in Loop: Header=BB0_1 Depth=1
	beq	$19, $zero, $BB0_63
	nop
$BB0_45:                                #   in Loop: Header=BB0_1 Depth=1
	addiu	$2, $zero, 2047
	beq	$19, $2, $BB0_47
	nop
$BB0_46:                                #   in Loop: Header=BB0_1 Depth=1
	sw	$3, 68($sp)
	sw	$20, 72($sp)
	sw	$18, 76($sp)
	addu	$20, $zero, $8
	sw	$8, 80($sp)
	j	$BB0_77
	nop
$BB0_47:                                #   in Loop: Header=BB0_1 Depth=1
	or	$2, $8, $16
	beq	$2, $zero, $BB0_62
	nop
# BB#48:                                #   in Loop: Header=BB0_1 Depth=1
	lui	$3, 32760
	lui	$2, 32752
	ori	$3, $3, 0
	and	$3, $20, $3
	ori	$2, $2, 0
	xor	$2, $3, $2
	beq	$2, $zero, $BB0_50
	nop
# BB#49:                                #   in Loop: Header=BB0_1 Depth=1
	addiu	$2, $zero, 0
	j	$BB0_51
	nop
$BB0_50:                                #   in Loop: Header=BB0_1 Depth=1
	lui	$2, 7
	ori	$2, $2, 65535
	and	$2, $20, $2
	or	$2, $22, $2
	addiu	$3, $zero, 0
	xor	$2, $2, $3
	sltu	$2, $zero, $2
$BB0_51:                                # %float64_is_signaling_nan.exit1.i.i
                                        #   in Loop: Header=BB0_1 Depth=1
	lui	$4, 32760
	lui	$3, 32752
	ori	$4, $4, 0
	and	$4, $23, $4
	ori	$3, $3, 0
	xor	$3, $4, $3
	beq	$3, $zero, $BB0_53
	nop
# BB#52:                                #   in Loop: Header=BB0_1 Depth=1
	addiu	$4, $zero, 0
	j	$BB0_54
	nop
$BB0_53:                                #   in Loop: Header=BB0_1 Depth=1
	lui	$3, 7
	ori	$3, $3, 65535
	and	$3, $23, $3
	or	$3, $8, $3
	addiu	$4, $zero, 0
	xor	$3, $3, $4
	sltu	$4, $zero, $3
$BB0_54:                                # %float64_is_signaling_nan.exit.i.i
                                        #   in Loop: Header=BB0_1 Depth=1
	lui	$5, 32767
	lui	$3, 32752
	ori	$5, $5, 65535
	ori	$3, $3, 0
	and	$5, $23, $5
	xor	$6, $5, $3
	beq	$6, $zero, $BB0_56
	nop
# BB#55:                                #   in Loop: Header=BB0_1 Depth=1
	sltu	$3, $3, $5
	j	$BB0_57
	nop
$BB0_56:                                # %float64_is_signaling_nan.exit.i.i
                                        #   in Loop: Header=BB0_1 Depth=1
	addiu	$3, $zero, 0
	xor	$3, $8, $3
	sltu	$3, $zero, $3
$BB0_57:                                # %float64_is_signaling_nan.exit.i.i
                                        #   in Loop: Header=BB0_1 Depth=1
	or	$2, $2, $4
	beq	$2, $zero, $BB0_59
	nop
# BB#58:                                # %float64_is_signaling_nan.exit.i.i
                                        #   in Loop: Header=BB0_1 Depth=1
	addu	$3, $zero, $4
$BB0_59:                                # %float64_is_signaling_nan.exit.i.i
                                        #   in Loop: Header=BB0_1 Depth=1
	lui	$4, 8
	addu	$5, $zero, $20
	beq	$3, $zero, $BB0_61
	nop
# BB#60:                                # %float64_is_signaling_nan.exit.i.i
                                        #   in Loop: Header=BB0_1 Depth=1
	addu	$5, $zero, $23
$BB0_61:                                # %float64_is_signaling_nan.exit.i.i
                                        #   in Loop: Header=BB0_1 Depth=1
	bne	$2, $zero, $BB0_38
	nop
	j	$BB0_39
	nop
$BB0_62:                                #   in Loop: Header=BB0_1 Depth=1
	sw	$8, 80($sp)
	addiu	$2, $zero, 0
	sll	$3, $3, 31
	j	$BB0_141
	nop
$BB0_63:                                #   in Loop: Header=BB0_1 Depth=1
	or	$2, $8, $16
	bne	$2, $zero, $BB0_68
	nop
# BB#64:                                #   in Loop: Header=BB0_1 Depth=1
	sw	$8, 80($sp)
	or	$2, $21, $22
	or	$2, $17, $2
	bne	$2, $zero, $BB0_66
	nop
$BB0_65:                                #   in Loop: Header=BB0_1 Depth=1
	lui	$3, 32767
	ori	$18, $18, 16
	addiu	$2, $zero, -1
	ori	$3, $3, 65535
	j	$BB0_141
	nop
$BB0_66:                                #   in Loop: Header=BB0_1 Depth=1
	lui	$2, 32752
	sll	$3, $3, 31
	ori	$4, $2, 0
	ori	$18, $18, 2
$BB0_67:                                #   in Loop: Header=BB0_1 Depth=1
	addiu	$2, $zero, 0
	or	$3, $3, $4
	j	$BB0_141
	nop
$BB0_68:                                #   in Loop: Header=BB0_1 Depth=1
	sw	$3, 68($sp)
	lui	$3, 1
	addu	$2, $zero, $16
	bne	$16, $zero, $BB0_70
	nop
# BB#69:                                #   in Loop: Header=BB0_1 Depth=1
	addu	$2, $zero, $8
$BB0_70:                                #   in Loop: Header=BB0_1 Depth=1
	ori	$3, $3, 0
	lui	$4, 255
	sltu	$3, $2, $3
	bne	$3, $zero, $BB0_72
	nop
# BB#71:                                #   in Loop: Header=BB0_1 Depth=1
	sw	$20, 72($sp)
	j	$BB0_73
	nop
$BB0_72:                                #   in Loop: Header=BB0_1 Depth=1
	sw	$20, 72($sp)
	sll	$2, $2, 16
$BB0_73:                                #   in Loop: Header=BB0_1 Depth=1
	ori	$4, $4, 65535
	sll	$3, $3, 4
	sltu	$4, $4, $2
	beq	$4, $zero, $BB0_75
	nop
# BB#74:                                #   in Loop: Header=BB0_1 Depth=1
	sw	$18, 76($sp)
	j	$BB0_76
	nop
$BB0_75:                                #   in Loop: Header=BB0_1 Depth=1
	sw	$18, 76($sp)
	ori	$3, $3, 8
	sll	$2, $2, 8
$BB0_76:                                # %normalizeFloat64Subnormal.exit10.i
                                        #   in Loop: Header=BB0_1 Depth=1
	addiu	$4, $zero, 0
	xor	$4, $16, $4
	sltu	$4, $4, 1
	sll	$4, $4, 5
	lui	$6, %hi(countLeadingZeros32.countLeadingZerosHigh)
	srl	$5, $2, 22
	addiu	$2, $6, %lo(countLeadingZeros32.countLeadingZerosHigh)
	andi	$5, $5, 1020
	addu	$5, $2, $5
	addu	$2, $3, $4
	lw	$3, 0($5)
	nop
	addu	$18, $2, $3
	addiu	$19, $zero, 12
	addiu	$6, $18, -11
	addu	$4, $zero, $8
	sw	$8, 80($sp)
	addu	$5, $zero, $16
	jal	__ashldi3
	nop
	addu	$20, $zero, $2
	addu	$16, $zero, $3
	subu	$19, $19, $18
$BB0_77:                                #   in Loop: Header=BB0_1 Depth=1
	beq	$21, $zero, $BB0_79
	nop
# BB#78:                                #   in Loop: Header=BB0_1 Depth=1
	sw	$23, 60($sp)
	addu	$2, $zero, $22
	sw	$22, 64($sp)
	j	$BB0_88
	nop
$BB0_79:                                #   in Loop: Header=BB0_1 Depth=1
	or	$2, $22, $17
	bne	$2, $zero, $BB0_81
	nop
# BB#80:                                #   in Loop: Header=BB0_1 Depth=1
	addiu	$2, $zero, 0
	lw	$3, 68($sp)
	nop
	sll	$3, $3, 31
	lw	$18, 76($sp)
	nop
	lw	$20, 72($sp)
	nop
	j	$BB0_141
	nop
$BB0_81:                                #   in Loop: Header=BB0_1 Depth=1
	lui	$3, 1
	addu	$2, $zero, $17
	bne	$17, $zero, $BB0_83
	nop
# BB#82:                                #   in Loop: Header=BB0_1 Depth=1
	addu	$2, $zero, $22
$BB0_83:                                #   in Loop: Header=BB0_1 Depth=1
	ori	$3, $3, 0
	lui	$4, 255
	sltu	$3, $2, $3
	beq	$3, $zero, $BB0_85
	nop
# BB#84:                                #   in Loop: Header=BB0_1 Depth=1
	sll	$2, $2, 16
$BB0_85:                                #   in Loop: Header=BB0_1 Depth=1
	sw	$23, 60($sp)
	ori	$4, $4, 65535
	sll	$3, $3, 4
	sltu	$4, $4, $2
	bne	$4, $zero, $BB0_87
	nop
# BB#86:                                #   in Loop: Header=BB0_1 Depth=1
	ori	$3, $3, 8
	sll	$2, $2, 8
$BB0_87:                                # %normalizeFloat64Subnormal.exit.i
                                        #   in Loop: Header=BB0_1 Depth=1
	addiu	$4, $zero, 0
	xor	$4, $17, $4
	sltu	$4, $4, 1
	sll	$4, $4, 5
	lui	$6, %hi(countLeadingZeros32.countLeadingZerosHigh)
	srl	$5, $2, 22
	addiu	$2, $6, %lo(countLeadingZeros32.countLeadingZerosHigh)
	andi	$5, $5, 1020
	addu	$5, $2, $5
	addu	$2, $3, $4
	lw	$3, 0($5)
	nop
	addu	$18, $2, $3
	addiu	$21, $zero, 12
	addiu	$6, $18, -11
	addu	$4, $zero, $22
	sw	$22, 64($sp)
	addu	$5, $zero, $17
	jal	__ashldi3
	nop
	addu	$17, $zero, $3
	subu	$21, $21, $18
$BB0_88:                                #   in Loop: Header=BB0_1 Depth=1
	sll	$23, $2, 10
	lui	$3, 16384
	sll	$4, $17, 10
	srl	$5, $2, 22
	addu	$2, $23, $23
	or	$4, $4, $5
	ori	$3, $3, 0
	or	$22, $4, $3
	sltu	$4, $2, $23
	lui	$3, 32768
	sll	$5, $16, 11
	srl	$6, $20, 21
	addu	$4, $4, $22
	or	$5, $5, $6
	ori	$6, $3, 0
	sll	$18, $20, 11
	subu	$3, $21, $19
	or	$20, $5, $6
	addu	$4, $22, $4
	addiu	$19, $3, 1021
	xor	$5, $20, $4
	beq	$5, $zero, $BB0_90
	nop
# BB#89:                                #   in Loop: Header=BB0_1 Depth=1
	sltu	$2, $4, $20
	j	$BB0_91
	nop
$BB0_90:                                #   in Loop: Header=BB0_1 Depth=1
	sltu	$2, $2, $18
$BB0_91:                                #   in Loop: Header=BB0_1 Depth=1
	bne	$2, $zero, $BB0_93
	nop
# BB#92:                                #   in Loop: Header=BB0_1 Depth=1
	srl	$2, $23, 1
	sll	$4, $22, 31
	addiu	$19, $3, 1022
	or	$23, $2, $4
	srl	$22, $22, 1
$BB0_93:                                #   in Loop: Header=BB0_1 Depth=1
	sltu	$2, $22, $20
	addiu	$21, $zero, -1
	xor	$3, $20, $22
	beq	$3, $zero, $BB0_95
	nop
# BB#94:                                #   in Loop: Header=BB0_1 Depth=1
	xori	$4, $2, 1
	j	$BB0_96
	nop
$BB0_95:                                #   in Loop: Header=BB0_1 Depth=1
	sltu	$4, $23, $18
	xori	$4, $4, 1
$BB0_96:                                #   in Loop: Header=BB0_1 Depth=1
	addu	$16, $zero, $21
	bne	$4, $zero, $BB0_120
	nop
# BB#97:                                #   in Loop: Header=BB0_1 Depth=1
	addiu	$17, $zero, 0
	addiu	$16, $zero, -1
	beq	$3, $zero, $BB0_99
	nop
# BB#98:                                #   in Loop: Header=BB0_1 Depth=1
	xori	$2, $2, 1
	j	$BB0_100
	nop
$BB0_99:                                #   in Loop: Header=BB0_1 Depth=1
	addiu	$2, $zero, 1
$BB0_100:                               #   in Loop: Header=BB0_1 Depth=1
	sw	$19, 56($sp)
	addu	$21, $zero, $17
	bne	$2, $zero, $BB0_102
	nop
# BB#101:                               #   in Loop: Header=BB0_1 Depth=1
	addiu	$21, $zero, 0
	addu	$4, $zero, $23
	addu	$5, $zero, $22
	addu	$6, $zero, $20
	addu	$7, $zero, $17
	jal	__udivdi3
	nop
	addu	$16, $zero, $2
$BB0_102:                               #   in Loop: Header=BB0_1 Depth=1
	addiu	$2, $zero, -2048
	and	$2, $18, $2
	sw	$2, 40($sp)
	multu	$16, $20
	mflo	$6
	mfhi	$5
	addiu	$19, $zero, 0
	mult	$16, $17
	mflo	$8
	multu	$16, $2
	mflo	$2
	mfhi	$7
	addiu	$3, $zero, -1
	addu	$4, $zero, $19
	beq	$2, $zero, $BB0_104
	nop
# BB#103:                               #   in Loop: Header=BB0_1 Depth=1
	addu	$4, $zero, $3
$BB0_104:                               #   in Loop: Header=BB0_1 Depth=1
	subu	$9, $23, $6
	sltu	$6, $23, $6
	addu	$8, $5, $8
	subu	$5, $9, $7
	addu	$5, $5, $4
	sltu	$7, $9, $7
	addu	$9, $6, $8
	sltu	$8, $19, $19
	sltu	$6, $5, $4
	subu	$9, $22, $9
	addu	$10, $7, $19
	addu	$7, $8, $2
	subu	$2, $9, $10
	addu	$4, $6, $4
	addu	$2, $2, $4
	subu	$4, $19, $7
	slt	$3, $3, $2
	bne	$3, $zero, $BB0_107
	nop
# BB#105:                               # %.lr.ph.i.i
                                        #   in Loop: Header=BB0_1 Depth=1
	subu	$3, $19, $19
	addiu	$6, $zero, 0
$BB0_106:                               #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addu	$3, $3, $6
	sltu	$7, $3, $6
	addu	$7, $7, $18
	addu	$4, $4, $7
	sltu	$8, $4, $18
	addiu	$7, $zero, 0
	addu	$8, $8, $7
	addu	$8, $6, $8
	addu	$9, $5, $20
	andi	$8, $8, 1
	addu	$5, $9, $8
	sltu	$9, $9, $20
	sltu	$8, $5, $8
	addu	$9, $9, $17
	addu	$2, $2, $9
	addu	$7, $8, $7
	addu	$2, $2, $7
	addiu	$16, $16, -1
	bltz	$2, $BB0_106
	nop
$BB0_107:                               # %._crit_edge.i.i
                                        #   in Loop: Header=BB0_1 Depth=1
	addiu	$2, $zero, -1
	addiu	$3, $zero, 0
	xor	$6, $20, $5
	beq	$6, $zero, $BB0_109
	nop
# BB#108:                               #   in Loop: Header=BB0_1 Depth=1
	sltu	$6, $5, $20
	j	$BB0_110
	nop
$BB0_109:                               # %._crit_edge.i.i
                                        #   in Loop: Header=BB0_1 Depth=1
	sltu	$6, $4, $17
$BB0_110:                               # %._crit_edge.i.i
                                        #   in Loop: Header=BB0_1 Depth=1
	xori	$6, $6, 1
	bne	$6, $zero, $BB0_112
	nop
# BB#111:                               #   in Loop: Header=BB0_1 Depth=1
	addu	$6, $zero, $20
	addu	$7, $zero, $17
	jal	__udivdi3
	nop
$BB0_112:                               # %estimateDiv128To64.exit.i
                                        #   in Loop: Header=BB0_1 Depth=1
	or	$21, $2, $21
	or	$16, $3, $16
	addiu	$2, $zero, 2
	andi	$3, $21, 511
	sltu	$2, $2, $3
	beq	$2, $zero, $BB0_114
	nop
# BB#113:                               #   in Loop: Header=BB0_1 Depth=1
	lw	$19, 56($sp)
	nop
	j	$BB0_120
	nop
$BB0_114:                               #   in Loop: Header=BB0_1 Depth=1
	lw	$2, 40($sp)
	nop
	multu	$16, $2
	mflo	$3
	mfhi	$4
	multu	$21, $20
	mflo	$9
	mfhi	$8
	multu	$21, $2
	mflo	$2
	mfhi	$5
	mult	$21, $19
	mflo	$6
	addu	$12, $3, $9
	addu	$24, $5, $6
	addu	$5, $12, $24
	addiu	$3, $zero, 0
	mult	$21, $17
	mflo	$15
	mult	$16, $19
	mflo	$14
	multu	$16, $20
	mflo	$11
	mfhi	$10
	mult	$16, $17
	mflo	$13
	addiu	$6, $zero, -1
	or	$25, $2, $5
	addu	$7, $zero, $3
	beq	$25, $zero, $BB0_116
	nop
# BB#115:                               #   in Loop: Header=BB0_1 Depth=1
	addu	$7, $zero, $6
$BB0_116:                               #   in Loop: Header=BB0_1 Depth=1
	sltu	$24, $5, $24
	addu	$24, $24, $3
	addu	$24, $3, $24
	sltu	$12, $12, $9
	addu	$15, $8, $15
	sll	$9, $24, 31
	addu	$4, $4, $14
	addu	$8, $12, $15
	addu	$8, $4, $8
	subu	$4, $23, $11
	sra	$12, $9, 31
	addu	$9, $4, $12
	sltu	$14, $8, $15
	sltu	$4, $23, $11
	addu	$11, $10, $13
	sltu	$13, $9, $12
	addu	$10, $14, $3
	addu	$11, $4, $11
	subu	$4, $9, $8
	addu	$4, $4, $7
	subu	$11, $22, $11
	addu	$12, $13, $12
	sltu	$8, $9, $8
	addu	$13, $3, $10
	sltu	$10, $3, $2
	sltu	$9, $4, $7
	addu	$11, $11, $12
	addu	$12, $8, $13
	addu	$8, $10, $5
	subu	$5, $11, $12
	addu	$7, $9, $7
	addu	$5, $5, $7
	subu	$2, $3, $2
	subu	$3, $3, $8
	slt	$6, $6, $5
	lw	$19, 56($sp)
	nop
	bne	$6, $zero, $BB0_119
	nop
# BB#117:                               # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB0_1 Depth=1
	addiu	$6, $zero, 0
$BB0_118:                               # %.lr.ph.i
                                        #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addu	$2, $2, $18
	sltu	$7, $2, $18
	addu	$7, $7, $20
	addu	$3, $3, $7
	sltu	$7, $3, $20
	addiu	$8, $zero, 0
	addu	$7, $7, $8
	addu	$7, $6, $7
	andi	$7, $7, 1
	addu	$7, $7, $4
	addiu	$21, $21, -1
	addiu	$9, $zero, -1
	sltu	$10, $7, $4
	sltu	$4, $21, $9
	addu	$8, $10, $8
	addu	$4, $4, $9
	addu	$5, $5, $8
	addu	$16, $16, $4
	addu	$4, $zero, $7
	bltz	$5, $BB0_118
	nop
$BB0_119:                               # %._crit_edge.i
                                        #   in Loop: Header=BB0_1 Depth=1
	or	$2, $2, $3
	addiu	$3, $zero, 0
	xor	$2, $2, $3
	sltu	$2, $zero, $2
	or	$21, $2, $21
$BB0_120:                               # %estimateDiv128To64.exit.thread.i
                                        #   in Loop: Header=BB0_1 Depth=1
	andi	$3, $19, 65535
	andi	$2, $21, 1023
	sltiu	$3, $3, 2045
	beq	$3, $zero, $BB0_122
	nop
# BB#121:                               #   in Loop: Header=BB0_1 Depth=1
	lw	$18, 76($sp)
	nop
	lw	$20, 72($sp)
	nop
	lw	$22, 64($sp)
	nop
	lw	$23, 60($sp)
	nop
	j	$BB0_134
	nop
$BB0_122:                               #   in Loop: Header=BB0_1 Depth=1
	addiu	$3, $zero, 2045
	slt	$3, $3, $19
	lw	$18, 76($sp)
	nop
	lw	$20, 72($sp)
	nop
	lw	$22, 64($sp)
	nop
	lw	$23, 60($sp)
	nop
	bne	$3, $zero, $BB0_126
	nop
# BB#123:                               #   in Loop: Header=BB0_1 Depth=1
	addiu	$3, $zero, 2045
	bne	$19, $3, $BB0_127
	nop
# BB#124:                               #   in Loop: Header=BB0_1 Depth=1
	addiu	$3, $21, 512
	addiu	$4, $zero, 512
	sltu	$3, $3, $4
	addiu	$4, $zero, 0
	addu	$4, $3, $4
	addiu	$3, $zero, -1
	addu	$4, $16, $4
	slt	$3, $3, $4
	beq	$3, $zero, $BB0_126
	nop
# BB#125:                               #   in Loop: Header=BB0_1 Depth=1
	addiu	$19, $zero, 2045
	j	$BB0_134
	nop
$BB0_126:                               #   in Loop: Header=BB0_1 Depth=1
	lui	$2, 32752
	lw	$3, 68($sp)
	nop
	sll	$3, $3, 31
	ori	$4, $2, 0
	ori	$18, $18, 9
	j	$BB0_67
	nop
$BB0_127:                               #   in Loop: Header=BB0_1 Depth=1
	addiu	$3, $zero, -1
	slt	$3, $3, $19
	bne	$3, $zero, $BB0_134
	nop
# BB#128:                               #   in Loop: Header=BB0_1 Depth=1
	slti	$2, $19, -63
	bne	$2, $zero, $BB0_130
	nop
# BB#129:                               #   in Loop: Header=BB0_1 Depth=1
	andi	$6, $19, 63
	addu	$4, $zero, $21
	addu	$5, $zero, $16
	jal	__ashldi3
	nop
	addiu	$4, $zero, 0
	or	$2, $2, $3
	xor	$17, $2, $4
	subu	$6, $4, $19
	addu	$4, $zero, $21
	addu	$5, $zero, $16
	jal	__lshrdi3
	nop
	sltu	$4, $zero, $17
	or	$21, $4, $2
	j	$BB0_131
	nop
$BB0_130:                               #   in Loop: Header=BB0_1 Depth=1
	addiu	$3, $zero, 0
	or	$2, $21, $16
	xor	$2, $2, $3
	sltu	$21, $zero, $2
$BB0_131:                               # %shift64RightJamming.exit.i.i
                                        #   in Loop: Header=BB0_1 Depth=1
	andi	$2, $21, 1023
	bne	$2, $zero, $BB0_133
	nop
# BB#132:                               #   in Loop: Header=BB0_1 Depth=1
	addiu	$2, $zero, 0
	addu	$19, $zero, $2
	addu	$16, $zero, $3
	j	$BB0_138
	nop
$BB0_133:                               #   in Loop: Header=BB0_1 Depth=1
	ori	$18, $18, 4
	addiu	$19, $zero, 0
	addu	$16, $zero, $3
$BB0_134:                               # %.thread.i.i
                                        #   in Loop: Header=BB0_1 Depth=1
	beq	$2, $zero, $BB0_136
	nop
# BB#135:                               #   in Loop: Header=BB0_1 Depth=1
	ori	$18, $18, 1
$BB0_136:                               # %.thread.i.i
                                        #   in Loop: Header=BB0_1 Depth=1
	bne	$2, $zero, $BB0_138
	nop
# BB#137:                               # %.thread.i.i
                                        #   in Loop: Header=BB0_1 Depth=1
	addiu	$2, $zero, 0
$BB0_138:                               # %.thread6.i.i
                                        #   in Loop: Header=BB0_1 Depth=1
	addiu	$5, $21, 512
	addiu	$6, $zero, 512
	sltu	$4, $5, $6
	addiu	$3, $zero, 0
	xor	$2, $2, $6
	addu	$4, $4, $3
	addu	$4, $16, $4
	sltu	$7, $2, 1
	srl	$2, $5, 10
	sll	$6, $4, 22
	nor	$5, $7, $zero
	addiu	$7, $zero, -2
	or	$2, $2, $6
	or	$5, $5, $7
	and	$2, $2, $5
	srl	$4, $4, 10
	lw	$5, 68($sp)
	nop
	sll	$5, $5, 31
	or	$6, $2, $4
	beq	$6, $zero, $BB0_140
	nop
# BB#139:                               #   in Loop: Header=BB0_1 Depth=1
	sll	$3, $19, 20
$BB0_140:                               # %.thread6.i.i
                                        #   in Loop: Header=BB0_1 Depth=1
	or	$4, $4, $5
	addu	$3, $3, $4
$BB0_141:                               # %float64_div.exit
                                        #   in Loop: Header=BB0_1 Depth=1
	lui	$4, %hi(z_output)
	addiu	$4, $4, %lo(z_output)
	lw	$5, 44($sp)
	nop
	subu	$4, $4, $5
	lw	$5, 0($4)
	nop
	lw	$4, 4($4)
	nop
	xor	$6, $2, $5
	xor	$7, $3, $4
	or	$6, $6, $7
	addiu	$7, $zero, 0
	xor	$7, $6, $7
	lui	$6, %hi($.str)
	sltu	$7, $7, 1
	lw	$17, 48($sp)
	nop
	addiu	$17, $17, -1
	lw	$8, 52($sp)
	nop
	addu	$8, $7, $8
	sw	$8, 52($sp)
	lw	$7, 80($sp)
	nop
	sw	$7, 16($sp)
	sw	$23, 20($sp)
	sw	$5, 24($sp)
	sw	$4, 28($sp)
	sw	$2, 32($sp)
	sw	$3, 36($sp)
	addiu	$4, $6, %lo($.str)
	addiu	$16, $zero, -22
	addu	$6, $zero, $22
	addu	$7, $zero, $20
	jal	printf
	nop
	addu	$4, $zero, $17
	bne	$4, $16, $BB0_1
	nop
# BB#142:
	lui	$2, %hi($.str1)
	addiu	$4, $2, %lo($.str1)
	addiu	$16, $zero, 22
	lw	$18, 52($sp)
	nop
	addu	$5, $zero, $18
	jal	printf
	nop
	addu	$17, $zero, $18
	bne	$18, $16, $BB0_144
	nop
# BB#143:
	lui	$2, %hi($.str2)
	addiu	$4, $2, %lo($.str2)
	j	$BB0_145
	nop
$BB0_144:
	lui	$2, %hi($.str3)
	addiu	$4, $2, %lo($.str3)
$BB0_145:
	jal	printf
	nop
	addu	$2, $zero, $17
	lw	$16, 84($sp)
	nop
	lw	$17, 88($sp)
	nop
	lw	$18, 92($sp)
	nop
	lw	$19, 96($sp)
	nop
	lw	$20, 100($sp)
	nop
	lw	$21, 104($sp)
	nop
	lw	$22, 108($sp)
	nop
	lw	$23, 112($sp)
	nop
	lw	$ra, 116($sp)
	nop
	addiu	$sp, $sp, 120
	jr	$ra
	nop
	.set	macro
	.set	reorder
	.end	main
$tmp13:
	.size	main, ($tmp13)-main
$tmp14:
	.cfi_endproc
$eh_func_end0:

	.type	a_input,@object         # @a_input
	.section	.rodata,"a",@progbits
	.align	3
a_input:
	.4byte	0                       # 0x7fff000000000000
	.4byte	2147418112
	.4byte	0                       # 0x7ff0000000000000
	.4byte	2146435072
	.4byte	0                       # 0x7ff0000000000000
	.4byte	2146435072
	.4byte	0                       # 0x7ff0000000000000
	.4byte	2146435072
	.4byte	0                       # 0x3ff0000000000000
	.4byte	1072693248
	.4byte	0                       # 0x3ff0000000000000
	.4byte	1072693248
	.4byte	0                       # 0x0
	.4byte	0
	.4byte	0                       # 0x3ff0000000000000
	.4byte	1072693248
	.4byte	0                       # 0x0
	.4byte	0
	.4byte	0                       # 0x8000000000000000
	.4byte	2147483648
	.4byte	0                       # 0x4008000000000000
	.4byte	1074266112
	.4byte	0                       # 0xc008000000000000
	.4byte	3221749760
	.4byte	0                       # 0x4008000000000000
	.4byte	1074266112
	.4byte	0                       # 0xc008000000000000
	.4byte	3221749760
	.4byte	0                       # 0x4000000000000000
	.4byte	1073741824
	.4byte	0                       # 0xc000000000000000
	.4byte	3221225472
	.4byte	0                       # 0x4000000000000000
	.4byte	1073741824
	.4byte	0                       # 0xc000000000000000
	.4byte	3221225472
	.4byte	0                       # 0x3ff0000000000000
	.4byte	1072693248
	.4byte	0                       # 0xbff0000000000000
	.4byte	3220176896
	.4byte	0                       # 0x3ff0000000000000
	.4byte	1072693248
	.4byte	0                       # 0xbff0000000000000
	.4byte	3220176896
	.size	a_input, 176

	.type	b_input,@object         # @b_input
	.align	3
b_input:
	.4byte	0                       # 0x3ff0000000000000
	.4byte	1072693248
	.4byte	0                       # 0x7ff8000000000000
	.4byte	2146959360
	.4byte	0                       # 0x7ff0000000000000
	.4byte	2146435072
	.4byte	0                       # 0x3ff0000000000000
	.4byte	1072693248
	.4byte	0                       # 0x7ff8000000000000
	.4byte	2146959360
	.4byte	0                       # 0x7ff0000000000000
	.4byte	2146435072
	.4byte	0                       # 0x0
	.4byte	0
	.4byte	0                       # 0x0
	.4byte	0
	.4byte	0                       # 0x3ff0000000000000
	.4byte	1072693248
	.4byte	0                       # 0x3ff0000000000000
	.4byte	1072693248
	.4byte	0                       # 0x4000000000000000
	.4byte	1073741824
	.4byte	0                       # 0x4000000000000000
	.4byte	1073741824
	.4byte	0                       # 0xc000000000000000
	.4byte	3221225472
	.4byte	0                       # 0xc000000000000000
	.4byte	3221225472
	.4byte	0                       # 0x4010000000000000
	.4byte	1074790400
	.4byte	0                       # 0x4010000000000000
	.4byte	1074790400
	.4byte	0                       # 0xc010000000000000
	.4byte	3222274048
	.4byte	0                       # 0xc010000000000000
	.4byte	3222274048
	.4byte	0                       # 0x3ff8000000000000
	.4byte	1073217536
	.4byte	0                       # 0x3ff8000000000000
	.4byte	1073217536
	.4byte	0                       # 0xbff8000000000000
	.4byte	3220701184
	.4byte	0                       # 0xbff8000000000000
	.4byte	3220701184
	.size	b_input, 176

	.type	z_output,@object        # @z_output
	.align	3
z_output:
	.4byte	0                       # 0x7fff000000000000
	.4byte	2147418112
	.4byte	0                       # 0x7ff8000000000000
	.4byte	2146959360
	.4byte	4294967295              # 0x7fffffffffffffff
	.4byte	2147483647
	.4byte	0                       # 0x7ff0000000000000
	.4byte	2146435072
	.4byte	0                       # 0x7ff8000000000000
	.4byte	2146959360
	.4byte	0                       # 0x0
	.4byte	0
	.4byte	4294967295              # 0x7fffffffffffffff
	.4byte	2147483647
	.4byte	0                       # 0x7ff0000000000000
	.4byte	2146435072
	.4byte	0                       # 0x0
	.4byte	0
	.4byte	0                       # 0x8000000000000000
	.4byte	2147483648
	.4byte	0                       # 0x3ff8000000000000
	.4byte	1073217536
	.4byte	0                       # 0xbff8000000000000
	.4byte	3220701184
	.4byte	0                       # 0xbff8000000000000
	.4byte	3220701184
	.4byte	0                       # 0x3ff8000000000000
	.4byte	1073217536
	.4byte	0                       # 0x3fe0000000000000
	.4byte	1071644672
	.4byte	0                       # 0xbfe0000000000000
	.4byte	3219128320
	.4byte	0                       # 0xbfe0000000000000
	.4byte	3219128320
	.4byte	0                       # 0x3fe0000000000000
	.4byte	1071644672
	.4byte	1431655765              # 0x3fe5555555555555
	.4byte	1071994197
	.4byte	1431655765              # 0xbfe5555555555555
	.4byte	3219477845
	.4byte	1431655765              # 0xbfe5555555555555
	.4byte	3219477845
	.4byte	1431655765              # 0x3fe5555555555555
	.4byte	1071994197
	.size	z_output, 176

	.type	$.str,@object           # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
$.str:
	.asciz	 "a_input=%016llx b_input=%016llx expected=%016llx output=%016llx\n"
	.size	$.str, 65

	.type	$.str1,@object          # @.str1
$.str1:
	.asciz	 "Result: %d\n"
	.size	$.str1, 12

	.type	$.str2,@object          # @.str2
$.str2:
	.asciz	 "RESULT: PASS\n"
	.size	$.str2, 14

	.type	$.str3,@object          # @.str3
$.str3:
	.asciz	 "RESULT: FAIL\n"
	.size	$.str3, 14

	.type	countLeadingZeros32.countLeadingZerosHigh,@object # @countLeadingZeros32.countLeadingZerosHigh
	.section	.rodata,"a",@progbits
	.align	2
countLeadingZeros32.countLeadingZerosHigh:
	.4byte	8                       # 0x8
	.4byte	7                       # 0x7
	.4byte	6                       # 0x6
	.4byte	6                       # 0x6
	.4byte	5                       # 0x5
	.4byte	5                       # 0x5
	.4byte	5                       # 0x5
	.4byte	5                       # 0x5
	.4byte	4                       # 0x4
	.4byte	4                       # 0x4
	.4byte	4                       # 0x4
	.4byte	4                       # 0x4
	.4byte	4                       # 0x4
	.4byte	4                       # 0x4
	.4byte	4                       # 0x4
	.4byte	4                       # 0x4
	.4byte	3                       # 0x3
	.4byte	3                       # 0x3
	.4byte	3                       # 0x3
	.4byte	3                       # 0x3
	.4byte	3                       # 0x3
	.4byte	3                       # 0x3
	.4byte	3                       # 0x3
	.4byte	3                       # 0x3
	.4byte	3                       # 0x3
	.4byte	3                       # 0x3
	.4byte	3                       # 0x3
	.4byte	3                       # 0x3
	.4byte	3                       # 0x3
	.4byte	3                       # 0x3
	.4byte	3                       # 0x3
	.4byte	3                       # 0x3
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	2                       # 0x2
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	1                       # 0x1
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.4byte	0                       # 0x0
	.size	countLeadingZeros32.countLeadingZerosHigh, 1024


