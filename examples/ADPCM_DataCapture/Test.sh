#!/bin/bash


for i in {0..118}
do

	echo ITERATION $i

	cp ./instances/adpcm$i.c ./adpcm.c

	perl -pi -e 's/adpcm_main/main/g' ./adpcm.c 

	make

	make v

	mv "./transcript" "./results/transcript$i"
	mv "./resources.legup.rpt" "./results/resources$i.legup.rpt"
	mv "./scheduling.legup.rpt" "./results/scheduling$i.legup.rpt"



done
