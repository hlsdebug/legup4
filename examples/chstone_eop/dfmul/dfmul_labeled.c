extern void __legup_label(char* label);
#include "milieu.h"
#include "softfloat.h"
volatile int16 eop_245_zExp_297;
volatile int eop_121_i_124;
volatile int16 eop_245_zExp_297;
volatile bits64 eop_246_bSig_291;
volatile bits64 eop_246_aSig_290;
volatile int16 eop_245_zExp_289;
volatile flag eop_244_zSign_254;
volatile flag eop_244_bSign_253;
volatile int16 eop_245_bExp_252;
volatile bits64 eop_246_bSig_251;
volatile flag eop_244_aSign_250;
volatile int16 eop_245_aExp_249;
volatile bits64 eop_246_aSig_248;
volatile int16 eop_174_zExp_230;
volatile bits64 eop_174_zSig_227;
volatile int16 eop_178_roundBits_220;
volatile int16 eop_174_zExp_219;
volatile flag eop_177_isTiny_215;
volatile int16 eop_178_roundBits_204;
volatile int16 eop_178_roundIncrement_200;
volatile int16 eop_178_roundIncrement_195;
volatile int16 eop_178_roundIncrement_191;
volatile int16 eop_178_roundIncrement_187;
volatile int16 eop_178_roundIncrement_182;
volatile flag eop_177_roundNearestEven_181;
volatile int8 eop_176_roundingMode_180;
volatile int8 eop_124_shiftCount_126;
volatile flag eop_109_bIsSignalingNaN_114;
volatile flag eop_109_bIsNaN_113;
volatile flag eop_109_aIsSignalingNaN_112;
volatile flag eop_109_aIsNaN_111;
volatile int8 eop_162_shiftCount_173;
volatile int8 eop_162_shiftCount_167;
volatile int8 eop_162_shiftCount_164;
volatile int8 eop_136_shiftCount_149;
volatile int8 eop_136_shiftCount_146;
volatile int8 eop_136_shiftCount_141;
volatile int8 eop_136_shiftCount_138;
volatile bits64 eop_90_z0_104;
volatile bits64 eop_90_z1_103;
volatile bits64 eop_90_z0_101;
volatile bits64 eop_90_zMiddleA_100;
volatile bits64 eop_90_z0_99;
volatile bits64 eop_90_zMiddleB_98;
volatile bits64 eop_90_zMiddleA_97;
volatile bits64 eop_90_z1_96;
volatile bits32 eop_89_bHigh_95;
volatile bits32 eop_89_bLow_94;
volatile bits32 eop_89_aHigh_93;
volatile bits32 eop_89_aLow_92;
volatile bits64 eop_62_z_74;
volatile bits64 eop_62_z_70;
volatile bits64 eop_62_z_66;
volatile int eop_120_main_result_130;
volatile float64 eop_126_result_129;
volatile float64 eop_122_x2_128;
volatile float64 eop_122_x1_127;
volatile int eop_121_i_124;
volatile int eop_120_main_result_123;
volatile bits64 eop_246_bSig_291;
volatile bits64 eop_246_aSig_290;
volatile int16 eop_245_zExp_289;
volatile flag eop_244_zSign_254;
volatile flag eop_244_bSign_253;
volatile int16 eop_245_bExp_252;
volatile bits64 eop_246_bSig_251;
volatile flag eop_244_aSign_250;
volatile int16 eop_245_aExp_249;
volatile bits64 eop_246_aSig_248;
volatile int16 eop_174_zExp_230;
volatile bits64 eop_174_zSig_227;
volatile int16 eop_178_roundBits_220;
volatile int16 eop_174_zExp_219;
volatile flag eop_177_isTiny_215;
volatile int16 eop_178_roundBits_204;
volatile int16 eop_178_roundIncrement_200;
volatile int16 eop_178_roundIncrement_195;
volatile int16 eop_178_roundIncrement_191;
volatile int16 eop_178_roundIncrement_187;
volatile int16 eop_178_roundIncrement_182;
volatile flag eop_177_roundNearestEven_181;
volatile int8 eop_176_roundingMode_180;
volatile int8 eop_124_shiftCount_126;
volatile flag eop_109_bIsSignalingNaN_114;
volatile flag eop_109_bIsNaN_113;
volatile flag eop_109_aIsSignalingNaN_112;
volatile flag eop_109_aIsNaN_111;
volatile int8 eop_162_shiftCount_173;
volatile int8 eop_162_shiftCount_167;
volatile int8 eop_162_shiftCount_164;
volatile int8 eop_136_shiftCount_149;
volatile int8 eop_136_shiftCount_146;
volatile int8 eop_136_shiftCount_141;
volatile int8 eop_136_shiftCount_138;
volatile bits64 eop_90_z0_104;
volatile bits64 eop_90_z1_103;
volatile bits64 eop_90_z0_101;
volatile bits64 eop_90_zMiddleA_100;
volatile bits64 eop_90_z0_99;
volatile bits64 eop_90_zMiddleB_98;
volatile bits64 eop_90_zMiddleA_97;
volatile bits64 eop_90_z1_96;
volatile bits32 eop_89_bHigh_95;
volatile bits32 eop_89_bLow_94;
volatile bits32 eop_89_aHigh_93;
volatile bits32 eop_89_aLow_92;
volatile bits64 eop_62_z_74;
volatile bits64 eop_62_z_70;
volatile bits64 eop_62_z_66;
/*
+--------------------------------------------------------------------------+
| CHStone : a suite of benchmark programs for C-based High-Level Synthesis |
| ======================================================================== |
|                                                                          |
| * Collected and Modified : Y. Hara, H. Tomiyama, S. Honda,               |
|                            H. Takada and K. Ishii                        |
|                            Nagoya University, Japan                      |
|                                                                          |
| * Remark :                                                               |
|    1. This source code is modified to unify the formats of the benchmark |
|       programs in CHStone.                                               |
|    2. Test vectors are added for CHStone.                                |
|    3. If "main_result" is 0 at the end of the program, the program is    |
|       correctly executed.                                                |
|    4. Please follow the copyright of each benchmark program.             |
+--------------------------------------------------------------------------+
*/
/*
 * Copyright (C) 2008
 * Y. Hara, H. Tomiyama, S. Honda, H. Takada and K. Ishii
 * Nagoya University, Japan
 * All rights reserved.
 *
 * Disclaimer of Warranty
 *
 * These software programs are available to the user without any license fee or
 * royalty on an "as is" basis. The authors disclaims any and all warranties, 
 * whether express, implied, or statuary, including any implied warranties or 
 * merchantability or of fitness for a particular purpose. In no event shall the
 * copyright-holder be liable for any incidental, punitive, or consequential damages
 * of any kind whatsoever arising from the use of these programs. This disclaimer
 * of warranty extends to the user of these programs and user's customers, employees,
 * agents, transferees, successors, and assigns.
 *
 */
#include <stdio.h>
#include "softfloat.c"
/*
+--------------------------------------------------------------------------+
| * Test Vectors (added for CHStone)                                       |
|     a_input, b_input : input data                                        |
|     z_output : expected output data                                      |
+--------------------------------------------------------------------------+
*/
#define N 20
const float64 a_input[20] = {(0x7FF0000000000000ULL), (0x7FFF000000000000ULL), (0x7FF0000000000000ULL), (0x7FF0000000000000ULL), (0x3FF0000000000000ULL), (0x0000000000000000ULL), (0x3FF0000000000000ULL), (0x0000000000000000ULL), (0x8000000000000000ULL), (0x3FF0000000000000ULL), (0x3FF0000000000000ULL), (0x4000000000000000ULL), (0x3FD0000000000000ULL), (0xC000000000000000ULL), (0xBFD0000000000000ULL), (0x4000000000000000ULL), (0xBFD0000000000000ULL), (0xC000000000000000ULL), (0x3FD0000000000000ULL), (0x0000000000000000ULL)
/* inf */
/* nan */
/* inf */
/* inf */
/* 1.0 */
/* 0.0 */
/* 1.0 */
/* 0.0 */
/* -0.0 */
/* 1.0 */
/* 1.0 */
/* 2.0 */
/* 0.25 */
/* -2.0 */
/* -0.25 */
/* 2.0 */
/* -0.25 */
/* -2.0 */
/* 0.25 */
/* 0.0 */
};
const float64 b_input[20] = {(0xFFFFFFFFFFFFFFFFULL), (0xFFF0000000000000ULL), (0x0000000000000000ULL), (0x3FF0000000000000ULL), (0xFFFF000000000000ULL), (0x7FF0000000000000ULL), (0x7FF0000000000000ULL), (0x3FF0000000000000ULL), (0x3FF0000000000000ULL), (0x0000000000000000ULL), (0x8000000000000000ULL), (0x3FD0000000000000ULL), (0x4000000000000000ULL), (0xBFD0000000000000ULL), (0xC000000000000000ULL), (0xBFD0000000000000ULL), (0x4000000000000000ULL), (0x3FD0000000000000ULL), (0xC000000000000000ULL), (0x0000000000000000ULL)
/* nan */
/* -inf */
/* nan */
/* -inf */
/* nan */
/* inf */
/* inf */
/* 1.0 */
/* 1.0 */
/* 0.0 */
/* -0.0 */
/* 0.25 */
/* 2.0 */
/* -0.25 */
/* -2.0 */
/* -0.25 */
/* -2.0 */
/* 0.25 */
/* -2.0 */
/* 0.0 */
};
const float64 z_output[20] = {(0xFFFFFFFFFFFFFFFFULL), (0x7FFF000000000000ULL), (0x7FFFFFFFFFFFFFFFULL), (0x7FF0000000000000ULL), (0xFFFF000000000000ULL), (0x7FFFFFFFFFFFFFFFULL), (0x7FF0000000000000ULL), (0x0000000000000000ULL), (0x8000000000000000ULL), (0x0000000000000000ULL), (0x8000000000000000ULL), (0x3FE0000000000000ULL), (0x3FE0000000000000ULL), (0x3FE0000000000000ULL), (0x3FE0000000000000ULL), (0xBFE0000000000000ULL), (0xBFE0000000000000ULL), (0xBFE0000000000000ULL), (0xBFE0000000000000ULL), (0x0000000000000000ULL)
/* nan */
/* nan */
/* nan */
/* inf */
/* nan */
/* nan */
/* inf */
/* 0.0 */
/* -0.0 */
/* 0.0 */
/* -0.0 */
/* 0.5 */
/* 0.5 */
/* 0.5 */
/* 0.5 */
/* -0.5 */
/* -0.5 */
/* -0.5 */
/* -0.5 */
/* 0.0 */
};

int main()
{
  int main_result;
  int i;
  float64 x1;
  float64 x2;
  main_result = 0;
  eop_120_main_result_123 = 0;
  for (i = 0; i < 20; i++) {
    float64 result;
    x1 = a_input[i];
    eop_122_x1_127 = x1;
    x2 = b_input[i];
    eop_122_x2_128 = x2;
    result = float64_mul(x1,x2);
    eop_126_result_129 = result;
    main_result += result == z_output[i];
    eop_120_main_result_130 = main_result;
    printf("a_input=%016llx b_input=%016llx expected=%016llx output=%016llx\n",a_input[i],b_input[i],z_output[i],result);
  }
  printf("Result: %d\n",main_result);
  if (main_result == 20) {
    printf("RESULT: PASS\n");
  }
   else {
    printf("RESULT: FAIL\n");
  }
  return main_result;
}
