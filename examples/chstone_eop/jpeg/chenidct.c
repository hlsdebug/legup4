/*
+--------------------------------------------------------------------------+
| CHStone : A suite of Benchmark Programs for C-based High-Level Synthesis |
| ======================================================================== |
|                                                                          |
| * Collected and Modified : Y. Hara, H. Tomiyama, S. Honda,               |
|                            H. Takada and K. Ishii                        |
|                            Nagoya University, Japan                      |
|                                                                          |
| * Remarks :                                                              |
|    1. This source code is reformatted to follow CHStone's style.         |
|    2. Test vectors are added for CHStone.                                |
|    3. If "main_result" is 0 at the end of the program, the program is    |
|       successfully executed.                                             |
|    4. Follow the copyright of each benchmark program.                    |
+--------------------------------------------------------------------------+
*/
/*
 *  IDCT transformation of Chen algorithm
 *
 *  @(#) $Id: chenidct.c,v 1.2 2003/07/18 10:19:21 honda Exp $
 */
/*************************************************************
Copyright (C) 1990, 1991, 1993 Andy C. Hung, all rights reserved.
PUBLIC DOMAIN LICENSE: Stanford University Portable Video Research
Group. If you use this software, you agree to the following: This
program package is purely experimental, and is licensed "as is".
Permission is granted to use, modify, and distribute this program
without charge for any purpose, provided this license/ disclaimer
notice appears in the copies.  No warranty or maintenance is given,
either expressed or implied.  In no event shall the author(s) be
liable to you or a third party for any special, incidental,
consequential, or other damages, arising out of the use or inability
to use the program for any purpose (or the loss of data), even if we
have been advised of such possibilities.  Any public reference or
advertisement of this source code should refer to it as the Portable
Video Research Group (PVRG) code, and not by any author(s) (or
Stanford University) name.
*************************************************************/
/*
************************************************************
chendct.c
A simple DCT algorithm that seems to have fairly nice arithmetic
properties.
W. H. Chen, C. H. Smith and S. C. Fralick "A fast computational
algorithm for the discrete cosine transform," IEEE Trans. Commun.,
vol. COM-25, pp. 1004-1009, Sept 1977.
************************************************************
*/
#define LS(r,s) ((r) << (s))
#define RS(r,s) ((r) >> (s))       /* Caution with rounding... */
#define MSCALE(expr)  RS((expr),9)
/* Cos constants */
#define c1d4 362L
#define c1d8 473L
#define c3d8 196L
#define c1d16 502L
#define c3d16 426L
#define c5d16 284L
#define c7d16 100L
/*
 *
 * ChenIDCT() implements the Chen inverse dct. Note that there are two
 * input vectors that represent x=input, and y=output, and must be
 * defined (and storage allocated) before this routine is called.
 */

void ChenIDct(int *x,int *y)
{
  register int i;
  register int *aptr;
  register int a0;
  register int a1;
  register int a2;
  register int a3;
  register int b0;
  register int b1;
  register int b2;
  register int b3;
  register int c0;
  register int c1;
  register int c2;
  register int c3;
/* Loop over columns */
  for (i = 0; i < 8; i++) {
    aptr = x + i;
    b0 =  *aptr << 2;
    eop_83_b0_91 = b0;
    aptr += 8;
    a0 =  *aptr << 2;
    eop_82_a0_93 = a0;
    aptr += 8;
    b2 =  *aptr << 2;
    eop_83_b2_95 = b2;
    aptr += 8;
    a1 =  *aptr << 2;
    eop_82_a1_97 = a1;
    aptr += 8;
    b1 =  *aptr << 2;
    eop_83_b1_99 = b1;
    aptr += 8;
    a2 =  *aptr << 2;
    eop_82_a2_101 = a2;
    aptr += 8;
    b3 =  *aptr << 2;
    eop_83_b3_103 = b3;
    aptr += 8;
    a3 =  *aptr << 2;
    eop_82_a3_105 = a3;
/* Split into even mode  b0 = x0  b1 = x4  b2 = x2  b3 = x6.
	 And the odd terms a0 = x1 a1 = x3 a2 = x5 a3 = x7.
	 */
    c0 = (100L * a0 - 502L * a3 >> 9);
    eop_84_c0_111 = c0;
    c1 = (426L * a2 - 284L * a1 >> 9);
    eop_84_c1_112 = c1;
    c2 = (426L * a1 + 284L * a2 >> 9);
    eop_84_c2_113 = c2;
    c3 = (502L * a0 + 100L * a3 >> 9);
    eop_84_c3_114 = c3;
/* First Butterfly on even terms.*/
    a0 = (362L * (b0 + b1) >> 9);
    eop_82_a0_118 = a0;
    a1 = (362L * (b0 - b1) >> 9);
    eop_82_a1_119 = a1;
    a2 = (196L * b2 - 473L * b3 >> 9);
    eop_82_a2_121 = a2;
    a3 = (473L * b2 + 196L * b3 >> 9);
    eop_82_a3_122 = a3;
    b0 = a0 + a3;
    eop_83_b0_124 = b0;
    b1 = a1 + a2;
    eop_83_b1_125 = b1;
    b2 = a1 - a2;
    eop_83_b2_126 = b2;
    b3 = a0 - a3;
    eop_83_b3_127 = b3;
/* Second Butterfly */
    a0 = c0 + c1;
    eop_82_a0_131 = a0;
    a1 = c0 - c1;
    eop_82_a1_132 = a1;
    a2 = c3 - c2;
    eop_82_a2_133 = a2;
    a3 = c3 + c2;
    eop_82_a3_134 = a3;
    c0 = a0;
    eop_84_c0_136 = c0;
    c1 = (362L * (a2 - a1) >> 9);
    eop_84_c1_137 = c1;
    c2 = (362L * (a2 + a1) >> 9);
    eop_84_c2_138 = c2;
    c3 = a3;
    eop_84_c3_139 = c3;
    aptr = y + i;
     *aptr = b0 + c3;
    aptr += 8;
     *aptr = b1 + c2;
    aptr += 8;
     *aptr = b2 + c1;
    aptr += 8;
     *aptr = b3 + c0;
    aptr += 8;
     *aptr = b3 - c0;
    aptr += 8;
     *aptr = b2 - c1;
    aptr += 8;
     *aptr = b1 - c2;
    aptr += 8;
     *aptr = b0 - c3;
  }
/* Loop over rows */
  for (i = 0; i < 8; i++) {
    aptr = y + (i << 3);
    b0 =  *(aptr++);
    eop_83_b0_164 = b0;
    a0 =  *(aptr++);
    eop_82_a0_165 = a0;
    b2 =  *(aptr++);
    eop_83_b2_166 = b2;
    a1 =  *(aptr++);
    eop_82_a1_167 = a1;
    b1 =  *(aptr++);
    eop_83_b1_168 = b1;
    a2 =  *(aptr++);
    eop_82_a2_169 = a2;
    b3 =  *(aptr++);
    eop_83_b3_170 = b3;
    a3 =  *aptr;
    eop_82_a3_171 = a3;
/*
	Split into even mode  b0 = x0  b1 = x4  b2 = x2  b3 = x6.
	And the odd terms a0 = x1 a1 = x3 a2 = x5 a3 = x7.
	*/
    c0 = (100L * a0 - 502L * a3 >> 9);
    eop_84_c0_178 = c0;
    c1 = (426L * a2 - 284L * a1 >> 9);
    eop_84_c1_179 = c1;
    c2 = (426L * a1 + 284L * a2 >> 9);
    eop_84_c2_180 = c2;
    c3 = (502L * a0 + 100L * a3 >> 9);
    eop_84_c3_181 = c3;
/* First Butterfly on even terms.*/
    a0 = (362L * (b0 + b1) >> 9);
    eop_82_a0_185 = a0;
    a1 = (362L * (b0 - b1) >> 9);
    eop_82_a1_186 = a1;
    a2 = (196L * b2 - 473L * b3 >> 9);
    eop_82_a2_188 = a2;
    a3 = (473L * b2 + 196L * b3 >> 9);
    eop_82_a3_189 = a3;
/* Calculate last set of b's */
    b0 = a0 + a3;
    eop_83_b0_193 = b0;
    b1 = a1 + a2;
    eop_83_b1_194 = b1;
    b2 = a1 - a2;
    eop_83_b2_195 = b2;
    b3 = a0 - a3;
    eop_83_b3_196 = b3;
/* Second Butterfly */
    a0 = c0 + c1;
    eop_82_a0_200 = a0;
    a1 = c0 - c1;
    eop_82_a1_201 = a1;
    a2 = c3 - c2;
    eop_82_a2_202 = a2;
    a3 = c3 + c2;
    eop_82_a3_203 = a3;
    c0 = a0;
    eop_84_c0_205 = c0;
    c1 = (362L * (a2 - a1) >> 9);
    eop_84_c1_206 = c1;
    c2 = (362L * (a2 + a1) >> 9);
    eop_84_c2_207 = c2;
    c3 = a3;
    eop_84_c3_208 = c3;
    aptr = y + (i << 3);
     *(aptr++) = b0 + c3;
     *(aptr++) = b1 + c2;
     *(aptr++) = b2 + c1;
     *(aptr++) = b3 + c0;
     *(aptr++) = b3 - c0;
     *(aptr++) = b2 - c1;
     *(aptr++) = b1 - c2;
     *aptr = b0 - c3;
  }
/*
    Retrieve correct accuracy. We have additional factor
    of 16 that must be removed.
   */
  for ((i = 0 , aptr = y), eop_80_i_226 = 0; i < 64; (i++ , aptr++)) 
     *aptr = (( *aptr < 0? *aptr - 8 :  *aptr + 8)) / 16;
  eop_80_i_226 = i;
}
/*END*/
