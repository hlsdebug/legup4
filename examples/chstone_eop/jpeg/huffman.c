/*
+--------------------------------------------------------------------------+
| CHStone : a suite of benchmark programs for C-based High-Level Synthesis |
| ======================================================================== |
|                                                                          |
| * Collected and Modified : Y. Hara, H. Tomiyama, S. Honda,               |
|                            H. Takada and K. Ishii                        |
|                            Nagoya University, Japan                      |
|                                                                          |
| * Remark :                                                               |
|    1. This source code is modified to unify the formats of the benchmark |
|       programs in CHStone.                                               |
|    2. Test vectors are added for CHStone.                                |
|    3. If "main_result" is 0 at the end of the program, the program is    |
|       correctly executed.                                                |
|    4. Please follow the copyright of each benchmark program.             |
+--------------------------------------------------------------------------+
*/
/*
 *  Huffman decoding module
 *
 *  @(#) $Id: huffman.c,v 1.2 2003/07/18 10:19:21 honda Exp $
 */
/*************************************************************
Copyright (C) 1990, 1991, 1993 Andy C. Hung, all rights reserved.
PUBLIC DOMAIN LICENSE: Stanford University Portable Video Research
Group. If you use this software, you agree to the following: This
program package is purely experimental, and is licensed "as is".
Permission is granted to use, modify, and distribute this program
without charge for any purpose, provided this license/ disclaimer
notice appears in the copies.  No warranty or maintenance is given,
either expressed or implied.  In no event shall the author(s) be
liable to you or a third party for any special, incidental,
consequential, or other damages, arising out of the use or inability
to use the program for any purpose (or the loss of data), even if we
have been advised of such possibilities.  Any public reference or
advertisement of this source code should refer to it as the Portable
Video Research Group (PVRG) code, and not by any author(s) (or
Stanford University) name.
*************************************************************/
/*
************************************************************
huffman.c
This file represents the core Huffman routines, most of them
implemented with the JPEG reference. These routines are not very fast
and can be improved, but comprise very little of software run-time.
************************************************************
*/
#include "decode.h"
#include "huffman.h"
#include "init.h"
/* Used for sign extensions. */
static const int extend_mask[20] = {(0xFFFFFFFE), (0xFFFFFFFC), (0xFFFFFFF8), (0xFFFFFFF0), (0xFFFFFFE0), (0xFFFFFFC0), (0xFFFFFF80), (0xFFFFFF00), (0xFFFFFE00), (0xFFFFFC00), (0xFFFFF800), (0xFFFFF000), (0xFFFFE000), (0xFFFFC000), (0xFFFF8000), (0xFFFF0000), (0xFFFE0000), (0xFFFC0000), (0xFFF80000), (0xFFF00000)};
/* Masks */
/* This is 2^i at ith position */
const int bit_set_mask[32] = {(0x00000001), (0x00000002), (0x00000004), (0x00000008), (0x00000010), (0x00000020), (0x00000040), (0x00000080), (0x00000100), (0x00000200), (0x00000400), (0x00000800), (0x00001000), (0x00002000), (0x00004000), (0x00008000), (0x00010000), (0x00020000), (0x00040000), (0x00080000), (0x00100000), (0x00200000), (0x00400000), (0x00800000), (0x01000000), (0x02000000), (0x04000000), (0x08000000), (0x10000000), (0x20000000), (0x40000000), (0x80000000)};
/* This is 2^{i+1}-1 */
const int lmask[32] = {(0x00000001), (0x00000003), (0x00000007), (0x0000000f), (0x0000001f), (0x0000003f), (0x0000007f), (0x000000ff), (0x000001ff), (0x000003ff), (0x000007ff), (0x00000fff), (0x00001fff), (0x00003fff), (0x00007fff), (0x0000ffff), (0x0001ffff), (0x0003ffff), (0x0007ffff), (0x000fffff), (0x001fffff), (0x003fffff), (0x007fffff), (0x00ffffff), (0x01ffffff), (0x03ffffff), (0x07ffffff), (0x0fffffff), (0x1fffffff), (0x3fffffff), (0x7fffffff), (0xffffffff)};
static unsigned int current_read_byte;
static int read_position;
/*
 * pgetc() gets a character onto the stream but it checks to see
 * if there are any marker conflicts.
 */

static int pgetc()
{
  int temp;
/* If MARKER then */
  if ((temp = ( *(CurHuffReadBuf++))) == 0xff) {
/* if next is not 0xff, then marker */
    if (temp = ( *(CurHuffReadBuf++))) {
      printf("Unanticipated marker detected.\n");
    }
     else {
      return 0xff;
    }
    eop_98_temp_101 = temp;
  }
  eop_98_temp_100 = temp;
  return temp;
}
/*
 * buf_getb() gets a bit from the read stream.
 */

int buf_getb()
{
  if (read_position < 0) {
    current_read_byte = (pgetc());
    eop_88_current_read_byte_118 = current_read_byte;
    read_position = 7;
    eop_89_read_position_119 = 7;
  }
  if (current_read_byte & bit_set_mask[read_position--]) {
    return 1;
  }
  eop_89_read_position_122 = read_position;
  return 0;
}
/*
 * megetv() gets n bits from the read stream and returns it.
 *
 */

int buf_getv(int n)
{
  int p;
  int rv;
  n--;
  eop_134_n_137 = n;
  p = n - read_position;
  eop_135_p_138 = p;
  while(p > 0){
/* If byte buffer contains almost entire word */
    if (read_position > 23) {
/* Manipulate buffer */
      rv = (current_read_byte << p);
      eop_135_rv_141 = rv;
/* Change read bytes */
      current_read_byte = (pgetc());
      eop_88_current_read_byte_142 = current_read_byte;
      rv |= current_read_byte >> 8 - p;
      read_position = 7 - p;
      eop_89_read_position_145 = read_position;
      return rv & lmask[n];
/* Can return pending residual val */
    }
    current_read_byte = current_read_byte << 8 | (pgetc());
    eop_88_current_read_byte_149 = current_read_byte;
/* else shift in new information */
    read_position += 8;
    eop_89_read_position_150 = read_position;
    p -= 8;
  }
/* If position is zero */
  if (!p) {
    read_position = - 1;
    eop_89_read_position_155 = read_position;
/* Can return current byte */
    return (current_read_byte & lmask[n]);
  }
  p = -p;
  eop_135_p_160 = p;
/* Else reverse position and shift */
  read_position = p - 1;
  eop_89_read_position_162 = read_position;
  return (current_read_byte >> p & lmask[n]);
}
/*
 * Create Table for decoding
 */

int huff_make_dhuff_tb(int *p_xhtbl_bits,int *p_dhtbl_maxcode,int *p_dhtbl_mincode,int *p_dhtbl_valptr)
{
  int i;
  int j;
  int p;
  int code;
  int size;
  int l;
  int huffsize[257];
  int huffcode[257];
  int lastp;
  int p_dhtbl_ml;
/*
     * Get size
     */
  for ((p = 0 , i = 1), eop_174_i_183 = 1, eop_174_p_183 = 0; i < 17; i++) {
    for (j = 1; j <= p_xhtbl_bits[i]; j++) {
      huffsize[p++] = i;
      eop_174_p_185 = p;
    }
  }
  huffsize[p] = 0;
  lastp = p;
  eop_177_lastp_190 = lastp;
  p = 0;
  eop_174_p_192 = 0;
  code = 0;
  eop_174_code_193 = 0;
  size = huffsize[0];
  eop_174_size_194 = size;
  while(1){
    do {
      huffcode[p++] = code++;
      eop_174_code_197 = code;
      eop_174_p_197 = p;
    }while (huffsize[p] == size && p < 257);
/* Overflow Detection */
/* All finished. */
    if (!huffsize[p]) {
      break; 
    }
    do {
/* Shift next code to expand prefix. */
      code <<= 1;
      size++;
      eop_174_size_206 = size;
    }while (huffsize[p] != size);
  }
  for (((p_dhtbl_ml = 1 , p = 0) , l = 1), eop_174_l_210 = 1, eop_174_p_210 = 0, eop_178_p_dhtbl_ml_210 = 1; l <= 16; l++) {
    if (p_xhtbl_bits[l] == 0) {
/* Watch out JPEG is wrong here */
      p_dhtbl_maxcode[l] = - 1;
/* We use -1 to indicate skipping. */
    }
     else {
      p_dhtbl_valptr[l] = p;
      p_dhtbl_mincode[l] = huffcode[p];
      p += p_xhtbl_bits[l] - 1;
      eop_174_p_216 = p;
      p_dhtbl_maxcode[l] = huffcode[p];
      p_dhtbl_ml = l;
      eop_178_p_dhtbl_ml_218 = p_dhtbl_ml;
      p++;
      eop_174_p_219 = p;
    }
  }
  p_dhtbl_maxcode[p_dhtbl_ml]++;
  return p_dhtbl_ml;
}
/*
 *  
 */

int DecodeHuffman(int *Xhuff_huffval,int Dhuff_ml,int *Dhuff_maxcode,int *Dhuff_mincode,int *Dhuff_valptr)
{
  int code;
  int l;
  int p;
  code = buf_getb();
  eop_233_code_235 = code;
  for (l = 1; code > Dhuff_maxcode[l]; l++) {
    code = (code << 1) + buf_getb();
    eop_233_code_237 = code;
  }
  if (code < Dhuff_maxcode[Dhuff_ml]) {
    main_result++;
    eop_41_main_result_241 = main_result;
    p = Dhuff_valptr[l] + code - Dhuff_mincode[l];
    eop_233_p_242 = p;
    return Xhuff_huffval[p];
  }
   else {
    printf("Huffman read error\n");
    exit(0);
  }
}
/*
 * Decode one MCU
 */

void DecodeHuffMCU(int *out_buf,int num_cmp)
{
  int s;
  int diff;
  int tbl_no;
  int *mptr;
  int k;
  int n;
  int r;
/*
     * Decode DC
     */
  tbl_no = p_jinfo_comps_info_dc_tbl_no[num_cmp];
  eop_258_tbl_no_263 = tbl_no;
  s = DecodeHuffman(&p_jinfo_dc_xhuff_tbl_huffval[tbl_no][0],p_jinfo_dc_dhuff_tbl_ml[tbl_no],&p_jinfo_dc_dhuff_tbl_maxcode[tbl_no][0],&p_jinfo_dc_dhuff_tbl_mincode[tbl_no][0],&p_jinfo_dc_dhuff_tbl_valptr[tbl_no][0]);
  eop_258_s_264 = s;
  if (s) {
    diff = buf_getv(s);
    eop_258_diff_272 = diff;
    s--;
    eop_258_s_273 = s;
    if ((diff & bit_set_mask[s]) == 0) {
      diff |= extend_mask[s];
      diff++;
      eop_258_diff_276 = diff;
    }
/* Change the last DC */
    diff +=  *out_buf;
    eop_258_diff_279 = diff;
     *out_buf = diff;
  }
/*
     * Decode AC
     */
/* Set all values to zero */
  for (mptr = out_buf + 1; mptr < out_buf + 64; mptr++) {
     *mptr = 0;
  }
/* JPEG Mistake */
  for (k = 1; k < 64; ) {
    r = DecodeHuffman(&p_jinfo_ac_xhuff_tbl_huffval[tbl_no][0],p_jinfo_ac_dhuff_tbl_ml[tbl_no],&p_jinfo_ac_dhuff_tbl_maxcode[tbl_no][0],&p_jinfo_ac_dhuff_tbl_mincode[tbl_no][0],&p_jinfo_ac_dhuff_tbl_valptr[tbl_no][0]);
    eop_258_r_292 = r;
/* Find significant bits */
    s = r & 0xf;
    eop_258_s_299 = s;
/* n = run-length */
    n = r >> 4 & 0xf;
    eop_258_n_300 = n;
    if (s) {
/* JPEG Mistake */
      if ((k += n) >= 64) 
        break; 
      eop_258_k_303 = k;
/* Get s bits */
      out_buf[k] = buf_getv(s);
/* Align s */
      s--;
      eop_258_s_306 = s;
/* Also (1 << s) */
      if ((out_buf[k] & bit_set_mask[s]) == 0) {
/* Also  (-1 << s) + 1 */
        out_buf[k] |= extend_mask[s];
/* Increment 2's c */
        out_buf[k]++;
      }
/* Goto next element */
      k++;
      eop_258_k_311 = k;
/* Zero run length code extnd */
    }
     else if (n == 15) {
      k += 16;
      eop_258_k_313 = k;
    }
     else {
      break; 
    }
  }
}
