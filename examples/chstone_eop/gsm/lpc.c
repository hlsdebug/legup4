/*
+--------------------------------------------------------------------------+
| CHStone : a suite of benchmark programs for C-based High-Level Synthesis |
| ======================================================================== |
|                                                                          |
| * Collected and Modified : Y. Hara, H. Tomiyama, S. Honda,               |
|                            H. Takada and K. Ishii                        |
|                            Nagoya University, Japan                      |
|                                                                          |
| * Remark :                                                               |
|    1. This source code is modified to unify the formats of the benchmark |
|       programs in CHStone.                                               |
|    2. Test vectors are added for CHStone.                                |
|    3. If "main_result" is 0 at the end of the program, the program is    |
|       correctly executed.                                                |
|    4. Please follow the copyright of each benchmark program.             |
+--------------------------------------------------------------------------+
*/
/*
 * Copyright 1992 by Jutta Degener and Carsten Bormann, Technische
 * Universitaet Berlin.  See the accompanying file "COPYRIGHT" for
 * details.  THERE IS ABSOLUTELY NO WARRANTY FOR THIS SOFTWARE.
 */
/* $Header: /home/kbs/jutta/src/gsm/gsm-1.0/src/RCS/lpc.c,v 1.5 1994/12/30 23:14:54 jutta Exp $ */
#include "private.h"
#include "add.c"
/*
 *  4.2.4 .. 4.2.7 LPC ANALYSIS SECTION
 */
/* 4.2.4 */

void Autocorrelation(
/* [0..159]     IN/OUT  */
word *s,
/* [0..8]       OUT     */
longword *L_ACF)
/*
 *  The goal is to compute the array L_ACF[k].  The signal s[i] must
 *  be scaled in order to avoid an overflow situation.
 */
{
  register int k;
  register int i;
  word temp;
  word smax;
  word scalauto;
  word n;
  word *sp;
  word sl;
/*  Search for the maximum.
   */
  smax = 0;
  eop_49_smax_56 = smax;
  for (k = 0; k <= 159; k++) {
    temp = gsm_abs(s[k]);
    eop_48_temp_59 = temp;
    if (temp > smax) {
      smax = temp;
      eop_49_smax_61 = smax;
    }
  }
/*  Computation of the scaling factor.
   */
  if (smax == 0) {
    scalauto = 0;
    eop_50_scalauto_67 = scalauto;
  }
   else {
/* sub(4,..) */
    scalauto = (4 - (gsm_norm(((longword )smax) << 16)));
    eop_50_scalauto_69 = scalauto;
  }
  if (scalauto > 0 && scalauto <= 4) {
    n = scalauto;
    eop_50_n_73 = n;
    for (k = 0; k <= 159; k++) 
      s[k] = gsm_mult_r(s[k],(16384 >> n - 1));
  }
/*  Compute the L_ACF[..].
   */
{
    sp = s;
    sl =  *sp;
    eop_52_sl_82 = sl;
#define STEP(k)	 L_ACF[k] += ((longword)sl * sp[ -(k) ]);
#define NEXTI	 sl = *++sp
    for (k = 8; k >= 0; k--) 
      L_ACF[k] = 0;
    L_ACF[0] += ((longword )sl) * sp[- 0];
    ;
    sl =  *(++sp);
    eop_52_sl_91 = sl;
    L_ACF[0] += ((longword )sl) * sp[- 0];
    ;
    L_ACF[1] += ((longword )sl) * sp[- 1];
    ;
    sl =  *(++sp);
    eop_52_sl_94 = sl;
    L_ACF[0] += ((longword )sl) * sp[- 0];
    ;
    L_ACF[1] += ((longword )sl) * sp[- 1];
    ;
    L_ACF[2] += ((longword )sl) * sp[- 2];
    ;
    sl =  *(++sp);
    eop_52_sl_98 = sl;
    L_ACF[0] += ((longword )sl) * sp[- 0];
    ;
    L_ACF[1] += ((longword )sl) * sp[- 1];
    ;
    L_ACF[2] += ((longword )sl) * sp[- 2];
    ;
    L_ACF[3] += ((longword )sl) * sp[- 3];
    ;
    sl =  *(++sp);
    eop_52_sl_103 = sl;
    L_ACF[0] += ((longword )sl) * sp[- 0];
    ;
    L_ACF[1] += ((longword )sl) * sp[- 1];
    ;
    L_ACF[2] += ((longword )sl) * sp[- 2];
    ;
    L_ACF[3] += ((longword )sl) * sp[- 3];
    ;
    L_ACF[4] += ((longword )sl) * sp[- 4];
    ;
    sl =  *(++sp);
    eop_52_sl_109 = sl;
    L_ACF[0] += ((longword )sl) * sp[- 0];
    ;
    L_ACF[1] += ((longword )sl) * sp[- 1];
    ;
    L_ACF[2] += ((longword )sl) * sp[- 2];
    ;
    L_ACF[3] += ((longword )sl) * sp[- 3];
    ;
    L_ACF[4] += ((longword )sl) * sp[- 4];
    ;
    L_ACF[5] += ((longword )sl) * sp[- 5];
    ;
    sl =  *(++sp);
    eop_52_sl_116 = sl;
    L_ACF[0] += ((longword )sl) * sp[- 0];
    ;
    L_ACF[1] += ((longword )sl) * sp[- 1];
    ;
    L_ACF[2] += ((longword )sl) * sp[- 2];
    ;
    L_ACF[3] += ((longword )sl) * sp[- 3];
    ;
    L_ACF[4] += ((longword )sl) * sp[- 4];
    ;
    L_ACF[5] += ((longword )sl) * sp[- 5];
    ;
    L_ACF[6] += ((longword )sl) * sp[- 6];
    ;
    sl =  *(++sp);
    eop_52_sl_124 = sl;
    L_ACF[0] += ((longword )sl) * sp[- 0];
    ;
    L_ACF[1] += ((longword )sl) * sp[- 1];
    ;
    L_ACF[2] += ((longword )sl) * sp[- 2];
    ;
    L_ACF[3] += ((longword )sl) * sp[- 3];
    ;
    L_ACF[4] += ((longword )sl) * sp[- 4];
    ;
    L_ACF[5] += ((longword )sl) * sp[- 5];
    ;
    L_ACF[6] += ((longword )sl) * sp[- 6];
    ;
    L_ACF[7] += ((longword )sl) * sp[- 7];
    ;
    for (i = 8; i <= 159; i++) {
      sl =  *(++sp);
      eop_52_sl_137 = sl;
      L_ACF[0] += ((longword )sl) * sp[- 0];
      ;
      L_ACF[1] += ((longword )sl) * sp[- 1];
      ;
      L_ACF[2] += ((longword )sl) * sp[- 2];
      ;
      L_ACF[3] += ((longword )sl) * sp[- 3];
      ;
      L_ACF[4] += ((longword )sl) * sp[- 4];
      ;
      L_ACF[5] += ((longword )sl) * sp[- 5];
      ;
      L_ACF[6] += ((longword )sl) * sp[- 6];
      ;
      L_ACF[7] += ((longword )sl) * sp[- 7];
      ;
      L_ACF[8] += ((longword )sl) * sp[- 8];
      ;
    }
    for (k = 8; k >= 0; k--) 
      L_ACF[k] <<= 1;
  }
/*   Rescaling of the array s[0..159]
   */
  if (scalauto > 0) 
    for (k = 159; k >= 0; k--) 
       *(s++) <<= scalauto;
}
/* 4.2.5 */

void Reflection_coefficients(
/* 0...8        IN      */
longword *L_ACF,
/* 0...7        OUT     */
word *r)
{
  register int i;
  register int m;
  register int n;
  register word temp;
/* 0..8 */
  word ACF[9];
/* 0..8 */
  word P[9];
/* 2..8 */
  word K[9];
/*  Schur recursion with 16 bits arithmetic.
   */
  if (L_ACF[0] == 0) {
    for (i = 8; i > 0; i--) 
       *(r++) = 0;
    return ;
  }
  temp = gsm_norm(L_ACF[0]);
  eop_168_temp_183 = temp;
  for (i = 0; i <= 8; i++) 
    ACF[i] = (L_ACF[i] << temp >> 16);
/*   Initialize array P[..] and K[..] for the recursion.
   */
  for (i = 1; i <= 7; i++) 
    K[i] = ACF[i];
  for (i = 0; i <= 8; i++) 
    P[i] = ACF[i];
/*   Compute reflection coefficients
   */
  for (n = 1; n <= 8; (n++ , r++)) {
    temp = P[1];
    eop_168_temp_200 = temp;
    temp = gsm_abs(temp);
    eop_168_temp_201 = temp;
    if (P[0] < temp) {
      for (i = n; i <= 8; i++) 
         *(r++) = 0;
      return ;
    }
     *r = gsm_div(temp,P[0]);
    if (P[1] > 0) 
/* r[n] = sub(0, r[n]) */
       *r = (-( *r));
    if (n == 8) 
      return ;
/*  Schur recursion
       */
    temp = gsm_mult_r(P[1], *r);
    eop_168_temp_218 = temp;
    P[0] = gsm_add(P[0],temp);
    for (m = 1; m <= 8 - n; m++) {
      temp = gsm_mult_r(K[m], *r);
      eop_168_temp_223 = temp;
      P[m] = gsm_add(P[m + 1],temp);
      temp = gsm_mult_r(P[m + 1], *r);
      eop_168_temp_226 = temp;
      K[m] = gsm_add(K[m],temp);
    }
  }
  eop_167_n_197 = n;
}
/* 4.2.6 */

void Transformation_to_Log_Area_Ratios(
/* 0..7    IN/OUT */
word *r)
/*
 *  The following scaling for r[..] and LAR[..] has been used:
 *
 *  r[..]   = integer( real_r[..]*32768. ); -1 <= real_r < 1.
 *  LAR[..] = integer( real_LAR[..] * 16384 );
 *  with -1.625 <= real_LAR <= 1.625
 */
{
  register word temp;
  register int i;
/* Computation of the LAR[0..7] from the r[0..7]
   */
  for (i = 1; i <= 8; (i++ , r++)) {
    temp =  *r;
    eop_244_temp_253 = temp;
    temp = gsm_abs(temp);
    eop_244_temp_254 = temp;
    if (temp < 22118) {
      temp >>= 1;
    }
     else if (temp < 31130) {
      temp -= 11059;
    }
     else {
      temp -= 26112;
      temp <<= 2;
    }
     *r = ((( *r) < 0?-temp : temp));
  }
  eop_245_i_250 = i;
}
/* 4.2.7 */

void Quantization_and_coding(
/* [0..7]       IN/OUT  */
word *LAR)
{
  register word temp;
/*  This procedure needs four tables; the following equations
   *  give the optimum scaling for the constants:
   *  
   *  A[0..7] = integer( real_A[0..7] * 1024 )
   *  B[0..7] = integer( real_B[0..7] *  512 )
   *  MAC[0..7] = maximum of the LARc[0..7]
   *  MIC[0..7] = minimum of the LARc[0..7]
   */
#	undef STEP
#	define	STEP( A, B, MAC, MIC )		\
		temp = GSM_MULT( A,   *LAR );	\
		temp = GSM_ADD(  temp,   B );	\
		temp = GSM_ADD(  temp, 256 );	\
		temp = SASR(     temp,   9 );	\
		*LAR  =  temp>MAC ? MAC - MIC : (temp<MIC ? 0 : temp - MIC); \
		LAR++;
  temp = gsm_mult(20480, *LAR);
  eop_279_temp_300 = temp;
  temp = gsm_add(temp,0);
  eop_279_temp_300 = temp;
  temp = gsm_add(temp,256);
  eop_279_temp_300 = temp;
  temp = (temp >> 9);
  eop_279_temp_300 = temp;
   *LAR = ((temp > 31?31 - - 32 : ((temp < - 32?0 : temp - - 32))));
  LAR++;
  ;
  temp = gsm_mult(20480, *LAR);
  eop_279_temp_301 = temp;
  temp = gsm_add(temp,0);
  eop_279_temp_301 = temp;
  temp = gsm_add(temp,256);
  eop_279_temp_301 = temp;
  temp = (temp >> 9);
  eop_279_temp_301 = temp;
   *LAR = ((temp > 31?31 - - 32 : ((temp < - 32?0 : temp - - 32))));
  LAR++;
  ;
  temp = gsm_mult(20480, *LAR);
  eop_279_temp_302 = temp;
  temp = gsm_add(temp,2048);
  eop_279_temp_302 = temp;
  temp = gsm_add(temp,256);
  eop_279_temp_302 = temp;
  temp = (temp >> 9);
  eop_279_temp_302 = temp;
   *LAR = ((temp > 15?15 - - 16 : ((temp < - 16?0 : temp - - 16))));
  LAR++;
  ;
  temp = gsm_mult(20480, *LAR);
  eop_279_temp_303 = temp;
  temp = gsm_add(temp,(- 2560));
  eop_279_temp_303 = temp;
  temp = gsm_add(temp,256);
  eop_279_temp_303 = temp;
  temp = (temp >> 9);
  eop_279_temp_303 = temp;
   *LAR = ((temp > 15?15 - - 16 : ((temp < - 16?0 : temp - - 16))));
  LAR++;
  ;
  temp = gsm_mult(13964, *LAR);
  eop_279_temp_305 = temp;
  temp = gsm_add(temp,94);
  eop_279_temp_305 = temp;
  temp = gsm_add(temp,256);
  eop_279_temp_305 = temp;
  temp = (temp >> 9);
  eop_279_temp_305 = temp;
   *LAR = ((temp > 7?7 - - 8 : ((temp < - 8?0 : temp - - 8))));
  LAR++;
  ;
  temp = gsm_mult(15360, *LAR);
  eop_279_temp_306 = temp;
  temp = gsm_add(temp,(- 1792));
  eop_279_temp_306 = temp;
  temp = gsm_add(temp,256);
  eop_279_temp_306 = temp;
  temp = (temp >> 9);
  eop_279_temp_306 = temp;
   *LAR = ((temp > 7?7 - - 8 : ((temp < - 8?0 : temp - - 8))));
  LAR++;
  ;
  temp = gsm_mult(8534, *LAR);
  eop_279_temp_307 = temp;
  temp = gsm_add(temp,(- 341));
  eop_279_temp_307 = temp;
  temp = gsm_add(temp,256);
  eop_279_temp_307 = temp;
  temp = (temp >> 9);
  eop_279_temp_307 = temp;
   *LAR = ((temp > 3?3 - - 4 : ((temp < - 4?0 : temp - - 4))));
  LAR++;
  ;
  temp = gsm_mult(9036, *LAR);
  eop_279_temp_308 = temp;
  temp = gsm_add(temp,(- 1144));
  eop_279_temp_308 = temp;
  temp = gsm_add(temp,256);
  eop_279_temp_308 = temp;
  temp = (temp >> 9);
  eop_279_temp_308 = temp;
   *LAR = ((temp > 3?3 - - 4 : ((temp < - 4?0 : temp - - 4))));
  LAR++;
  ;
#	undef	STEP
}

void Gsm_LPC_Analysis(
/* 0..159 signals       IN/OUT  */
word *s,
/* 0..7   LARc's        OUT     */
word *LARc)
{
  longword L_ACF[9];
  Autocorrelation(s,L_ACF);
  Reflection_coefficients(L_ACF,LARc);
  Transformation_to_Log_Area_Ratios(LARc);
  Quantization_and_coding(LARc);
}
