#include "milieu.h"
#include "softfloat.h"
volatile int16 eop_328_zExp_395;
volatile int16 eop_330_expDiff_386;
volatile int16 eop_330_expDiff_368;
volatile int16 eop_247_zExp_310;
volatile int16 eop_247_zExp_306;
volatile int16 eop_249_expDiff_282;
volatile int16 eop_249_expDiff_267;
volatile int eop_199_i_202;
volatile int16 eop_328_zExp_395;
volatile int16 eop_330_expDiff_386;
volatile int16 eop_330_expDiff_368;
volatile int16 eop_247_zExp_310;
volatile int16 eop_247_zExp_306;
volatile int16 eop_249_expDiff_282;
volatile int16 eop_249_expDiff_267;
volatile flag eop_409_bSign_412;
volatile flag eop_409_aSign_411;
volatile int16 eop_328_zExp_393;
volatile bits64 eop_329_zSig_392;
volatile int16 eop_328_zExp_375;
volatile bits64 eop_329_zSig_374;
volatile int16 eop_328_bExp_353;
volatile int16 eop_328_aExp_352;
volatile int16 eop_330_expDiff_336;
volatile int16 eop_328_bExp_335;
volatile bits64 eop_329_bSig_334;
volatile int16 eop_328_aExp_333;
volatile bits64 eop_329_aSig_332;
volatile bits64 eop_248_zSig_309;
volatile bits64 eop_248_zSig_305;
volatile int16 eop_247_zExp_301;
volatile bits64 eop_248_zSig_300;
volatile int16 eop_247_zExp_288;
volatile int16 eop_247_zExp_271;
volatile int16 eop_249_expDiff_255;
volatile int16 eop_247_bExp_254;
volatile bits64 eop_248_bSig_253;
volatile int16 eop_247_aExp_252;
volatile bits64 eop_248_aSig_251;
volatile int8 eop_229_shiftCount_231;
volatile int16 eop_156_zExp_212;
volatile bits64 eop_156_zSig_209;
volatile int16 eop_160_roundBits_202;
volatile int16 eop_156_zExp_201;
volatile flag eop_159_isTiny_197;
volatile int16 eop_160_roundBits_186;
volatile int16 eop_160_roundIncrement_182;
volatile int16 eop_160_roundIncrement_177;
volatile int16 eop_160_roundIncrement_173;
volatile int16 eop_160_roundIncrement_169;
volatile int16 eop_160_roundIncrement_164;
volatile flag eop_159_roundNearestEven_163;
volatile int8 eop_158_roundingMode_162;
volatile flag eop_111_bIsSignalingNaN_116;
volatile flag eop_111_bIsNaN_115;
volatile flag eop_111_aIsSignalingNaN_114;
volatile flag eop_111_aIsNaN_113;
volatile int8 eop_132_shiftCount_143;
volatile int8 eop_132_shiftCount_137;
volatile int8 eop_132_shiftCount_134;
volatile int8 eop_106_shiftCount_119;
volatile int8 eop_106_shiftCount_116;
volatile int8 eop_106_shiftCount_111;
volatile int8 eop_106_shiftCount_108;
volatile bits64 eop_62_z_74;
volatile bits64 eop_62_z_70;
volatile bits64 eop_62_z_66;
volatile int eop_198_main_result_208;
volatile float64 eop_204_result_207;
volatile float64 eop_200_x2_206;
volatile float64 eop_200_x1_205;
volatile int eop_199_i_202;
volatile int eop_198_main_result_201;
volatile flag eop_409_bSign_412;
volatile flag eop_409_aSign_411;
volatile int16 eop_328_zExp_393;
volatile bits64 eop_329_zSig_392;
volatile int16 eop_328_zExp_375;
volatile bits64 eop_329_zSig_374;
volatile int16 eop_328_bExp_353;
volatile int16 eop_328_aExp_352;
volatile int16 eop_330_expDiff_336;
volatile int16 eop_328_bExp_335;
volatile bits64 eop_329_bSig_334;
volatile int16 eop_328_aExp_333;
volatile bits64 eop_329_aSig_332;
volatile bits64 eop_248_zSig_309;
volatile bits64 eop_248_zSig_305;
volatile int16 eop_247_zExp_301;
volatile bits64 eop_248_zSig_300;
volatile int16 eop_247_zExp_288;
volatile int16 eop_247_zExp_271;
volatile int16 eop_249_expDiff_255;
volatile int16 eop_247_bExp_254;
volatile bits64 eop_248_bSig_253;
volatile int16 eop_247_aExp_252;
volatile bits64 eop_248_aSig_251;
volatile int8 eop_229_shiftCount_231;
volatile int16 eop_156_zExp_212;
volatile bits64 eop_156_zSig_209;
volatile int16 eop_160_roundBits_202;
volatile int16 eop_156_zExp_201;
volatile flag eop_159_isTiny_197;
volatile int16 eop_160_roundBits_186;
volatile int16 eop_160_roundIncrement_182;
volatile int16 eop_160_roundIncrement_177;
volatile int16 eop_160_roundIncrement_173;
volatile int16 eop_160_roundIncrement_169;
volatile int16 eop_160_roundIncrement_164;
volatile flag eop_159_roundNearestEven_163;
volatile int8 eop_158_roundingMode_162;
volatile flag eop_111_bIsSignalingNaN_116;
volatile flag eop_111_bIsNaN_115;
volatile flag eop_111_aIsSignalingNaN_114;
volatile flag eop_111_aIsNaN_113;
volatile int8 eop_132_shiftCount_143;
volatile int8 eop_132_shiftCount_137;
volatile int8 eop_132_shiftCount_134;
volatile int8 eop_106_shiftCount_119;
volatile int8 eop_106_shiftCount_116;
volatile int8 eop_106_shiftCount_111;
volatile int8 eop_106_shiftCount_108;
volatile bits64 eop_62_z_74;
volatile bits64 eop_62_z_70;
volatile bits64 eop_62_z_66;
/*
+--------------------------------------------------------------------------+
| CHStone : a suite of benchmark programs for C-based High-Level Synthesis |
| ======================================================================== |
|                                                                          |
| * Collected and Modified : Y. Hara, H. Tomiyama, S. Honda,               |
|                            H. Takada and K. Ishii                        |
|                            Nagoya University, Japan                      |
|                                                                          |
| * Remark :                                                               |
|    1. This source code is modified to unify the formats of the benchmark |
|       programs in CHStone.                                               |
|    2. Test vectors are added for CHStone.                                |
|    3. If "main_result" is 0 at the end of the program, the program is    |
|       correctly executed.                                                |
|    4. Please follow the copyright of each benchmark program.             |
+--------------------------------------------------------------------------+
*/
/*
 * Copyright (C) 2008
 * Y. Hara, H. Tomiyama, S. Honda, H. Takada and K. Ishii
 * Nagoya University, Japan
 * All rights reserved.
 *
 * Disclaimer of Warranty
 *
 * These software programs are available to the user without any license fee or
 * royalty on an "as is" basis. The authors disclaims any and all warranties, 
 * whether express, implied, or statuary, including any implied warranties or 
 * merchantability or of fitness for a particular purpose. In no event shall the
 * copyright-holder be liable for any incidental, punitive, or consequential damages
 * of any kind whatsoever arising from the use of these programs. This disclaimer
 * of warranty extends to the user of these programs and user's customers, employees,
 * agents, transferees, successors, and assigns.
 *
 */
#include <stdio.h>
#include "softfloat.c"
/*
+--------------------------------------------------------------------------+
| * Test Vectors (added for CHStone)                                       |
|     a_input, b_input : input data                                        |
|     z_output : expected output data                                      |
+--------------------------------------------------------------------------+
*/
#define N 46
const float64 a_input[46] = {(0x7FF8000000000000ULL), (0x7FF0000000000000ULL), (0x4000000000000000ULL), (0x4000000000000000ULL), (0x3FF0000000000000ULL), (0x3FF0000000000000ULL), (0x0000000000000000ULL), (0x3FF8000000000000ULL), (0x7FF8000000000000ULL), (0x7FF0000000000000ULL), (0x0000000000000000ULL), (0x3FF8000000000000ULL), (0xFFF8000000000000ULL), (0xFFF0000000000000ULL), (0xC000000000000000ULL), (0xC000000000000000ULL), (0xBFF0000000000000ULL), (0xBFF0000000000000ULL), (0x8000000000000000ULL), (0xBFF8000000000000ULL), (0xFFF8000000000000ULL), (0xFFF0000000000000ULL), (0x8000000000000000ULL), (0xBFF8000000000000ULL), (0x7FF8000000000000ULL), (0x7FF0000000000000ULL), (0x3FF0000000000000ULL), (0x3FF0000000000000ULL), (0x3FF0000000000000ULL), (0x0000000000000000ULL), (0x3FF8000000000000ULL), (0x7FF8000000000000ULL), (0x7FF0000000000000ULL), (0x3FF0000000000000ULL), (0x4000000000000000ULL), (0xFFF0000000000000ULL), (0xFFF0000000000000ULL), (0xBFF0000000000000ULL), (0xBFF0000000000000ULL), (0xBFF0000000000000ULL), (0x8000000000000000ULL), (0xBFF8000000000000ULL), (0xFFF8000000000000ULL), (0xFFF0000000000000ULL), (0xBFF0000000000000ULL), (0xC000000000000000ULL)
/* nan */
/* inf */
/* 2.0 */
/* 2.0 */
/* 1.0 */
/* 1.0 */
/* 0.0 */
/* 1.5 */
/* nan */
/* inf */
/* 0.0 */
/* 1.5 */
/* nan */
/* -inf */
/* -2.0 */
/* -2.0 */
/* -1.0 */
/* -1.0 */
/* -0.0 */
/* -1.5 */
/* nan */
/* -inf */
/* -0.0 */
/* -1.5 */
/* nan */
/* inf */
/* 1.0 */
/* 1.0 */
/* 1.0 */
/* 0.0 */
/* 1.5 */
/* nan */
/* inf */
/* 1.0 */
/* 2.0 */
/* -inf */
/* -inf */
/* -1.0 */
/* -1.0 */
/* -1.0 */
/* -0.0 */
/* -1.5 */
/* nan */
/* -inf */
/* -1.0 */
/* -2.0 */
};
const float64 b_input[46] = {(0x3FF0000000000000ULL), (0x3FF0000000000000ULL), (0x0000000000000000ULL), (0x3FF8000000000000ULL), (0x7FF8000000000000ULL), (0x7FF0000000000000ULL), (0x4000000000000000ULL), (0x4000000000000000ULL), (0x7FF0000000000000ULL), (0x7FF0000000000000ULL), (0x0000000000000000ULL), (0x3FF0000000000000ULL), (0xBFF0000000000000ULL), (0xBFF0000000000000ULL), (0x8000000000000000ULL), (0xBFF8000000000000ULL), (0xFFF8000000000000ULL), (0xFFF0000000000000ULL), (0xC000000000000000ULL), (0xC000000000000000ULL), (0xFFF0000000000000ULL), (0xFFF0000000000000ULL), (0x8000000000000000ULL), (0xBFF0000000000000ULL), (0xFFF0000000000000ULL), (0xFFF0000000000000ULL), (0xBFF0000000000000ULL), (0xFFF8000000000000ULL), (0xFFF0000000000000ULL), (0xBFF0000000000000ULL), (0xC000000000000000ULL), (0xBFF0000000000000ULL), (0xBFF0000000000000ULL), (0x8000000000000000ULL), (0xBFF8000000000000ULL), (0x7FF8000000000000ULL), (0x7FF0000000000000ULL), (0x3FF0000000000000ULL), (0x7FF8000000000000ULL), (0x7FF0000000000000ULL), (0x3FF0000000000000ULL), (0x4000000000000000ULL), (0x3FF0000000000000ULL), (0x3FF0000000000000ULL), (0x0000000000000000ULL), (0x3FF8000000000000ULL)
/* 1.0 */
/* 1.0 */
/* 0.0 */
/* 1.5 */
/* nan */
/* inf */
/* 2.0 */
/* 2.0 */
/* inf */
/* inf */
/* 0.0 */
/* 1.0 */
/* -1.0 */
/* -1.0 */
/* -0.0 */
/* -1.5 */
/* nan */
/* -inf */
/* -2.0 */
/* -2.0 */
/* -inf */
/* -inf */
/* -inf */
/* -1.0 */
/* -inf */
/* -inf */
/* -1.0 */
/* nan */
/* -inf */
/* -1.0 */
/* -2.0 */
/* -1.0 */
/* -1.0 */
/* -0.0 */
/* -1.5 */
/* nan */
/* inf */
/* 1.0 */
/* nan */
/* inf */
/* 1.0 */
/* 2.0 */
/* 1.0 */
/* 1.0 */
/* 0.0 */
/* 1.5 */
};
const float64 z_output[46] = {(0x7FF8000000000000ULL), (0x7FF0000000000000ULL), (0x4000000000000000ULL), (0x400C000000000000ULL), (0x7FF8000000000000ULL), (0x7FF0000000000000ULL), (0x4000000000000000ULL), (0x400C000000000000ULL), (0x7FF8000000000000ULL), (0x7FF0000000000000ULL), (0x0000000000000000ULL), (0x4004000000000000ULL), (0xFFF8000000000000ULL), (0xFFF0000000000000ULL), (0xC000000000000000ULL), (0xC00C000000000000ULL), (0xFFF8000000000000ULL), (0xFFF0000000000000ULL), (0xC000000000000000ULL), (0xC00C000000000000ULL), (0xFFF8000000000000ULL), (0xFFF0000000000000ULL), (0x8000000000000000ULL), (0xC004000000000000ULL), (0x7FF8000000000000ULL), (0x7FFFFFFFFFFFFFFFULL), (0x0000000000000000ULL), (0xFFF8000000000000ULL), (0xFFF0000000000000ULL), (0xBFF0000000000000ULL), (0xBFE0000000000000ULL), (0x7FF8000000000000ULL), (0x7FF0000000000000ULL), (0x3FF0000000000000ULL), (0x3FE0000000000000ULL), (0x7FF8000000000000ULL), (0x7FFFFFFFFFFFFFFFULL), (0x0000000000000000ULL), (0x7FF8000000000000ULL), (0x7FF0000000000000ULL), (0x3FF0000000000000ULL), (0x3FE0000000000000ULL), (0xFFF8000000000000ULL), (0xFFF0000000000000ULL), (0xBFF0000000000000ULL), (0xBFE0000000000000ULL)
/* nan */
/* inf */
/* 2.0 */
/* 3.5 */
/* nan */
/* inf */
/* 2.0 */
/* 3.5 */
/* nan */
/* inf */
/* 0.0 */
/* 2.5 */
/* nan */
/* -inf */
/* -2.0 */
/* -3.5 */
/* nan */
/* -inf */
/* -2.0 */
/* -3.5 */
/* nan */
/* -inf */
/* -0.0 */
/* -2.5 */
/* nan */
/* nan */
/* 0.0 */
/* nan */
/* -inf */
/* -1.0 */
/* -0.5 */
/* nan */
/* inf */
/* 1.0 */
/* 0.5 */
/* nan */
/* nan */
/* 0.0 */
/* nan */
/* inf */
/* 1.0 */
/* 0.5 */
/* nan */
/* -inf */
/* -1.0 */
/* -0.5 */
};

int main()
{
  int main_result;
  int i;
  float64 x1;
  float64 x2;
  main_result = 0;
  eop_198_main_result_201 = 0;
  for (i = 0; i < 46; i++) {
    float64 result;
    x1 = a_input[i];
    eop_200_x1_205 = x1;
    x2 = b_input[i];
    eop_200_x2_206 = x2;
    result = float64_add(x1,x2);
    eop_204_result_207 = result;
    main_result += result == z_output[i];
    eop_198_main_result_208 = main_result;
    printf("a_input=%016llx b_input=%016llx expected=%016llx output=%016llx\n",a_input[i],b_input[i],z_output[i],result);
  }
  printf("Result: %d\n",main_result);
  if (main_result == 46) {
    printf("RESULT: PASS\n");
  }
   else {
    printf("RESULT: FAIL\n");
  }
  return main_result;
}
